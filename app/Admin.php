<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;

class Admin extends Authenticatable
{
    public $table = "apps_admin";

    protected $fillable = [
        'name', 'email', 'password','divisi' 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        //  'remember_token',
    ];

    public function div()
    {
        return $this->hasOne('App\Models\AppsRoles', 'id', 'divisi');
    }

}
