<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\GeneralSetting;
use App\Helpers\LogHelper;
use App\Models\Transaction;

class ExpiredTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:expiredtransaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expired transaction';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $batas = GeneralSetting::where('key_setting','expired_time')->first()['value']; //on hour

        echo "sekarang " .now()."\n";
        echo "expired " . date('Y-m-d H:i:s',strtotime('-'.$batas.' hour',strtotime(now())))."\n";
        echo "\n\n\n";

        $data = Transaction::where('payment_status','UNPAID')->get();
        $expired = date('Y-m-d H:i:s',strtotime('-'.$batas.' hour',strtotime(now())));
        foreach($data as $d){
            if ($d['created_at'] <= $expired){
                LogHelper::add('transaction', $d['users_id'], 'SYSTEM', '', "Change to Expired transaction id : ". $d['id'] .", record for users_id : ".$d['users_id']);
                Transaction::where('id',$d['id'])->update(['payment_status' => 'EXPIRED']);
                echo "expired ".$d['created_at']."\n";
            }else{
                echo "engga ".$d['created_at']."\n";
                
            }
        }
    }
}
