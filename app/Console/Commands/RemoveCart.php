<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\GeneralSetting;
use App\Helpers\LogHelper;
use App\Models\Cart;

class RemoveCart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:removecart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removing Cart';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $batas = GeneralSetting::where('key_setting','cart_time')->first()['value']; //on hour

        echo "sekarang " .now()."\n";
        echo "expired " . date('Y-m-d H:i:s',strtotime('-'.$batas.' hour',strtotime(now())))."\n";
        echo "\n\n\n";

        $cart = Cart::get();
        $expired = date('Y-m-d H:i:s',strtotime('-'.$batas.' hour',strtotime(now())));
        foreach($cart as $c){
            if ($c['created_at'] <= $expired){
                LogHelper::add('cart', $c['users_id'], 'SYSTEM', '', "Removed cart id : ". $c['id'] .", variant id : ".$c['variant_id']." record for users_id : ".$c['users_id']);
                Cart::where('id',$c['id'])->delete();
                echo "expired ".$c['created_at']."\n";
            }
        }
    }
}
