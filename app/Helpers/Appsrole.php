<?php
namespace App\Helpers;

use App\Models\AppsRoles;
use App\Models\Client;
use Illuminate\Support\Facades\Auth;

class Appsrole {

    public static function category_user()
    {
        return Auth::user();
    }
    public static function menu($divisi)
    {
        $role = AppsRoles::where('id',$divisi)->first();
        // foreach(json_decode($role['role']) as $a => $b){
            $rolemenu = json_decode($role['role']);
        // }
        return json_encode($rolemenu);
    }
    public static function iduser()
    {
        $email = Auth::guard('api')->user()->email;
        $data = Client::select('id')->where('email',$email)->first();

        return json_encode($data['id']);
    }
    public static function typeuser()
    {
        $email = Auth::guard('api')->user()->email;

        return json_encode($data['id']);
    }
}

