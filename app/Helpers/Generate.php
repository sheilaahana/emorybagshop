<?php
namespace App\Helpers;
use App\Admin;
use App\User;
use App\Models\Transaction;
use App\Models\City;
use App\Models\Province;
use App\Models\GeneralSetting;
use App\Models\Warehouse;
use App\Models\WarehouseDetail;

class Generate {

    public static function admin($id)
    {
        $data = Admin::where('id',$id)->first();
        return $data['name'];
        
    }
    public static function cityorigin()
    {
        $data = GeneralSetting::where('key_setting','city_origin')->first();

        return $data['value'];
        
    }
    public static function store()
    {
        $data = GeneralSetting::where('key_setting','store')->first();

        return $data['value'];
        
    }
    public static function setting($key)
    {
        $data = GeneralSetting::where('key_setting',$key)->first();

        return $data['value'];
        
    }
    public static function user($id)
    {
        $data = User::where('id',$id)->first();
        return $data['email'];
        
    }
    public static function warehouse($id)
    {
        $data = User::where('id',$id)->first();
        return $data['warehouse_id'];
        
    }
    public static function wh($id)
    {
        $data = Warehouse::where('id',$id)->first();
        return $data['warehouse']." ( ".$data['code']." )";
        
    }
    public static function position($warehouseid, $variantid)
    {
        $wh = Warehouse::where('id',$warehouseid)->first();
        $data = WarehouseDetail::where('warehouse_id',$warehouseid)->where('variant_id',$variantid)->first();
        return "Warehouse : ".$wh['warehouse']." ( ".$wh['code'].") <br>Rak : ".$data['nomor_rak']." <br> Baris : ".$data['baris']." <br> Kolom : ".$data['kolom'];
        
    }
    public static function city($id)
    {
        $data = City::select('id','city','type')->where('id',$id)->first();
        return $data['type']." ".$data['city'];
        
    }
    public static function province($id)
    {
        $data = City::select('province_id')->where('id',$id)->first();
        return $data['province_id'];
        
    }
    public static function money($value = 0)
    {
        return 'Rp. ' . number_format( $value, 0 , '' , '.' ) . ',-';
    }
    public static function number($value = 0)
    {
        return number_format( $value, 0 , '' , '.' );
    }
    public static function invoice()
    {
        $data = Transaction::select('id','created_at')->wheredate('created_at',now())->count();

        $num = $data+1;
        $number = 'INV';
        $number .= date('Ymd', strtotime('now'));
        $number .= sprintf("%'02d", $num);
        return $number;
        
    }
    public static function link_verify($email)
    {
        return env('FRONTEND_URL')."/dashboard/verify-account/?token=".self::encryptedURL('encrypt',$email);
    }
    public static function link_reset($email)
    {
        return env('FRONTEND_URL')."/dashboard/reset-account/?token=".self::encryptedURL('encrypt',$email);
    }
    public static function value_history($value)
    {
        return json_decode($value);
    }
    public static function getFileExtension($file_name) {
        return pathinfo($file_name, PATHINFO_EXTENSION);
    }


    public static function slugUrl($string)
    {
        $string = strtolower($string);
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        $string = preg_replace("/[\s-]+/", " ", $string);
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string;
    }

    public static function encryptedURL($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'This is my secret key';
        $secret_iv = 'This is my secret iv';
        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    public static function countAvailableDay($startDate, $endDate)
    {
        return ((abs(strtotime ($startDate) - strtotime ($endDate)))/(60*60*24));
    }
}
