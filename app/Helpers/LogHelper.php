<?php
namespace App\Helpers;

use App\Models\Report as Rep;
use App\Models\LogChange;
use App\Models\LogStock;
use App\Models\WarehouseDetail;
use App\Models\ProductVariant;
use App\Models\Transaction;
use App\Models\TransactionStatus;
use Auth;

class LogHelper {

    public static function add($module, $moduleid, $changeby, $changeid, $description)
    {
        // cart id nya id users
        // LogHelper::add('product', $id, 'ADMIN', Auth::id(), "change data product detail");

        $field = [
            'module' => $module,
            'module_id' => $moduleid,
            'description' => $description,
            'change_by' => $changeby,
            'change_id' => $changeid,
        ];
        $input = LogChange::create($field);

        return $input;
    }
    public static function stock_minus($warehouse, $variantid, $changeby, $changeid, $stock, $description){
        // LogHelper::stock_minus('warehouse','variant','ADMIN', Auth::id(),'9','desc');
        $d = ProductVariant::where('id',$variantid)->first();
        $input = LogStock::create([
            'warehouse_id' => $warehouse,
            'product_id' => $d['product_id'],
            'variant_id' => $variantid,
            'type' => 'minus',
            'stock' => $stock,
            'description' => $description,
            'change_by' => $changeby,
            'change_id' => $changeid,
        ]);
        return $input;
    }
    public static function stock_plus($warehouse, $variantid, $changeby, $changeid, $stock, $description){
        // LogHelper::stock_plus('warehouse','variant','ADMIN', Auth::id(),'9','desc');
        $d = ProductVariant::where('id',$variantid)->first();
        $input = LogStock::create([
            'warehouse_id' => $warehouse,
            'product_id' => $d['product_id'],
            'variant_id' => $variantid,
            'type' => 'plus',
            'stock' => $stock,
            'description' => $description,
            'change_by' => $changeby,
            'change_id' => $changeid,
        ]);
        return $input;
    }
    public static function master_stock($variant_id,$stock){
        $d = ProductVariant::where('id',$variant_id);
        $cek = $d->count();
        if($cek>0){
            $update = $d->update(['qty_stock' => $stock ]);
        }
        return "saved";
    }
    public static function warehouse_stock($variant_id,$stock){
        $d = WarehouseDetail::where('variant_id',$variant_id)->where('warehouse_id','1');
        $cek = $d->count();
        if($cek>0){
            $update = $d->update(['stock' => $stock ]);
        }else{
            $d = ProductVariant::where('id',$variant_id);
            $cek = $d->count();
            if($cek>0){
                $data = $d->first();
                WarehouseDetail::create([
                    'warehouse_id' => '1',
                    'product_id' => $data['product_id'],
                    'variant_id' => $data['id'],
                    'nomor_rak' => "-",
                    'baris' => "-",
                    'kolom' => "-",
                    'stock' => $stock,
                    'catatan' => "-",
                ]);
            }
        }
        return "saved";
    }
    public static function status($id, $status, $desc, $changeby, $changeid)
    {
        $insert = ['status_trx' => $status];
        $merge = [];
        $cek = Transaction::where('id',$id)->count();
        if($cek > 0){
            TransactionStatus::create([
                'transaction_id' => $id,
                'status' => $status,
                'description' => $desc,
                'change_by' => $changeby,
                'change_id' => $changeid,
            ]);
            if($status == 'GUDANG'){
                $merge['change_gudang'] = date('Y-m-d');   
            }
            if($status == 'EKSPEDISI'){
                $merge['change_shipping'] = date('Y-m-d');   
            }
            $insert = array_merge($merge,$insert);
            Transaction::where('id',$id)->update($insert);
            
            if(in_array(strtoupper($status),['GAGAL','ARCHIVED'])){
                $data = Transaction::where('id',$id)->first();
                foreach($data['detail'] as $det){
                    if($data['warehouse_id'] == '1'){
                        $prodvar = ProductVariant::where('id',$det['variant_id']);
                        $qtyvar = $prodvar->first()['qty_stock'] + $det['qty'];
                        $prodvar->update(['qty_stock' => $qtyvar]);
                    }
                    $prodvar = WarehouseDetail::where('variant_id',$det['variant_id'])->where('warehouse_id',$data['warehouse_id']);
                    $qtyvar = $prodvar->first()['stock'] + $det['qty'];
                    $prodvar->update(['stock' => $qtyvar]);
                
                    Self::stock_plus($data['warehouse_id'], $det['variant_id'],'ADMIN', Auth::id(), $det['qty'],'Back stock '.$status.' transaction '.$data['invoice_number']);
                }
            }
            return false;
        }else{
            return true;
        }
    }
}
