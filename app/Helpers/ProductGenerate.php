<?php
namespace App\Helpers;

use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\Cart;
use App\User;
use App\Models\WarehouseDetail;
use Auth;

class ProductGenerate {

    public static function harga_user($category_id,$product_id)
    {
        $data = Product::where('id',$product_id)->first();
        if($data['harga_diskon']!=null){
            $harga = $data['harga_diskon'];
        }else{
            $harga = $data['harga_jual'];
        }

        return $harga;
    }
    public static function gudang($fill,$variant_id,$warehouse_id)
    {
        $data = WarehouseDetail::where('variant_id',$variant_id)->where('warehouse_id',$warehouse_id)->first();
        if(!empty($data)){
            return $data[$fill];
        }
        return "";
    }
    public static function harga($product_id)
    {
        $data = Product::where('id',$product_id)->first();
        if($data['harga_diskon']!=null){
            $harga = $data['harga_diskon'];
        }else{
            $harga = $data['harga_jual'];
        }

        return $harga;
    }
    public static function userwh($ware){
        $user = User::where('warehouse_id',$ware)->get();
        $u = [];
        foreach($user as $a){
            $u[] = $a['id'];
        }
        $u = array_unique($u);

        return $u;
    }
    public static function valid2($stock_cart,$variant_id)
    {
        $warehouse = User::where('id',Auth::id())->first()['warehouse_id'];
        if($warehouse == 1){
            $variant = ProductVariant::select('qty_stock')->where('id',$variant_id)->first();
            $cart = Cart::select('qty')->where('variant_id',$variant_id)->where('users_id','!=', Auth::id())->sum('qty');
            $totalstock = $variant['qty_stock']-$cart;
        }else{
            $variant = WarehouseDetail::where('variant_id',$variant_id)->first();
            $u = Self::userwh($warehouse);
            $cart = Cart::select('qty')->where('variant_id',$variant_id)->whereIn('users_id',$u)->where('users_id','!=', Auth::id())->sum('qty');
            $totalstock = $variant['stock']-$cart;
        }
        

        if($totalstock>=$stock_cart){
            $valid = 'true';
        }else{
            $valid = 'false';
        }
        return $valid;
    }
    public static function valid($cart_id,$variant_id)
    {
        $stock = Self::stock_cart($cart_id,$variant_id);
        $qty = Cart::select('qty')->where('id',$cart_id)->first()['qty'];

        if($stock>=$qty){
            $valid = 'true';
        }else{
            $valid = 'false';
        }

        return $valid;
    }
    public static function stock_cart($cart_id,$variant_id) // stok varian yang belum di keep selain milik dia sendiri
    {
        $warehouse = User::where('id',Auth::id())->first()['warehouse_id'];
        if($warehouse == 1){
            $variant = ProductVariant::select('qty_stock')->where('id',$variant_id)->first();
            $cart = Cart::select('qty')->where('variant_id',$variant_id)->where('id','!=',$cart_id)->sum('qty');
            $totalstock = $variant['qty_stock']-$cart;
            if($totalstock<0){
                $totalstock = 0;
            }
        }else{
            $variant = WarehouseDetail::select('stock')->where('variant_id',$variant_id)->first();
            $u = Self::userwh($warehouse);
            $cart = Cart::select('qty')->where('variant_id',$variant_id)->whereIn('users_id',$u)->where('id','!=', $cart_id)->sum('qty');
            $totalstock = $variant['stock']-$cart;
            if($totalstock == null){
                $totalstock = 0;
            }
        }
        return $totalstock;
    }
    public static function stock($variant_id) // stok varian yang belum di keep secara keseluruhan
    {
        $warehouse = User::where('id',Auth::id())->first()['warehouse_id'];
        if($warehouse != 1){
            $variant = WarehouseDetail::where('warehouse_id',$warehouse)->where('variant_id',$variant_id)->first();
            $totalstock = $variant['stock'];
            if($totalstock == null){
                $totalstock = 0;
            }
        }else{
            $variant = ProductVariant::select('qty_stock')->where('id',$variant_id)->first();
            $cart = Cart::select('qty')->where('variant_id',$variant_id)->sum('qty');
            $totalstock = $variant['qty_stock']-$cart;
            if($totalstock<0){
                $totalstock = 0;
            }
        }
        return $totalstock;        
    }
    public static function keepby($variant_id) // stok varian yang belum di keep secara keseluruhan
    {
        $cart = Cart::select('user_id')->where('variant_id',$variant_id)->count();
        return $cart;
    }
    public static function keepqty($variant_id) // stok varian yang belum di keep secara keseluruhan
    {
        $cart = Cart::select('qty')->where('variant_id',$variant_id)->sum('qty');
        return $cart;
    }
}
