<?php
namespace App\Helpers;

use App\Models\Report as Rep;
use App\Models\Transaction;
use App\Models\LogChange;


class Report {

    public static function laporan($request)
    {
        
        $data = Rep::orderBy('id','desc');
        if($request['customer']!==null){
            $data = $data->where('name','like','%'.$request['customer'].'%');
        }
        if($request['invoice']!==null){
            $data = $data->where('invoice_number','like','%'.$request['invoice'].'%');
        }
        if($request['start_date']!==null && $request['end_date']!==null){
            $tanggal = "between";
        }elseif($request['start_date']!==null && $request['end_date']==null){
            $tanggal = "start";
            $date = $request['start_date'];
        }elseif($request['start_date']==null && $request['end_date']!==null){
            $tanggal = "start";
            $date = $request['end_date'];
        }
        if(isset($tanggal)){
            if($tanggal == "start"){
                $data = $data->whereDate('created_at',$date);
            }elseif($tanggal == "between"){
                $data = $data->whereBetween('created_at', array($request['start_date'], $request['end_date']));
            }
        }
        if($request['change_gudang'] !== null){
            $data = $data->whereDate('change_gudang',$request['change_gudang']);
        }
        if($request['confirmed_date'] !== null){
            $data = $data->whereDate('confirmed_date',$request['confirmed_date']);
        }
        if($request['change_shipping'] !== null){
            $data = $data->whereDate('change_shipping',$request['change_shipping']);
        }
        if($request['set_shipping'] !== null){
            $data = $data->whereDate('set_shipping',$request['set_shipping']);
        }
        if($request['product']!==null){
            $data = $data->where('product_name','like','%'.$request['product'].'%');
        }
        if($request['data']!==null){
            if($request['data'] == 'all'){
                $data = $data->get();
            }else{
                $data = $data->paginate($request['data']);
            }
        }else{
            $data = $data->paginate(20);
        }
        return $data;
    }
    public static function log($request)
    {
        $data = LogChange::orderBy('id','ASC');
        if(isset($request['module'])){
            $data = $data->where('module',$request['module']);
        }
        if(isset($request['change_by'])){
            $data = $data->where('change_by',$request['change_by']);
        }
        if(isset($request['data'])){
            if($request['data'] == 'all'){
                $data = $data->get();
            }else{
                $data = $data->paginate($request['data']);
            }
        }else{
            $data = $data->get();
        }   

        return $data;
    }
    public static function summary($request = [])
    {
        $start_date = Transaction::select('created_at')->orderby('created_at','ASC')->first();
        $start_date = strtotime($start_date['created_at']);
        $end_date = Transaction::select('created_at')->orderby('created_at','DESC')->first();
        $end_date = strtotime($end_date['created_at']);

        $numMonths = 1 + (date("Y",$end_date)-date("Y",$start_date))*12;
        $numMonths += date("m",$end_date)-date("m",$start_date);

        // return date('d m y', $start_date)." - ". date('d m y', $end_date)." - ". $numMonths;

        $data = [
            'customer_service' => [],
            'quality_control' => [],
            'gudang' => [],
            'packing' => []
        ];
        for ($i=0; $i < $numMonths; $i++) { 
            $fill = date('m-Y', strtotime("+".$i."month", $start_date ));
            
            $date = [
                date('Y-m-01',strtotime("+".$i."month", $start_date )),
                date('Y-m-t',strtotime("+".$i."month", $start_date ))
            ];

            $customer_service = Transaction::select('customer_service')->whereBetween('created_at', $date)->groupby('customer_service')->get()->pluck('customer_service');
            foreach($customer_service as $cs){
                if($cs != null){
                    $data['customer_service'][$fill][$cs] =  Transaction::where('customer_service', $cs)->whereBetween('created_at', $date)->count();
                }
            }
            $quality_control = Transaction::select('quality_control')->whereBetween('created_at', $date)->groupby('quality_control')->get()->pluck('quality_control');
            foreach($quality_control as $qc){
                if($qc != null){
                    $data['quality_control'][$fill][$qc] = Transaction::where('quality_control', $qc)->whereBetween('created_at', $date)->count();
                }
            }
            $gudang = Transaction::select('gudang')->whereBetween('created_at', $date)->groupby('gudang')->get()->pluck('gudang');
            foreach($gudang as $gd){
                if($gd != null){
                    $data['gudang'][$fill][$gd] = Transaction::where('gudang', $gd)->whereBetween('created_at', $date)->count();
                }
            }
            $packing = Transaction::select('packing')->whereBetween('created_at', $date)->groupby('packing')->get()->pluck('packing');
            foreach($packing as $pc){
                if($pc != null){
                    $data['packing'][$fill][$pc] = Transaction::where('packing', $pc)->whereBetween('created_at', $date)->count();
                }
            }
        }
        return $data;
    }
}
