<?php
namespace App\Helpers;
use App\Models\UserAdmin;

class Responses {

    public static function valid($error)
    {
        $error = json_decode(json_encode($error), true);
        $err = "";
        foreach($error as $e => $a){
            foreach($a as $b){
                $err .= "\n".$b;
            }
        }
        $output['meta'] = [
            'code' => 422,
            'message' => substr($err,1),
            'error' => $error
        ];
        return response($output, 422);


        // $output['meta'] = ['code' => 422,'error'=>$validator->errors()];
        // return response()->json($output, 422); 
    }
    public static function error($code, $message)
    {
        $output['meta'] = ['code' => $code, 'message' => $message]; 
        return $output;
    }
    public static function output($data)
    {
        if($data){
            $output['meta'] = ['code' => 200, 'message' => 'Data Found'];
            $output['data'] = $data;
        }else{
            $output['meta'] = ['code' => 400, 'message' => 'Data not Found']; 
        }
        return $output;
    }
    public static function save($data)
    {
        if ($data) {
            $code = 200;
            $message = 'Data Saved!';
	    }else{
            $code = 400;
            $message = 'Data Failed to Save!';
        }
        $output['meta'] = ['code' => $code, 'message' => $message];
        if ($data) {
            $output['data'] = $data;
	    }
        return response($output, $code);

    }
    public static function delete($data)
    {
        if ($data) {
            $code = 200;
            $message = 'Data Deleted!';
	    }else{
            $code = 400;
            $message = 'Data Failed to Delete!';
        }
        $output['meta'] = ['code' => $code, 'message' => $message];
        return response($output, $code);
    }
}
