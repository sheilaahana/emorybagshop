<?php
namespace App\Helpers;

use App\Models\Voucher;
use App\Models\VoucherRedeem;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\ProductCategory;
use App\Helpers\Generate;
use Auth;

class VoucherGenerate {

    public static function redeemed($id)
    {
        $data = VoucherRedeem::where('voucher_id',$id)->count();
        return $data;
    }
    public static function pname($id)
    {
        $data = Product::select('product_name')->where('id',$id)->first()['product_name'];
        return $data;
    }
    public static function category($id)
    {
        $data = ProductCategory::select('category_name')->where('id',$id)->first()['category_name'];
        return $data;
    }
    public static function add_redeem($transaction_id, $voucher_code)
    {
        $trans = Transaction::where('id',$transaction_id)->first();
        foreach($voucher_code as $vc) {
            $voc = Voucher::where('code',$vc)->first();
            VoucherRedeem::create([
                'users_id' => $trans['users_id'],
                'voucher_id' => $voc['id'],
                'transaction_id' => $transaction_id,
                'code' => $voc['code'],
                'discount_type' => $voc['discount_type'],
                'type' => $voc['type'],
                'value' => $voc['value'],
            ]);
        }
        return "success";
    }
    public static function check($request)
    {
        $tag = [];
        foreach($request['cart'] as $c){
            $product_id[] = $c['product_id'];
        }
        $product_id = array_unique($product_id);
        $tagdata = ['is_promo','is_bestseller','is_hotitem','is_mostwanted'];
        foreach($product_id as $pid){
            $produkdata = Product::where('id',$pid)->first();
            $category_id[] = $produkdata['category_id'];
            foreach($tagdata as $t){
                if($produkdata[$t] == 1){
                    $tag[] = $t;
                }
            }
        }
        $category_id = array_unique($category_id);
        $tag = array_unique($tag);
        if(count($request['code'])>1){
            foreach($request['code'] as $codevoucher){
                $canjoin = Voucher::where('code',$codevoucher)->first();
                if($canjoin['is_join'] == 0){
                    return response()->json([
                        'meta' => [
                            'code' => 403,
                            'message' => 'Maaf kode voucher "'.$codevoucher.'" tidak dapat digabung'
                        ]
                    ]);
                }
            }
        }
        foreach($request['code'] as $codevoucher){
            $get = Voucher::where('code',$codevoucher)->first();
            $potongan = 0;
            if(!empty($get)){
                if($get['is_valid'] == 0){
                    return response()->json([
                        'meta' => [
                            'code' => 403,
                            'message' => 'Maaf kode voucher "'.$codevoucher.'" tidak valid'
                        ]
                    ]);
                }

                $today = date('Y-m-d');
                $DateBegin = date('Y-m-d', strtotime($get['start_date']));
                $DateEnd = date('Y-m-d', strtotime($get['end_date']));
                // Ngecek Tanggal Expired
                if (($today >= $DateBegin) && ($today <= $DateEnd)){

                    if(!empty($get['minimal_spend'])){
                        if($get['minimal_spend']>$request['cart_price']){
                            return response()->json([
                                'meta' => [
                                    'code' => 403,
                                    'message' => 'Maaf kode voucher "'.$codevoucher.'" bisa digunakan untuk minimal pembelanjaan '.Generate::money($get['minimal_spend'])
                                ]
                            ]);
                        }
                    }
                    if(!empty($get['maximal_spend'])){
                        if($get['maximal_spend']<$request['cart_price']){
                            return response()->json([
                                'meta' => [
                                    'code' => 403,
                                    'message' => 'Maaf kode voucher "'.$codevoucher.'" bisa digunakan untuk maksimal pembelanjaan '.Generate::money($get['maximal_spend'])
                                ]
                            ]);
                        }
                    }
                    if(!empty($get['whitelist_user'])){
                        if(!in_array(Auth::id(), $get['whitelist_user'])){
                            return response()->json([
                                'meta' => [
                                    'code' => 403,
                                    'message' => 'Maaf user dalam transaksi ini tidak termasuk pada ketentuan kode voucher "'.$codevoucher.'" ini'
                                ]
                            ]);
                        }
                    }
                    if(!empty($get['blacklist_user'])){
                        if(in_array(Auth::id(), $get['blacklist_user'])){
                            return response()->json([
                                'meta' => [
                                    'code' => 403,
                                    'message' => 'Maaf kode voucher "'.$codevoucher.'" tidak dapat digunakan oleh user dalam transaksi ini'
                                ]
                            ]);
                        }
                    }

                    // ngecek validity_ general
                    $reed = VoucherRedeem::where('voucher_id',$get['id'])->count();
                    if($reed>=$get['validity_general']){
                        return response()->json([
                            'meta' => [
                                'code' => 403,
                                'message' => 'Maaf kode voucher "'.$codevoucher.'" sudah melebihi limit general',
                            ]
                        ]);
                    }
                    // ngecek validity_ user
                    $reedmem = VoucherRedeem::where('voucher_id',$get['id'])->where('users_id',Auth::id())->count();
                    if($reedmem>=$get['validity_user']){
                        return response()->json([
                            'meta' => [
                                'code' => 403,
                                'message' => 'Maaf kode voucher "'.$codevoucher.'" sudah melebihi limit per user',

                            ]
                        ]);
                    }


                    // ngeecek kalau ada spesifik product
                    foreach($product_id as $pid){
                        if(!empty($get['product_include'])){
                            if(!in_array($pid, $get['product_include'])){
                                return response()->json([
                                    'meta' => [
                                        'code' => 403,
                                        'message' => 'Maaf product dalam transaksi ini tidak termasuk dengan ketentuan kode voucher "'.$codevoucher.'" ini'
                                    ]
                                ]);
                            }
                        }
                        if(!empty($get['product_exclude'])){
                            if(in_array($pid, $get['product_exclude'])){
                                return response()->json([
                                    'meta' => [
                                        'code' => 403,
                                        'message' => 'Maaf product dalam transaksi ini tidak sesuai dengan ketentuan kode voucher "'.$codevoucher.'" ini'
                                    ]
                                ]);
                            }
                        }
                    }
                    foreach($category_id as $cid){
                        if(!empty($get['category_include'])){
                            if(!in_array($cid, $get['category_include'])){
                                return response()->json([
                                    'meta' => [
                                        'code' => 403,
                                        'message' => 'Maaf category product dalam transaksi ini tidak termasuk dengan ketentuan kode voucher "'.$codevoucher.'" ini'
                                    ]
                                ]);
                            }
                        }
                        if(!empty($get['category_exclude'])){
                            if(in_array($cid, $get['category_exclude'])){
                                return response()->json([
                                    'meta' => [
                                        'code' => 403,
                                        'message' => 'Maaf category product dalam transaksi ini tidak sesuai dengan ketentuan kode voucher "'.$codevoucher.'" ini'
                                    ]
                                ]);
                            }
                        }
                    }
                    foreach($tag as $tid){
                        if(!empty($get['tag_include'])){
                            if(!in_array($tid, $get['tag_include'])){
                                return response()->json([
                                    'meta' => [
                                        'code' => 403,
                                        'message' => 'Maaf tag product dalam transaksi ini tidak termasuk dengan ketentuan kode voucher "'.$codevoucher.'" ini'
                                    ]
                                ]);
                            }
                        }
                        if(!empty($get['tag_exclude'])){
                            if(in_array($tid, $get['tag_exclude'])){
                                return response()->json([
                                    'meta' => [
                                        'code' => 403,
                                        'message' => 'Maaf tag product dalam transaksi ini tidak sesuai dengan ketentuan kode voucher "'.$codevoucher.'" ini'
                                    ]
                                ]);
                            }
                        }
                    }
                    
                    // valid
                    // basic potongan voucher
                    if($get['discount_type']=='AMOUNT'){
                        $disc = $get['value'];
                    }elseif($get['discount_type']=='PERCENT'){
                        if($get['value'] > 100 ){
                            $get['value'] = 100;
                        }
                        $disc = $request['cart_price']*$get['value']/100;
                    }
                    if($get['type']=='CART'){
                        $dlast['voucher'][$codevoucher]['discount_cart'] = $disc;
                    }
                    if($get['type']=='SHIPPING'){
                        $dlast['voucher'][$codevoucher]['discount_shipping'] = $disc;
                    }
                }else{
                    // decline
                    return response()->json([
                        'meta' => [
                            'code' => 403,
                            'message' => 'Maaf kode voucher "'.$codevoucher.'" tidak dapat digunakan sekarang'
                        ]
                        ]);
                }
                
            }else{
                return response()->json([
                    'meta' => [
                        'code' => 410,
                        'message' => 'Maaf, kode voucher "'.$codevoucher.'" yang anda masukkan tidak ditemukan'
                    ]
                ]);
            }
        }

        $discountcart = 0;
        $discountshipping = 0;
        foreach($request['code'] as $codevoucher){
            if(isset($dlast['voucher'][$codevoucher]['discount_cart'])){
                $discountcart = $discountcart + $dlast['voucher'][$codevoucher]['discount_cart'];
            }

            if(isset($dlast['voucher'][$codevoucher]['discount_shipping'])){
                $discountshipping = $discountshipping + $dlast['voucher'][$codevoucher]['discount_shipping'];
            }
        }
        $dlast['discount'] = [
            'discount_cart' => $discountcart,
            'discount_shipping' => $discountshipping,
            'first_cart_price' => $request['cart_price'],
            'last_cart_price' => $request['cart_price'] - $discountcart,
            'first_shipping_price' => $request['shipping_price'],
            'last_shipping_price' => $request['shipping_price'] - $discountshipping,
        ];

        if($dlast['discount']['last_cart_price']<0){
            $dlast['discount']['last_cart_price'] = 0;
        }
        if($dlast['discount']['last_shipping_price']<0){
            $dlast['discount']['last_shipping_price'] = 0;
        }
        

        $code = 200;
        $message = "Selamat, voucher dapat digunakan";

        return response()->json([
            'meta' => [
                'code' => $code,
                'message' => $message
            ],
            'data' => $dlast
        ]);
    }   
    public static function code($request){
        $data = Self::check($request);
        $data = json_decode(json_encode($data), true);
        return $data['original'];
    }
    
}
