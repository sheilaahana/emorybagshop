<?php
namespace App\Helpers;

use App\Models\Report as Rep;
use App\Models\Wallet;
use App\Models\WalletUsers;
use App\Models\GeneralSetting;
use App\Models\Transaction;
use Auth;

class WalletHelper {

    public static function add($inputby, $category, $data)
    {
        $field = [
            'users_id' => $data['users_id'],
            'category' => $category,
            'type' => $data['type'],
            'amount' => $data['amount'],
            'input_by' => $inputby,
            'memo' => $data['memo'],
        ];
        if($inputby=='ADMIN'){
            $field['admin_id'] = Auth::id();
        }
        $input = Wallet::create($field);

        return $input;
    }
    public static function add_point($transaction_id)
    {
        $perpoint = GeneralSetting::where('key_setting','bonus_point')->first()['value'];
        $trans = Transaction::where('id',$transaction_id)->first();
        $total = $trans['total'];
        $getpoint = floor($total/$perpoint);
        $data = [
            'users_id' => $trans['users_id'],
            'type' => 'DB',
            'amount' => $getpoint,
            'memo' => 'Get point from transaction #'.$trans['invoice_number']
        ];

        Transaction::where('id',$transaction_id)->update(['get_point' => $getpoint]);

        Self::add('SYSTEM','POINT',$data);

        return $getpoint;
    }
    public static function totalwallet($users_id)
    {
        $db = Wallet::where('type','DB')->where('users_id',$users_id)->sum('amount');
        $cr = Wallet::where('type','CR')->where('users_id',$users_id)->sum('amount');
        $total = $db - $cr;
        return $total;

    }
    public static function wallet($users_id)
    {
        $db = Wallet::where('type','DB')->where('category','WALLET')->where('users_id',$users_id)->sum('amount');
        $cr = Wallet::where('type','CR')->where('category','WALLET')->where('users_id',$users_id)->sum('amount');
        $total = $db - $cr;
        return $total;
    }
    public static function point($users_id)
    {
        $db = Wallet::where('type','DB')->where('category','POINT')->where('users_id',$users_id)->sum('amount');
        $cr = Wallet::where('type','CR')->where('category','POINT')->where('users_id',$users_id)->sum('amount');
        $total = $db - $cr;
        return $total;
    }
}
