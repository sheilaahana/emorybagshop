<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\Cart;
use App\Models\Wishlist;
use Auth;
use App\Helpers\LogHelper;

use App\Helpers\Responses as Res;
use App\Helpers\ProductGenerate as Prod;

class ActionController extends Controller
{
    public function wishlist(){
        $count = Wishlist::where('users_id',Auth::id())->count();
        $data = Wishlist::where('users_id',Auth::id())->with([
                    'product' => function ($a){ $a->select('id','product_name','harga_jual','harga_diskon','image','category_id','type_product') ;},
                    'variant'
                ])->orderby('created_at','desc')->get();

        if ($count>0) {
            $code = 200;
            $message = 'Data Found!';
            $output['data'] = $data;
        }else{
            $code = 400;
            $message = 'Data not Found!';
        }
        $output['meta'] = ['code' => $code, 'message' => $message];
        return response($output, 200);
        
        // return Res::output($data);
    }
    public function add_wishlist(Request $request){
        $cek = Product::where('id',$request->input('product_id'))->count();
        if($cek>0){
            if($request->input('variant_id')!==null){
                $cek1 = ProductVariant::where('id',$request->input('variant_id'))->where('product_id',$request->input('product_id'))->count();
                if($cek1==0){
                    $output['meta'] = ['code' => 400, 'message' => 'Invalid Variant ID, Data not Found'];
                    return response($output, 400);
                }
                $cek3 = Wishlist::where('users_id',Auth::id())->where('variant_id',$request->input('variant_id'))->count();
                if($cek3>0){
                    $output['meta'] = ['code' => 400, 'message' => 'Data Variant Already Exist'];
                    return response($output, 400);
                }
            }else{
                $cek2 = Wishlist::where('users_id',Auth::id())->where('product_id',$request->input('product_id'))->count();
                if($cek2 > 0){
                    $output['meta'] = ['code' => 400, 'message' => 'Data Product Already Exist'];
                    return response($output, 400);
                }
            }

            $input = Wishlist::create([
                'product_id' => $request->input('product_id'),
                'variant_id' => $request->input('variant_id'),
                'users_id' => Auth::id(),
            ]);
            return Res::save($input);
        }else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid Product ID, Data not Found'];
        }
        return response($output, 400);
    }
    public function delete_wishlist($id){

        $cek = Wishlist::where('users_id',Auth::id())->where('id',$id)->count();
        if($cek > 0){
            $delete = Wishlist::where('id',$id)->delete();
            
            return Res::delete($delete);
        }
        else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid Data'];
            return response($output, 400);
        }

    }
    public function delete_wishlist_variant($id){

        $cek = Wishlist::where('users_id',Auth::id())->where('variant_id',$id)->count();
        if($cek > 0){
            $delete = Wishlist::where('variant_id',$id)->delete();
            
            return Res::delete($delete);
        }
        else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid Data'];
            return response($output, 400);
        }
    }
    public function delete_wishlist_product($id){

        $cek = Wishlist::where('users_id',Auth::id())->where('product_id',$id)->count();
        if($cek > 0){
            $delete = Wishlist::where('product_id',$id)->delete();
            
            return Res::delete($delete);
        }
        else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid Data'];
            return response($output, 400);
        }
    }

    public function cart(){
        $data = Cart::select('id','memo','product_id','variant_id','qty')->where('users_id',Auth::id())->with([
                    'product' => function ($a){ $a->select('id','product_name','harga_jual','berat','harga_diskon','image','category_id','type_product'); },
                    'variant'
                ])->orderby('created_at','desc')->get();
        $total = 0;
        // return $data;
        
        $output = [];
        foreach($data as $d){
            // $harga = Prod::harga($d['product_id']);
            $stock = Prod::stock_cart($d['id'],$d['variant_id']);
            $valid = Prod::valid($d['id'],$d['variant_id']);
            if(isset($d['product']['harga_diskon']) && !empty($d['product']['harga_diskon'])){
                $harga = $d['product']['harga_diskon'];
            }else{
                $harga = $d['product']['harga_jual'];
            }
            $perproduct = $d['qty']*$harga;
            $output['cart'][] = [
                'id' => $d['id'],
                'product' => $d['product'],
                'variant' => $d['variant'],
                'memo' => $d['memo'],
                'qty' => $d['qty'],
                'harga' => $harga,
                'stock' => $stock,
                'validity_stock' => $valid,
                'sub_total' => $perproduct
            ];
            $total = $total + $perproduct;
        }
        if($total!=0){
            $output['total'] = $total;
        }
        return Res::output($output);
    }
    public function mass_cart(Request $request){
        $count =   count($request->all());
        for($a=0;$a<$count;$a++){
            $req = $request->all()[$a];
            $dat = ProductVariant::where('id',$req['variant_id']);
            $cek = $dat->count();
            $valid = Prod::valid2($req['qty'],$req['variant_id']);
            if($req['qty'] < 1){
                $output['meta'] = ['code' => 422, 'message' => 'Invalid Stock Input'];
                return $output;
            }
            if($valid == "false"){
                $output['meta'] = ['code' => 400, 'message' => 'Invalid Stock'];
                return $output;
            }
            if($cek==0){
                $output['meta'] = ['code' => 400, 'message' => 'Invalid Product ID, Data not Found'];
                return $output;
            }
        }
        for($a=0;$a<$count;$a++){
            $req = $request->all()[$a];
            $dat = ProductVariant::where('id',$req['variant_id']);

            $product_id = $dat->select('id','product_id')->first()['product_id'];
            $dat2 = Cart::where('users_id',Auth::id())->where('variant_id',$req['variant_id']);
            $cek2 = $dat2->count();
            if($cek2 == 0){
                $cekstock = Prod::stock($req['variant_id']);
                if($cekstock - $req['qty'] >= 0 ){
                    $input = Cart::create([
                        'product_id' => $product_id,
                        'users_id' => Auth::id(),
                        'memo' => $req['memo'],
                        'variant_id' => $req['variant_id'],
                        'qty' => $req['qty']
                    ]);
                    LogHelper::stock_minus(Auth::user()->warehouse_id, $req['variant_id'], 'SYSTEM', "", $req['qty'],'Customer '.Auth::id().' Add to Cart');
                }else{
                    $output['meta'] = ['code' => 400, 'message' => 'Invalid stock'];
                    return response($output, 400);
                }
            }
            else{
                LogHelper::stock_plus(Auth::user()->warehouse_id, $req['variant_id'], 'SYSTEM', "", $dat2->first()['qty'],'Customer '.Auth::id().' Update Cart');
                $input = Cart::where('id',$dat2->first()['id'])->update([
                    'memo' => $req['memo'],
                    'qty' => $req['qty']
                ]);
                LogHelper::stock_minus(Auth::user()->warehouse_id, $req['variant_id'], 'SYSTEM', "", $req['qty'],'Customer '.Auth::id().' Update Cart');
                $input = Cart::where('id',$dat2->first()['id'])->first();
            }
            $output['meta'] = ['code' => 200, 'message' => 'Data Saved'];
        }
        return $output;
    }
    public function add_cart(Request $request){

        $validate = $request->validate([
            'variant_id' => 'required|max:100', 
            'qty' => 'required|integer|min:1',
        ]);

        $valid = Prod::valid2($request->qty,$request->variant_id);
        if($valid == "false"){
            $output['meta'] = ['code' => 400, 'message' => 'Invalid Stock'];
            return $output;
        }

        $dat = ProductVariant::where('id',$request->variant_id);
        $cek = $dat->count();
        if($cek>0){
            $product_id = $dat->select('id','product_id')->first()['product_id'];
            $dat2 = Cart::where('users_id',Auth::id())->where('variant_id',$request->variant_id);
            $cek2 = $dat2->count();
            if($cek2 == 0){
                $cekstock = Prod::stock($request->variant_id);
                if($cekstock - $request->qty >= 0 ){
                    $input = Cart::create([
                        'product_id' => $product_id,
                        'users_id' => Auth::id(),
                        'memo' => $request->memo,
                        'variant_id' => $request->variant_id,
                        'qty' => $request->qty
                    ]);
                    LogHelper::stock_minus(Auth::user()->warehouse_id, $request->variant_id, 'SYSTEM', "", $request->qty, 'Customer '.Auth::id().' Add to Cart');

                }else{
                    $output['meta'] = ['code' => 400, 'message' => 'Invalid stock'];
                    return response($output, 400);
                }
            }
            else{
                LogHelper::stock_plus(Auth::user()->warehouse_id, $request->variant_id, 'SYSTEM', "", $dat2->first()['qty'], 'Customer '.Auth::id().' Update Data Cart');
                $input = Cart::where('id',$dat2->first()['id'])->update([
                    'memo' => $request->memo,
                    'qty' => $request->qty
                ]);
                LogHelper::stock_minus(Auth::user()->warehouse_id, $request->variant_id, 'SYSTEM', "", $request->qty, 'Customer '.Auth::id().' Update Data Cart');
                $input = Cart::where('id',$dat2->first()['id'])->first();
            }
            return Res::save($input);
        }else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid Product ID, Data not Found'];
        }
        return response($output, 400);
    }
    public function delete_cart($id){

        $cek = Cart::where('users_id',Auth::id())->where('id',$id)->count();
        if($cek > 0){
            $data = Cart::where('users_id',Auth::id())->where('id',$id)->first();
            LogHelper::stock_plus(Auth::user()->warehouse_id, $data['variant_id'], 'SYSTEM', "", $data['qty'], 'Customer '.Auth::id().' Delete Data Cart');

            $delete = Cart::where('id',$id)->delete();
            
            return Res::delete($delete);
        }
        else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid Data'];
            return response($output, 400);
        }

    }
}
