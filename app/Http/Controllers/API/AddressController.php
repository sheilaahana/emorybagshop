<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserShipping;
use Auth;
use Validator;
use App\Helpers\Responses as Res;

class AddressController extends Controller
{
    public function address(){
        $data = UserShipping::where('users_id',Auth::id())->get();

        return Res::output($data);
    }
    public function add_address(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'phone' => 'required', 
            'address' => 'required', 
            'city' => 'required', 
            'province' => 'required', 
        ]); 
        if ($validator->fails()) { 
            return Res::valid($validator); 
        }
        if($request->input('address_id') == null){
            $input = UserShipping::create([
                'name' => $request->input('name'), 
                'phone' => $request->input('phone'), 
                'address' => $request->input('address'), 
                'postal_code' => $request->input('postal_code'), 
                'city' => $request->input('city'), 
                'province' => $request->input('province'), 
                'users_id' => Auth::id(), 
            ]);
        }else{
            $cek = UserShipping::where('id',$request->input('address_id'))->where('users_id',Auth::id())->count();
            if($cek>0){
                $input = UserShipping::where('id',$request->input('address_id'))->update([
                    'name' => $request->input('name'), 
                    'phone' => $request->input('phone'), 
                    'address' => $request->input('address'), 
                    'postal_code' => $request->input('postal_code'), 
                    'city' => $request->input('city'), 
                    'province' => $request->input('province'), 
                ]);
                $input = UserShipping::where('id',$request->input('address_id'))->first();
            }else{
                $output['meta'] = ['code' => '400', 'message' => 'Invalid data / data is not belongs to you'];
                return response($output, 400);
            }
        }
        
        return Res::save($input);
    }
    public function delete_address($id){

        $cek = UserShipping::where('users_id',Auth::id())->where('id',$id)->count();
        if($cek > 0){
            $delete = UserShipping::where('id',$id)->delete();
            
            return Res::delete($delete);
        }
        else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid Data'];
            return response($output, 400);
        }

    }
}
