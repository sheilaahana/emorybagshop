<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ProductCategory;
use App\Models\Slider;
use App\Models\PaymentTarget;
use App\Models\GeneralSetting;

use App\Helpers\Responses as Res;
use App\Helpers\Appsrole as Appsrole;

class MasterController extends Controller
{
    public function product_category(){
        if(isset($_GET['id'])){
            $data = ProductCategory::where('id',$_GET['id'])->first(); 
        }else{
            $data = ProductCategory::all();            
        }

        return Res::output($data);
    }
    public function payment_target(){
        $data = PaymentTarget::select('id','bank_name','bank_icon','name','number');
        if(isset($_GET['id'])){
            $data = $data->where('id',$_GET['id'])->first(); 
        }else{
            $data = $data->get();            
        }

        return Res::output($data);
    }
    public function slider(){
        
        $data = Slider::all(); 

        return Res::output($data);
    }
    public function courier(){
        $data = GeneralSetting::where('key_setting','jasa_pengiriman')->first();
        $d = json_decode($data['value']);
        foreach($d as $v){
            $jasa[] = $v;
        }

        return Res::output($jasa);
    }
    public function general_setting(){
        $data = GeneralSetting::all();
        foreach($data as $v){
           if($v['key_setting']!="jasa_pengiriman"){
               $output[$v['key_setting']] = $v['value'];
           }
        }

        return Res::output($output);
    }
}
