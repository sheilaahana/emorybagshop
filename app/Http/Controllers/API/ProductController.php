<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\WarehouseDetail;
use App\User;
use Auth;

use App\Helpers\Responses as Res;

class ProductController extends Controller
{
    public function index(){
        if(isset($_GET['per_page'])){ $per_page = $_GET['per_page']; }else{ $per_page = 20; }

        $data = Product::where('is_publish',1);
        if(isset($_GET['search']) && !empty($_GET['search'])){
            $data = $data->where('product_name','like', '%'.$_GET['search'].'%');
        }
        if(isset($_GET['category']) && !empty($_GET['category'])){
            $data = $data->where('category_id', $_GET['category']);
        }
        if(isset($_GET['filter']) && !empty($_GET['filter'])){
            $filterfield = strtolower($_GET['filter']);
            if( $filterfield == 'hot'){
                $filter = 'is_hotitem';
            }
            elseif($filterfield == 'promo'){
                $filter = 'is_promo';
            }
            elseif($filterfield == 'best'){
                $filter = 'is_bestseller';
            }
            elseif($filterfield == 'most'){
                $filter = 'is_mostwanted';
            }
            $data = $data->where($filter, 1);
        }
        $warehouse = User::where('id',Auth::id())->first()['warehouse_id'];
        if($warehouse != 1){
            $pid = [];
            $pd = WarehouseDetail::select('product_id')->where('warehouse_id',$warehouse)->groupby('product_id')->get();
            foreach($pd as $p){
                $pid[] = $p['product_id'];
            }
            $data = $data->whereIn('id',$pid);
        }

        $data = $data->paginate($per_page);

        return Res::output($data);
    }
    public function detail($id){
        $data = Product::with([
                    'variant' => function($b) {
                        $b->select('id','product_id','variant','hexa','qty_stock','image');
                    }
                ])->where('is_publish',1)->where('id',$id)->first();
        $count = $data['count_seen']+1;
        Product::where('id',$id)->update(['count_seen'=>$count]);
        return Res::output($data);
    }
}
