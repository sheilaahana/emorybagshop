<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Province;
use App\Models\City;
use App\Models\Courier;
use App\Models\UserShipping;
use App\Helpers\Generate as Generate;
use App\Helpers\Responses as Res;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Validator;
use Auth;


class ShippingController extends Controller
{
    public function province(){
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $data = Province::where('id',$_GET['id'])->first();
        }else{
            $data = Province::all();
        }
        return Res::output($data);
    }
    public function city(){
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $data = City::where('id',$_GET['id'])->first();
        }
        elseif(isset($_GET['province_id']) && !empty($_GET['province_id'])){
            $data = City::where('province_id',$_GET['province_id'])->get();            
        }
        else{
            $data = City::all();
        }
        return Res::output($data);
    }
    public function courier(){
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $data = Courier::where('id',$_GET['id'])->first();
        }else{
            $data = Courier::all();
        }
        return Res::output($data);
    }
    public function cost(Request $request){

    if($request->input('address_id') !== null ){
        $cek = UserShipping::where('id',$request->input('address_id'))->where('users_id',Auth::id())->first();
        if($cek){
            $destination = $cek['city'];
        }else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid data address_id'];
            return response($output, 400);
        }
    }elseif($request->input('destination') !== null ){
        $destination = $request->input('destination');
    }else{
        $output['meta'] = ['code' => 400, 'message' => 'Required Data Destination'];
        return response($output, 400);
    }
        $courier = strtolower($request->input('courier'));
        $client = new Client(); //GuzzleHttp\Client
        $result = $client->post('https://pro.rajaongkir.com/api/cost', [
            'form_params' => [
                'origin' => Generate::cityorigin(),
                'destination' => $destination,
                'weight' => $request->input('weight')?:1,
                'courier' => $courier?:'jne',
                'originType' => 'city',
                'destinationType' => 'city'
            ],
            'headers' => [
                'key' => '178ccaf77e12458548df4bac71c2ded6'
            ]
        ]);
        $result = json_decode($result->getBody(), true);
        $data['origin'] = $result['rajaongkir']['origin_details'];
        $data['destination'] = $result['rajaongkir']['destination_details'];
        foreach($result['rajaongkir']['results'] as $r){
            $data['weight'] = $request->input('weight')?:1;
            $data['courier'] = $r['name'];
            $data['courier_code'] = $r['code'];
            foreach($r['costs'] as $c){
                $data['service'][] = [
                    'name' => $c['service'],
                    'description' => $c['description'],
                    'price' => $c['cost'][0]['value'],
                    'estimated' => $c['cost'][0]['etd'],
                    'note' => $c['cost'][0]['note'],
                ];
            }
        }

        return Res::output($data);
    }
    public function tracking(Request $request){

        $validator = Validator::make($request->all(), [ 
            'courier' => 'required', 
            'resi' => 'required', 
        ]); 
        if ($validator->fails()) { 
            return Res::valid($validator->errors());           
        }
        
        if($request->all()){
            $kurir = strtolower($request->courier);
            try {
                $client = new Client(); //GuzzleHttp\Client
                $result = $client->post('https://pro.rajaongkir.com/api/waybill', [
                    'form_params' => [
                        'courier' => $kurir?:'jne',
                        'waybill' => $request->resi,
                    ],
                    'headers' => [
                        'key' => '178ccaf77e12458548df4bac71c2ded6'
                    ]
                ]);
                $result = json_decode($result->getBody(), true);
                $data['query'] = $result['rajaongkir']['query'];
                $data['result'] = $result['rajaongkir']['result'];
                return Res::output($data);
            } catch (RequestException $e) {
                $response = $e->getResponse();
                echo (string)($response->getBody());
            } catch (\Exception $e) {
                if ($e->getResponse()->getStatusCode() == '400') {
                    $result = "Resi not Found";
                    $data['error']['resi'] = $request->resi;
                    $data['error']['courier'] = $request->courier;
                    return Res::error(400,'Resi not Found');
                }
            }
        }
    }
}
