<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Responses as Res;
use App\Helpers\ProductGenerate as Prod;
use App\Helpers\WalletHelper;
use App\Helpers\VoucherGenerate;
use App\Helpers\LogHelper;
use App\Helpers\Generate as Generate;
use Auth;
use App\Models\UserShipping;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\Cart;
use App\Models\ProductVariant;
use App\Models\WarehouseDetail;
use Illuminate\Support\Facades\Storage;
use Validator;

class TransactionController extends Controller
{
    public function list(){
        $data = Transaction::select('id','created_at','invoice_number','payment_receipt','confirmation_date','shipping_status','payment_status','notes','status_trx')
            ->where('users_id',Auth::id())->where('status_trx','<>','ARCHIVED')->orderby('created_at','desc');

        if(isset($_GET['search']) && !empty($_GET['search'])){
            $data = $data->where('invoice_number','like', '%'.$_GET['search'].'%')->orWhere('notes','like', '%'.$_GET['search'].'%')->orWhere('nama_penerima','like', '%'.$_GET['search'].'%');
        }
        if(isset($_GET['payment_status']) && !empty($_GET['payment_status'])){
            $data = $data->where('payment_status',$_GET['payment_status']);
        }
        if(isset($_GET['status']) && !empty($_GET['status'])){
            $data = $data->where('status_trx',$_GET['status']);
        }
        if(isset($_GET['shipping_status']) && !empty($_GET['shipping_status'])){
            $data = $data->where('shipping_status',$_GET['shipping_status']);
        }
        $data = $data->get();

        return Res::output($data);
    }
    public function list_detail(){
        $data = Transaction::where('users_id',Auth::id())->with(['detail' => function($a){ $a->with(['product','variant']); } ])->orderby('created_at','desc')->get();
        return Res::output($data);
    }
    public function detail($id){
        $data = Transaction::where('id',$id)->where('users_id',Auth::id());
        if($data->count() > 0){
            $data = $data->with(['detail' => function($a){ $a->with(['product','variant']); } ])->get();
            return Res::output($data);
        }else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid ID or data is not belongs to you'];
            return response($output, 400);
            
        }
    }
    public function terima(Request $request){
        $data = Transaction::where('id',$request->input('transaction_id'))->where('users_id',Auth::id());
        if($data->count() > 0){
            $input = Transaction::where('id',$request->input('transaction_id'))->update([
                        'shipping_status' => 'TERKIRIM'
                    ]);
            $data = Transaction::select('id','invoice_number','shipping_status')->where('id',$request->input('transaction_id'))->first();
            return Res::save($data);
        }else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid ID or data is not belongs to you'];
            return response($output, 400);
            
        }
    }
    public function billing(Request $request){
        $cek_alamat = UserShipping::where('id',$request->input('address_id'))->where('users_id',Auth::id())->count();
        if($cek_alamat > 0 ) {
            foreach($request->input('cart_id') as $ci){
                $cek_cart = Cart::where('id',$ci)->where('users_id',Auth::id())->count();
                if($cek_cart == 0){
                    $output['meta'] = ['code' => 400, 'message' => 'Empty cart_id or this Cart Items is not belongs to you'];
                    return response($output, 400);
                    
                }else{
                    $cek_valid = Cart::select('variant_id')->where('id',$ci)->where('users_id',Auth::id())->first();
                    $valid = Prod::valid($ci,$cek_valid['variant_id']);
                    if($valid == 'false'){
                        $output['meta'] = ['code' => 400, 'message' => 'Invalid Stock, check your items'];
                    return response($output, 400);
                        
                    }
                }
                $cart_id[] = $ci;
            }

            $data['address'] = UserShipping::where('id',$request->input('address_id'))->where('users_id',Auth::id())->first();
            $data['cart'] = Cart::whereIn('id',$cart_id)->with(['product','variant'])->get();

            return Res::output($data);
        }else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid address_id or this Address is not belongs to you'];
        }
        return response($output, 400);
        
    }
    public function upload_bukti(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'image' => 'required|image', 
            'transaction_id' => 'required', 
        ]); 
        if ($validator->fails()) { 
            return Res::valid($validator);               
        }
        $cek = Transaction::where('id',$request->input('transaction_id'))->where('users_id',Auth::id())->count();
        if($cek > 0){
            $data = Transaction::select('id','payment_receipt')->where('id',$request->input('transaction_id'))->where('users_id',Auth::id())->first();
            if($data['payment_receipt'] !== null && !empty($data['payment_receipt'])){
                $output['meta'] = ['code' => 400, 'message' => 'You have confirmed this payment'];            
                return response($output, 400);
                
            }else{
                $path = $request->file('image')->store('public/bukti');
                $input = Transaction::where('id',$request->input('transaction_id'))->update([
                    'payment_receipt' => $path,
                    'confirmation_date' => date(now())
                ]);
                LogHelper::status($request->transaction_id, 'CONFIRMATION', "Customer Upload Bukti Transfer", "SYSTEM", "");
                return Res::save($input); 
            }
        }else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid Transaction ID or this data is not belongs to you'];            
            return response($output, 400);
            
        }
        
    }
    public function upload_bukti_mass(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'image' => 'required|image', 
            'transaction_id' => 'required', 
        ]); 
        if ($validator->fails()) { 
            return Res::valid($validator);               
        }
        $request->transaction_id = json_decode($request->transaction_id,true);
        $path = $request->file('image')->store('public/bukti');

        foreach($request->transaction_id as $id){
            $cek = Transaction::where('id',$id)->where('users_id',Auth::id())->count();
            if($cek > 0){
                $img = Transaction::select('id','payment_receipt')->where('id',$id)->where('users_id',Auth::id())->first()['payment_receipt'];
                $input = Transaction::where('id',$id)->update([
                    'payment_receipt' => $path,
                    'confirmation_date' => date(now())
                ]);
                LogHelper::status($id, 'CONFIRMATION', "Customer Upload Bukti Transfer ( mass )", "SYSTEM", "");
                if($img!== null){
                    Storage::delete($img);
                }
            }else{
                $output['meta'] = ['code' => 400, 'message' => 'Invalid Transaction ID or this data is not belongs to you'];            
                return response($output, 400);
            }
        }
        return Res::save($input); 

        
        
    }   
    public function checkout(Request $request){
        if(Generate::store() == 'closed'){
            $output['meta'] = ['code'=> 400, 'message' => "STORE CLOSED"];
            return $output;
        }

        $cek_alamat = UserShipping::where('id',$request->input('address_id'))->where('users_id',Auth::id())->count();
        if($cek_alamat > 0 ) {
            foreach($request->input('cart') as $ci){
                $cek_cart = Cart::where('id',$ci['id_cart'])->where('users_id',Auth::id())->count();
                if($cek_cart == 0){
                    $output['meta'] = ['code' => 400, 'message' => 'Empty cart_id or this Cart Items is not belongs to you'];
                    return response($output, 400);
                    
                }else{
                    $cek_valid = Cart::select('variant_id')->where('id',$ci['id_cart'])->where('users_id',Auth::id())->first();
                        $valid = Prod::valid2($ci['qty'],$cek_valid['variant_id']);
                    if($valid == 'false'){
                        $output['meta'] = ['code' => 400, 'message' => 'Invalid Stock, check your items'];
                        return response($output, 400);
                    }
                }
                $cart_id[] = $ci;
            }
            if(!empty($request->voucher_code)){
                $cekvoucher = [
                    'code' => $request->voucher_code,
                    'shipping_price' => $request->input('shipping_price'),
                    'cart_price' => $request->input('total')
                ];
                foreach($cart_id as $cid){
                    $pid = Cart::where('id',$cid)->first()['product_id'];
                    $cekvoucher['cart'][] = ['id_cart' => $cid, "product_id" => $pid];
                }
                $hasilvoucher = VoucherGenerate::code($cekvoucher);
                // return $hasilvoucher['data'];
                if($hasilvoucher['meta']['code'] == 200 ){
                    $request->shipping_service = $hasilvoucher['data']['discount']['last_shipping_price'];
                    $request->total = $hasilvoucher['data']['discount']['last_cart_price'];
                    $voucher_valid = true;
                }else{
                    return $hasilvoucher['meta'];
                }
            }

            $address = UserShipping::where('id',$request->input('address_id'))->where('users_id',Auth::id())->first();

            $input_transaksi = Transaction::create([
                'invoice_number' => Generate::invoice(),
                'cashless_order' => $request->input('cashless_order')?:"",
                'users_id' => Auth::id(),
                'email' => Auth::user()->email,
                'nama_penerima' => $address['name'],
                'telpon_penerima' => $address['phone'],
                'alamat_penerima' => $address['address'],
                'kodepos' => $address['postal_code'],
                'city_origin' => Generate::cityorigin(),
                'city_destination' => $address['city'],
                'courier' => $request->input('courier'),
                'shipping_service' => $request->input('shipping_service'),
                'ongkos_kirim' => $request->shipping_price,
                'total' => $request->total,
                'payment_target' => $request->input('payment_target'),
                'jenis_pesanan' => Auth::user()->type,
                'warehouse_id' => Auth::user()->warehouse_id,
                'status_trs' => "PENDING",
                'notes' => $request->notes,
                'nama_pengirim' => isset($request->nama_pengirim) && !empty($request->nama_pengirim) ? $request->nama_pengirim : Generate::setting('nama_pengirim'),
                'kontak_pengirim' => isset($request->kontak_pengirim) && !empty($request->kontak_pengirim) ? $request->kontak_pengirim : Generate::setting('kontak_pengirim'),
            ]);   
            $getpoint = WalletHelper::add_point($input_transaksi['id']);
            if(isset($voucher_valid)){
                VoucherGenerate::add_redeem($input_transaksi['id'],$request->voucher_code);
            }

            foreach($cart_id as $cid){
                $cart = Cart::where('id',$cid)->with('product')->first();
                $input_detail = TransactionDetail::create([
                    'transaction_id' => $input_transaksi['id'],
                    'product_id' => $cart['product_id'],
                    'variant_id' => $cart['variant_id'],
                    'price' => Prod::harga($cart['product_id']),
                    'qty' => $cid['qty'],
                    'memo' => $cid['memo'],
                ]);
                    
                $prodvar = WarehouseDetail::where('variant_id',$cart['variant_id'])->where('warehouse_id',Auth::user()->warehouse_id);
                $qtyvar = $prodvar->first()['stock'] - $cid['qty'];
                $prodvar->update(['stock' => $qtyvar]);

                if(Auth::user()->warehouse_id == '1'){
                    LogHelper::master_stock($cart['variant_id'], $qtyvar);
                }

                LogHelper::stock_plus(Auth::user()->warehouse_id, $cart['variant_id'], 'SYSTEM', "", $cid['qty'], 'Get from Cart berfore Customer '.Auth::id().'Checkout '. $input_transaksi['invoice_number']);
                LogHelper::stock_minus(Auth::user()->warehouse_id, $cart['variant_id'], 'SYSTEM', "", $cid['qty'], 'Customer '.Auth::id().'Checkout '. $input_transaksi['invoice_number']);
                
                Cart::where('id',$cid['id_cart'])->delete();
            }
            $data = Transaction::where('id',$input_transaksi['id'])->with('detail')->first();
            LogHelper::add('customer', Auth::id(), 'USER', Auth::id(), "Melakukan Checkout Transaction");

            return Res::output($data);
        }else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid address_id or this Address is not belongs to you'];
        }
        return response($output, 400);
        
    }
    public function checkout_backup(Request $request){
        $cek_alamat = UserShipping::where('id',$request->input('address_id'))->where('users_id',Auth::id())->count();
        if($cek_alamat > 0 ) {
            foreach($request->input('cart_id') as $ci){
                $cek_cart = Cart::where('id',$ci)->where('users_id',Auth::id())->count();
                if($cek_cart == 0){
                    $output['meta'] = ['code' => 400, 'message' => 'Empty cart_id or this Cart Items is not belongs to you'];
                    return response($output, 400);
                    
                }else{
                    $cek_valid = Cart::select('variant_id')->where('id',$ci)->where('users_id',Auth::id())->first();
                    $valid = Prod::valid($ci,$cek_valid['variant_id']);
                    if($valid == 'false'){
                        $output['meta'] = ['code' => 400, 'message' => 'Invalid Stock, check your items'];
                        return response($output, 400);
                        
                    }
                }
                $cart_id[] = $ci;
            }
            $address = UserShipping::where('id',$request->input('address_id'))->where('users_id',Auth::id())->first();

            $input_transaksi = Transaction::create([
                'invoice_number' => Generate::invoice(),
                'users_id' => Auth::id(),
                'email' => Auth::user()->email,
                'nama_penerima' => $address['name'],
                'telpon_penerima' => $address['phone'],
                'alamat_penerima' => $address['address'],
                'kodepos' => $address['postal_code'],
                'city_origin' => Generate::cityorigin(),
                'city_destination' => $address['city'],
                'courier' => $request->input('courier'),
                'shipping_service' => $request->input('shipping_service'),
                'ongkos_kirim' => $request->input('shipping_price'),
                'total' => $request->input('total'),
                'payment_target' => $request->input('payment_target'),
            ]);   
            foreach($cart_id as $cid){
                $cart = Cart::where('id',$cid)->with('product')->first();
                $input_detail = TransactionDetail::create([
                    'transaction_id' => $input_transaksi['id'],
                    'product_id' => $cart['product_id'],
                    'variant_id' => $cart['variant_id'],
                    'price' => Prod::harga($cart['product_id']),
                    'qty' => $cart['qty'],
                    'memo' => $cart['memo'],
                ]);
                $prodvar = ProductVariant::where('id',$cart['variant_id']);
                $qtyvar = $prodvar->first()['qty_stock'] - $cart['qty'];
                $prodvar->update(['qty_stock' => $qtyvar]);
                Cart::where('id',$cid)->delete();
            }
            $data = Transaction::where('id',$input_transaksi['id'])->with('detail')->first();

            return Res::output($data);
        }else{
            $output['meta'] = ['code' => 400, 'message' => 'Invalid address_id or this Address is not belongs to you'];
        }
        return response($output, 400);
        
    }
}
