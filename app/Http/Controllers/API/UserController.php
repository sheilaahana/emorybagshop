<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Responses as Res;
use Validator;
use App\User;
use App\Models\UserShipping;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Auth;
use App\Helpers\LogHelper;



class UserController extends Controller
{   
    public function profile(){
        $data = User::where('id',Auth::id())->with('warehouse','category','city')->first();

        return Res::output($data);
    }
    public function edit_profile(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'phone' => 'required', 
            'email' => ['required','email',Rule::unique('users')->ignore(Auth::id())],
            'address' => 'required', 
            'city' => 'required', 
            'province' => 'required', 
            'postal_code' => 'required', 
            'gender' => 'required', 
        ]); 
        if ($validator->fails()) { 
           return Res::valid($validator->errors());            
        }
        $tanggal_lahir = date("Y-m-d", strtotime(str_replace('/', '-', $request->input('birthdate'))));

        $user = User::where('id',Auth::id())->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'gender' => $request->input('gender'),
            'birthdate' => $tanggal_lahir,
            'address' => $request->input('address'),
            'postal_code' => $request->input('postal_code'),
            'city' => $request->input('city'),
            'province' => $request->input('province'),
        ]);
        LogHelper::add('customer', Auth::id(), 'USER', Auth::id(), "Change Data Customer");

        $user = User::where('id',Auth::id())->first();
        return Res::save($user); 
    }
    public function upload_image(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'image' => 'required|image', 
        ]); 
        if ($validator->fails()) { 
            return Res::valid($validator->errors());           
        }
        $old_foto = User::where('id',Auth::id())->first()['image'];
        $path = $request->file('image')->store('public/profile');
        $user = User::where('id',Auth::id())->update([
            'image' => $path,
        ]);
        if($old_foto!=''){
            Storage::delete($old_foto);
        }
        LogHelper::add('customer', Auth::id(), 'USER', Auth::id(), "Change Image Profile");

        return Res::save($user); 
    }
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email|unique:users', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
            'type' => 'required', 
            'phone' => 'required', 
            'address' => 'required', 
            'city' => 'required', 
            'province' => 'required', 
            'postal_code' => 'required', 
            'gender' => 'required', 
        ]); 
        if ($validator->fails()) { 
            return Res::valid($validator->errors());           
        }
        $tanggal_lahir = date("Y-m-d", strtotime(str_replace('/', '-', $request->input('birthdate'))));

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'type' => $request->input('type'),
            'gender' => $request->input('gender'),
            'birthdate' => $tanggal_lahir,
            'password' => Hash::make($request->input('password')),
            'status' => 'MODERATE',
            'address' => $request->input('address'),
            'postal_code' => $request->input('postal_code'),
            'city' => $request->input('city'),
            'province' => $request->input('province'),
        ]);
        $user['shipping'] = UserShipping::create([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
            'postal_code' => $request->input('postal_code'),
            'city' => $request->input('city'),
            'province' => $request->input('province'),
            'users_id' => $user['id']
        ]);
        if($user){
            $output['meta'] = ['code' => 200, 'message' => 'User Registered'];
            $output['data'] = $user;
            LogHelper::add('customer', $user['id'], 'USER', $user['id'], "Customer Registered");
            
            return response($output, 200);
        }else{
            $output['meta'] = ['code' => 400, 'message' => 'User failed to Register'];
            return response($output, 400);
        }
        
        // $success['token'] =  $user->createToken('MyApp')->accessToken; 
    }
}

