<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\VoucherCode;
use App\Models\VoucherRedeem;
use App\Helpers\VoucherGenerate;
use Validator;
use Auth;


class VoucherController extends Controller
{
    public function voucher_check(Request $request){
        
        $validator = Validator::make($request->all(), [ 
            'code' => 'required', 
            'shipping_price' => 'required', 
            'cart_price' => 'required', 
            'cart' => 'required', 
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        return VoucherGenerate::check($request);
    }
}
