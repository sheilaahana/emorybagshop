<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wallet;
use App\Helpers\Responses as Res;
use App\Helpers\WalletHelper;
use Auth;

class WalletController extends Controller
{
    public function list(Request $request){
        $data = Wallet::where('users_id',Auth::id());
        if($request->start_date){
            $date = [
                date('Y-m-d' ,strtotime($request->start_date)),
                date('Y-m-d' ,strtotime($request->start_date)) 
            ];
            if($request->end_date){
                $date[1] = date('Y-m-d' ,strtotime($request->end_date));
            }
            $data = $data->whereBetween('created_at',$date);
        }
        if($request->sort && in_array(strtoupper($request->sort), ['ASC','DESC'])){
            $data = $data->orderby('created_at',$request->sort);
        }
        $data = $data->get();
        return Res::output($data);
    }
    public function amount(){
        $data['wallet'] = WalletHelper::wallet(Auth::id());
        $data['point'] = WalletHelper::point(Auth::id());
        $data['total'] = WalletHelper::totalwallet(Auth::id());

        return Res::output($data);
    }
    public function full(Request $request){
        $data['amount'] = Self::amount()['data'];
        $data['list'] = Self::list($request)['data'];

        return Res::output($data);
    }
}
