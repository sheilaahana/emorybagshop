<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Models\AppsModules;
use App\Models\AppsRoles;
use Illuminate\Validation\Rule;
use App\Models\LogChange;
use App\Helpers\LogHelper;
use Auth;
use Validator;

class AdministratorController extends Controller
{
    public function index(){
        $data = Admin::with('div')->where('status','!=','Deleted')->where('id','!=','999')->orderby('divisi','ASC')->get();
        $log = LogChange::where('module','administrator')->orderby('id','DESC')->get();
        return view('content.admin.list')->with(['data' => $data,'log'=>$log]);
    }
    public function form(){

       	if (isset($_GET['id'])) {
            $data = Admin::where('id',$_GET['id'])->first();
        }
        $data['divdata'] = AppsRoles::all();

        return view('content.admin.form')->with(['data'=>$data]);

    }
    public function role_form(){

         $data['module'] = AppsModules::where('type','!=','child')->get();
         $data['role'] = AppsRoles::all();
        $log = LogChange::where('module','role')->orderby('id','DESC')->get();

         return view('content.admin.role')->with(['data'=>$data,'log'=>$log]);
    }
    public function save_role(Request $request){
        $divisi = AppsRoles::all();
        foreach($divisi as $d){
            $ganti = json_encode($request[$d['id']]);
            AppsRoles::where('id',$d['id'])->update(['role'=>$ganti]);
        }
        LogHelper::add('role', "0", 'ADMIN', Auth::id(), "Edit Role Administrator");
  		return redirect('administrator/role')->with('message','Data Saved');
    }
    public function save(Request $request){
        if($request->id){
            $validate = $request->validate([
                'email' => ['required','email',Rule::unique('apps_admin')->ignore($request->id)],
                'name' => 'required|max:200',
                'divisi' => 'required',
            ]);
            $input = Admin::where('id',$request->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'divisi' => $request->divisi,
            ]);
            LogHelper::add('administrator', $request->id, 'ADMIN', Auth::id(), "edit data Administrator ".$request->name);
           
        }else{
            $validate = $request->validate([
                'email' => 'unique:apps_admin|required|email|max:100',
                'name' => 'required|max:200',
                'divisi' => 'required',
            ]);

            $input = Admin::create([
                'name' => $request->name,
                'email' => $request->email,
                'divisi' => $request->divisi,
                'password' => bcrypt('EmoryBagShop'),
                'status' => "Active"
            ]);
            LogHelper::add('administrator', $input['id'], 'ADMIN', Auth::id(), "Add new data Administrator ".$request->name);

        }
  		return redirect('administrator');
    }
    public function reset($id){
        $name = Admin::where('id',$id)->first()['name'];
        $input = Admin::where('id',$id)->update([
            'password' => bcrypt('EmoryBagShop')
        ]);
        if($input){
            $message = "Password Updated, your password is EmoryBagShop";
            LogHelper::add('administrator', $id, 'ADMIN', Auth::id(), "Reset Password Administrator ".$name);

        }else{
            $message = "Failed";
        }
  		return redirect('administrator')->with(['message' => $message]);
    }
    public function delete($id)
    {
        if($id == Auth::id()){
  		    return redirect('administrator')->with('message',"Can't Delete your own Account");
        }
        $u = Admin::where('id',$id)->first();
        $delete = Admin::where('id',$id)->where('id','!=','999')->update([ 'status' => 'Deleted','email' => "-".$u['email'] ]);
        // $delete = Admin::where('id',$id)->delete();
  		if ($delete) {
            $message = 'Deleted!';
            LogHelper::add('administrator', $id, 'ADMIN', Auth::id(), "Delete Data Administrator ".$u['name']);
              
  		}else{
  			$message = 'Delete Failed!';
  		}
  		return redirect('administrator')->with('message',$message);
    }
}
