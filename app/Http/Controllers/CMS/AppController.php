<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GeneralSetting;
use App\Models\Province;
use App\Models\City;
use App\Models\Courier;

class AppController extends Controller
{
    public function index(){
        $data['data'] = GeneralSetting::all();
        $data['province'] = Province::all();
        $data['courier'] = Courier::all();
    
        return view('content.setting.form')->with(['data' => $data]);
    }
    public function save(Request $request){
        foreach($request->change as $key => $value){
            if($key=='jasa_pengiriman'){
                $value = json_encode($value);
            }
            GeneralSetting::where('key_setting',$key)->update([ 'value' => $value ]);
        }
        return redirect()->back()->with('message','Data Saved');
    }
    function ajax_city($id){
        $query = City::where('province_id',$id)->get();
        if (count($query)>0) {
            $data = "<option value=''>- Select City -</option>";
            foreach ($query as $value) {
                $data .= "<option value='".$value['id']."'>".$value['type']." ".$value['city']."</option>";
            }
        }else{
            $data = "<option value=''>- No Data  -</option>";
        }
        return $data;
    }
}
