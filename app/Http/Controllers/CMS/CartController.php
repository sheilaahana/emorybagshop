<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Helpers\LogHelper;
use App\Helpers\Generate;
use App\Models\ProductVariant;
use App\User;
use Auth;

class CartController extends Controller
{
    public function index(){
        $variant = [];
        $data = Cart::select('variant_id')->groupby('variant_id')->get();
        if(!empty($data)){
            foreach($data as $d){
                $id[] = $d['variant_id'];
            }
            if(!empty($id)){
                $variant = ProductVariant::whereIn('id',$id)->get();
            }
        }
        return view('content.cart.list')->with(['data' => $variant]);
    }
    public function form(){
        if(isset($_GET['id'])){
            $data['variant'] = ProductVariant::where('id',$_GET['id'])->with('product')->first();
            $data['user'] = User::get();
            return view('content.cart.form')->with(['data' => $data]);
        }
        else{
            return redirect()->back();
        }
    }
    public function save(Request $request){
        if($request->qty > 0){
            $cek = Cart::where('variant_id',$request->variant_id)->where('users_id',$request->user)->count();
            if($cek > 0){
                Cart::where('variant_id',$request->variant_id)->where('users_id',$request->user)->update([
                    'product_id' => $request->product_id,
                    'users_id' => $request->user,
                    'memo' => '',
                    'variant_id' => $request->variant_id,
                    'qty' => $request->qty,
                ]);
            }else{
                Cart::create([
                    'product_id' => $request->product_id,
                    'users_id' => $request->user,
                    'memo' => '',
                    'variant_id' => $request->variant_id,
                    'qty' => $request->qty,
                ]);
            }
            LogHelper::stock_minus(Generate::warehouse($request->user), $request->variant_id, 'SYSTEM', "", $request->qty, 'Customer '.$request->user.' Add to Cart by admin');
            
            return redirect('product/detail/'.$request->product_id);
        }else{
            return redirect()->back();
        }   

    }
    public function detail($id){
        $data = ProductVariant::where('id',$id)->first();
        $data['cart'] = Cart::where('variant_id',$id)->get();
        return view('content.cart.detail')->with(['data' => $data]);
    }
    public function delete($id){
        $data = Cart::where('id',$id)->first();
        $cek = Cart::where('id',$id)->delete();
        LogHelper::stock_plus(Generate::warehouse($data['users_id']), $data['variant_id'], 'ADMIN', Auth::id(), $data['qty'], 'Delete Customer Cart '.$data['users_id'].' by Admin');
        LogHelper::add('cart', $id, 'ADMIN', Auth::id(), "Removed variant ".$data['product']['product_name']." - ".$data['variant']['variant']." record for users_id : ".$data['users_id']);
        LogHelper::add('product', $data['product_id'], 'ADMIN', Auth::id(), "Removed product variant ".$data['variant']['variant']." from cart record for users_id : ".$data['users_id']);
        return redirect()->back()->with('message','Data Deleted');
    }
}
