<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserCategory;
use App\Models\ProductCategory;
use App\Models\Product;
use App\Models\UserPrice;
use App\Models\LogChange;
use App\Helpers\LogHelper;
use App\User;
use Auth;
use Illuminate\Support\Facades\Storage;


class CategoryController extends Controller
{
    public function user(){
        $data = UserCategory::all();
        $log = LogChange::where('module','usercategory')->orderby('id','DESC')->get();
        return view('content.usercategory.list')->with(['data' => $data, 'log' => $log]);
    }
    public function user_form(){
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $data = UserCategory::where('id',$_GET['id'])->first();
            return view('content.usercategory.form')->with(['data' => $data]);
        }else{
            return view('content.usercategory.form');
        }
    }
    public function user_save(Request $request){
        $field = [
            'category' => $request->input('category'),
            'description' => $request->input('description'),
        ];
        if($request->input('id')){
            $input = UserCategory::where('id',$request->input('id'))->update($field);
            LogHelper::add('usercategory', $request->input('id'), 'ADMIN', Auth::id(), "edit data category ".$field['category']);
        }else{
            $input = UserCategory::create($field);
            LogHelper::add('usercategory', $input['id'], 'ADMIN', Auth::id(), "add new data category ".$field['category']);
        }
        if($input){
            $message = "Data Saved";
        }else{
            $message = "Failed to Saved";
        }
        return redirect('category/user')->with('message',$message);
    }    
    public function user_delete($id){
        $cek = User::where('category_id',$id)->count();
        $cek1 = UserPrice::where('category_id',$id)->count();
        if($cek>0 || $cek1>0){
            $message = "Can't delete, data used";
        }else{
            $cat = UserCategory::where('id',$id)->first()['category'];
            UserCategory::where('id',$id)->delete();
            $message = "Data Deleted";
            LogHelper::add('usercategory', $id, 'ADMIN', Auth::id(), "delete data category ".$cat);

        }
        return redirect('category/user')->with('message',$message);
    }

    public function product(){
        $data = ProductCategory::all();
        $log = LogChange::where('module','productcategory')->orderby('id','DESC')->get();

        return view('content.productcategory.list')->with(['data' => $data,'log'=>$log]);
    }
    public function product_form(){
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $data = ProductCategory::where('id',$_GET['id'])->first();
            return view('content.productcategory.form')->with(['data' => $data]);
        }else{
            return view('content.productcategory.form');
        }
    }
    public function product_save(Request $request){
        $field = [
            'category_name' => $request->input('category_name'),
            'type' => "READY STOCK",
            'description' => $request->input('description'),
        ];

        if($request->input('id')){
            $dt = ProductCategory::where('id',$request->id);
            $img = $dt->first()['icon'];

            $id = $request->input('id');
            $input = ProductCategory::where('id',$id)->update($field);

            if($request->file('icon')!== null){
                Storage::delete($img);
            }
            LogHelper::add('productcategory', $id, 'ADMIN', Auth::id(), "Edit data product category ".$field['category_name']);

        }else{
            $input = ProductCategory::create($field);
            $id = $input['id'];
            LogHelper::add('productcategory', $id, 'ADMIN', Auth::id(), "Add new data product category ".$field['category_name']);

        }
        if($request->file('icon')!== null){
            $path = $request->file('icon')->store('public/assets');
            ProductCategory::where('id',$id)->update(['icon'=> $path]);
        }
        if($input){
            $message = "Data Saved";
        }else{
            $message = "Failed to Saved";
        }
        return redirect('category/product')->with('message',$message);
    }    
    public function product_delete($id){
        $cek = Product::where('category_id',$id)->count();
        if($cek>0){
            $message = "Can't delete, data used";
        }else{
            $cat = ProductCategory::where('id',$id)->first()['category_name'];

            ProductCategory::where('id',$id)->delete();
            $message = "Data Deleted";
            
            LogHelper::add('productcategory', $id, 'ADMIN', Auth::id(), "Delete data product category ".$cat);

        }
        return redirect('category/product')->with('message',$message);
    }
}
