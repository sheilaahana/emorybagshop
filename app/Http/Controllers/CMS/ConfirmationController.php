<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Helpers\LogHelper;
use Auth;

class ConfirmationController extends Controller
{
    public function index(){
        $data = Transaction::where('confirmation_date','!=',null)->where('payment_status','UNPAID')->orwhere('status_trx','CONFIRMATION')->orderby('id','desc')->get();
        return view('content.confirmation.list')->with(['data' => $data]);
    }
    public function shipping(){
        $data = Transaction::orderby('id','desc');
        if(isset($_GET['name']) && $_GET['name']!=''){
            $data = $data->where('nama_penerima','like','%'.$_GET['name'].'%');
        }
        if(isset($_GET['invoice']) && $_GET['invoice']!=''){
            $data = $data->where('invoice_number','like','%'.$_GET['invoice'].'%');
        }
        if(isset($_GET['date']) && $_GET['date']!=''){
            $data = $data->where('confirmation_date',$_GET['date']);
        }
        if(isset($_GET['shipping']) && $_GET['shipping']!=''){
            $data = $data->where('shipping_status',$_GET['shipping']);
        }
        if(isset($_GET['resi']) && $_GET['resi']!=''){
            if($_GET['resi'] == 'EMPTY'){
                $data = $data->where('shipping_receipt',null);
            }elseif($_GET['resi'] == 'ISSET'){
                $data = $data->where('shipping_receipt','!=','');
            }
        }
        
        $data = $data->paginate(10);
        return view('content.shipping.list')->with(['data' => $data]);
    }
    public function shipping_save(Request $request){
        // return $request['update'];
        foreach($request['update'] as $u => $i){
            Transaction::where('id',$u)->update([
                'shipping_status' => $i['status'],
                'shipping_receipt' => $i['resi'],
                'shipping_date' => $i['shipping_date'],
                'shipping_time' => $i['shipping_time'],
                'set_shipping' => date('Y-m-d'),
            ]);
            LogHelper::add('transaction', $u, 'ADMIN', Auth::id(), "Changed transaction shipping status to ".$i['status']." and receipt to ".$i['resi']);
        }
        return redirect()->back()->with('message', 'Data Saved !');
        $data = Transaction::orderby('id','desc')->paginate(10);
        return view('content.shipping.list')->with(['data' => $data]);
    }
}
