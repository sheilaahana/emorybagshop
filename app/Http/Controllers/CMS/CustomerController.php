<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\UserCategory;
use Illuminate\Validation\Rule;
use App\Models\UserShipping;
use App\Models\Province;
use App\Helpers\LogHelper;
use App\Models\LogChange;
use Illuminate\Support\Facades\Hash;
use App\Models\Warehouse;
use Auth;

class CustomerController extends Controller
{
    public function index(){
        $data = User::all();
        return view('content.customer.list')->with(['data' => $data]);
    }
    public function form(){
        $data['master']['category'] = UserCategory::all();
        $data['master']['province'] = Province::all();
        $data['master']['warehouse'] = Warehouse::all();
        if(isset($_GET['id'])){
            $data['data'] = User::where('id',$_GET['id'])->first();
        }
        
        return view('content.customer.form')->with(['data' => $data]);
    }
    public function detail($id){
        $data = User::where('id',$id)->first();
        $data['log'] = LogChange::where('module','customer')->where('module_id',$id)->orderby('id','DESC')->get();
        return view('content.customer.detail')->with(['data' => $data]);
    }
    public function change_status($id){
        $status = User::where('id', $id)->first()['status'];
        if($status == 'INACTIVE'){
            $status = 'ACTIVE';
        }elseif($status == 'ACTIVE'){
            $status = 'INACTIVE';
        }
        $input = User::where('id',$id)->update(['status' => $status]);
        LogHelper::add('customer', $id, 'ADMIN', Auth::id(), "Change Status Data Customer to ".$status);


        return redirect()->back();
    }
    public function mass_save(Request $request){
        if($request->mass){
            User::whereIn('id',$request->mass)->update(['status' => $request->action ]);
            foreach($request->mass as $d){
                LogHelper::add('customer', $d, 'ADMIN', Auth::id(), "change data user to ".$request->action." using mass action");
            }
        }
  		return redirect('customer');
    }

    public function save(Request $request){
        $password = Hash::make($request->input('password'));
        $field = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'type' => $request->type,
            'status' => $request->status,
            'phone' => $request->phone,
            'category_id' => $request->category_id,
            'address' => $request->address,
            'postal_code' => $request->postal_code,
            'city' => $request->city,
            'province' => $request->province,
            'gender' => $request->gender,
            'warehouse_id' => $request->warehouse_id,
        ];
        if($request->id){
            $validate = $request->validate([
                'email' => ['required','email',Rule::unique('users')->ignore($request->id)],
                'name' => 'required|max:100', 
                'type' => 'required',
                'status' => 'required',
                'category_id' => 'required',
                'address' => 'required',
                'postal_code' => 'required',
                'city' => 'required',
                'province' => 'required',
                'warehouse_id' => 'required',
            ]);
            $input = User::where('id',$request->id)->update($field);
            $id = $request->id;
            LogHelper::add('customer', $id, 'ADMIN', Auth::id(), "Change Data Customer");
        }else{
            $validate = $request->validate([
                'email' => 'unique:users|required|email|max:100',
                'name' => 'required|max:100',
                'type' => 'required',
                'password' => 'required',
                'status' => 'required',
                'category_id' => 'required',
                'address' => 'required',
                'postal_code' => 'required',
                'city' => 'required',
                'province' => 'required',
                'warehouse_id' => 'required',
            ]);
            $input = User::create($field);
            $input['shipping'] = UserShipping::create([
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'address' => $request->input('address'),
                'postal_code' => $request->input('postal_code'),
                'city' => $request->input('city'),
                'province' => $request->input('province'),
                'users_id' => $input['id']
            ]);
            $id = $input['id'];

            LogHelper::add('customer', $id, 'ADMIN', Auth::id(), "Create Data Customer");

        }
        
        if($request->input('password')!== null){
            $password = Hash::make($request->input('password'));
        }else{
            $password = Hash::make($request->input('email'));
        }
        $input = User::where('id',$id)->update(['password' => $password ]);

  		return redirect('customer/detail/'.$id);
    }
    
}
