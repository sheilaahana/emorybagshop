<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\Transaction;
use App\User;
use DB;

class DashboardController extends Controller
{
    public function index(){

        $last_week = date('Y-m-d' ,strtotime(now()."- 7 Days"));
        $graf = [];
        for($i=1;$i<=7;$i++){
            $date = date('Y-m-d' ,strtotime($last_week."+ ".$i." Days"));
            $count =  Transaction::select('created_at')->whereDate('created_at',$date)->count();
            $graf[] = ['x' => $date,  'y' => $count ];
        }


        $data['product_qty'] = Product::where('is_publish',1)->count();
        $data['user_qty'] = User::count();
        $data['transaction_qty'] = Transaction::count();
        $data['pesanan'] = Transaction::orderby('id','DESC')->limit(20)->get();
        $data['peringatan'] = ProductVariant::with('product')->where('qty_stock',"<=",10)->orderby('qty_stock','ASC')->limit(20)->get();
        $data['grafik'] = $graf;
        return view('content.dashboard.dashboard')->with(['data' => $data]);
    }
    public function ajax_data(){
        $last_week = date('Y-m-d' ,strtotime(now()."- 7 Days"));
        $data = [];
        for($i=1;$i<=7;$i++){
            $date = date('Y-m-d' ,strtotime($last_week."+ ".$i." Days"));
            $count =  Transaction::select('created_at')->whereDate('created_at',$date)->count();
            $data[] = ['x' => $date,  'y' => $count ];
        }
        return $data;
    }
    public function export_backup(){
        $data = DB::table('product_harga')->get();
        echo "<table border=1>";
        foreach($data as $d){
            $string = $d->product_name;
            // 1
            $string = substr($string, strpos($string, " ") + 1);
            $string = substr($string, strpos($string, " ") + 1);
            $string = substr($string, strpos($string, " ") + 1);
            $string = substr($string, strpos($string, " ") + 1);
            $string = substr($string, strpos($string, " ") + 1);
            $string = substr($string, strpos($string, " ") + 1);

            echo "<tr>";
            echo "<td>".$string."</td>";
            echo "<td>".$d->harga_jual."</td>";
            echo "</tr>";
        }
        echo "</table>";
    }
    public function export_2(){
        $data = DB::table('product_image')->get();
        echo "<table border=1>";
        foreach($data as $d){
            $string = $d->product_name;
            // 1
            $string = substr($string, strpos($string, " ") + 1);
            $string = substr($string, strpos($string, " ") + 1);
            $string = substr($string, strpos($string, " ") + 1);
            $string = substr($string, strpos($string, " ") + 1);
            $string = substr($string, strpos($string, " ") + 1);
            $string = substr($string, strpos($string, " ") + 1);

            echo "UPDATE product_image SET product_name = '".$string."' WHERE product_image.id = ".$d->id.";";
            echo "<br>";
            // echo "<tr>";
            // echo "<td>".$string."</td>";
            // echo "<td>".$d->image."".$d->ext."</td>";
            // echo "</tr>";
        }
        echo "</table>";
    }
    public function export_1(){
        $data = DB::table('product_harga_update')->get();
        foreach($data as $d){
            echo "UPDATE product SET harga_jual = '".$d->harga."' WHERE product.id = ".$d->id.";";
            echo "<br>";
        }
    }

    public function export_3(){
        $data = DB::table('relate_image')->where('idproduct',"!=","")->get();
        // $data = DB::table('relate_image')->get();
        foreach($data as $d){
            echo "UPDATE product SET image = 'public/product/".$d->image."".$d->ext."' WHERE product.id = ".$d->idproduct.";";
            echo "<br>";
        }
    }
    public function export(){
        $data = DB::table('product_variant')->get();
        // $data = DB::table('relate_image')->get();
        echo "<br>";

        foreach($data as $d){
            
           
        echo "INSERT INTO `log_stock` (`id`, `warehouse_id`, `product_id`, `variant_id`, `type`, `stock`, `description`, `change_by`, `change_id`, `created_at`, `updated_At`) VALUES";
        // echo " (NULL, '1', ".$d->product_id.", '".$d->id."', 'plus', '0', 'First Stock', 'SYSTEM', '', current_timestamp(), NULL);";
            // echo "<br>";
            echo " (NULL, '1', ".$d->product_id.", '".$d->id."', 'plus', '".$d->qty_stock."', 'Real Stock', 'SYSTEM', '', current_timestamp(), NULL);";
            echo "<br>";
        }
    }
    public function export_wh(){
        $data = DB::table('product_variant')->get();
        // $data = DB::table('relate_image')->get();
        echo "<br>";
        $nomor = 0;
        foreach($data as $d){
            $nomor++;
            echo "INSERT INTO `warehouse_detail` (`id`, `warehouse_id`, `product_id`, `variant_id`, `nomor_rak`, `baris`, `kolom`, `stock`, `catatan`, `created_at`, `updated_at`) VALUES ";
            echo "(NULL, '1', '".$d->product_id."', '".$d->id."', '-', '-', '-', '".$d->qty_stock."', '-', current_timestamp(), NULL);";
            echo "<br>";
        }
    }
}
