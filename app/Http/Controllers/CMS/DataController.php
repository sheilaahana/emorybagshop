<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\UserShipping;
use App\Models\Warehouse;
use App\Models\WarehouseDetail;
use App\User;
use App\Helpers\Responses as Res;
use App\Helpers\Generate;

class DataController extends Controller
{
    function product(){
        $data = Product::select('id','product_name','category_id')->get();
        return json_encode($data)   ;
    }
    function product_user($user){
        $warehouse_id = Generate::warehouse($user);
        $data = [];
        $id = [];
        $fid = [];
        $output = [];
        if($warehouse_id){
            $cekwh = Warehouse::where('id',$warehouse_id)->count();
            if($cekwh > 0){
                $wh = WarehouseDetail::select('product_id')->where('warehouse_id',$warehouse_id)->get();
                foreach($wh as $w){
                    $id[] = $w['product_id'];
                }
                $id= array_unique($id);
                foreach($id as $d){
                    $fid[] = $d;
                }
            }
        }
        $data = Product::whereIn('id',$fid)->get();
        foreach($data as $d){
            $output[] = ['code' => $d['id'], 'label' => $d['product_name']."-".$d['harga_jual'] ];
        }
        
        return json_encode($output);
    }
    function variant_product($id){

        $data = ProductVariant::where('product_id',$id)->get();
        return json_encode($data);
    }
    function variant_user($id,$user){
        $data = [];
        $cekuser = User::where('id',$user)->count();
        if($cekuser){
            $warehouse_id = Generate::warehouse($user);
            if($warehouse_id){
                $cekwh = Warehouse::where('id',$warehouse_id)->count();
                if($cekwh > 0){
                    $wd = WarehouseDetail::where('product_id',$id)->where('warehouse_id',$warehouse_id)->get();
                    foreach($wd as $w){
                        $pv = ProductVariant::where('id',$w['variant_id'])->first();
                        $pv['qty_stock'] = $w['stock'];
                        $data[] = $pv;
                    }
                }
            }
        }

        return json_encode($data);
    }
    function stock_user($id,$user){
        $data = [];
        $cekuser = User::where('id',$user)->count();
        if($cekuser){
            $warehouse_id = Generate::warehouse($user);
            if($warehouse_id){
                $cekwh = Warehouse::where('id',$warehouse_id)->count();
                if($cekwh > 0){
                    $wd = WarehouseDetail::where('variant_id',$id)->where('warehouse_id',$warehouse_id)->first();
                }
            }
        }

        return json_encode($wd['stock']);
    }
    function user_address($id){
        $data = UserShipping::where('users_id',$id)->get();

        return json_encode($data);
    }
    function address($id){
        $data = UserShipping::where('id',$id)->first();

        return json_encode($data);
    }
    function user(){
        $data = User::get();

        return $data;
    }
}
