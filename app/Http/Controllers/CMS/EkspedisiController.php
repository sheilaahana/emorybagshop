<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Helpers\LogHelper;
use Auth;

class EkspedisiController extends Controller
{
    public function index(){
        $data = Transaction::where('status_trx',"EKSPEDISI")->with('detail')->orderby('id','desc')->get();
        return view('content.ekspedisi.list')->with(['data' => $data]);
    }
    public function change(Request $request){
        if($request->resi){
            LogHelper::status($request->transaction_id, "SELESAI", "Generate by System, Change status after packing", "ADMIN", Auth::id());
            Transaction::where('id',$request->transaction_id)->update([
                'shipping_receipt' => $request->resi,
                'shipping_date' => $request->shipping_date,
                'shipping_time' => $request->shipping_time,
                'set_shipping' => date('Y-m-d'),
                'shipping_status' => "SUDAH KIRIM",
            ]);
        }
        
        return redirect('ekspedisi');
    }
}
