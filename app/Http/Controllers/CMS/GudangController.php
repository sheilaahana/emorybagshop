<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Helpers\LogHelper;
use Auth;


class GudangController extends Controller
{
    public function index(){
        $data = Transaction::where('status_trx',"GUDANG")->with('detail')->orderby('id','desc')->get();
        return view('content.gudang.list')->with(['data' => $data]);
    }
    public function change(Request $request){

        LogHelper::status($request->transaction_id, "EKSPEDISI", "Generate by System, Change status after packing", "ADMIN", Auth::id());
        Transaction::where('id',$request->transaction_id)->update([
            'gudang' => $request->gudang?:"-",
            'quality_control' => $request->qc?:"-",
            'packing' => $request->packing?:"-",
        ]);

        return redirect('gudang');
    }
}
