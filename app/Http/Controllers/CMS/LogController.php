<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Warehouse;
use App\Models\Product;
use App\Models\LogChange;
use App\Models\LogStock;
use Excel;
use App;
use App\Helpers\Report as Rep;


class LogController extends Controller
{
    public function index(){
        $data = LogChange::orderBy('id','DESC');
        if(isset($_GET['changeby'])){
            $data = $data->where('change_by',$_GET['changeby']);
        }elseif(isset($_GET['module'])){
            $data = $data->where('module',$_GET['module']);
        }
        $data = $data->get();
        return view('content.log.list')->with(['data' => $data]);
    }
    public function report(Request $request){
        $param = [];
        $module = LogChange::select('module')->groupby('module')->get();
        foreach($module as $m){
            $mod[] = $m['module'];
        }
        $helper['module'] = $mod;

        if(!empty($request->all())){
            foreach($request->all() as $a => $b){
                if($a != '_token'){
                    $param[$a] = $b;
                }
            }
            $data = Rep::log($param);
        }
        else{
            $data = LogChange::all();
        }
        return view('content.log.report')->with(['data' => $data,'helper' => $helper,'param' => $param]);
    }
    public function export(Request $request){
        $param = json_decode($request->param,true);
        $namafile = '';
        if(!empty($param)){
            foreach($param as $a => $b){
                if($a != '_token'){
                    if($b!==null){
                        $namafile .= "-".$a."_".$b;
                    }
                }
            }
        }else{
            $namafile .= " per ".date('d M Y');
        }  

        $excel = App::make('excel');
        Excel::create('Laporan Log Change'.$namafile, function($excel) use ($param)  {

            $excel->sheet('1', function($sheet) use ($param)  {

                $data = Rep::log($param);
                // $data = LogChange::orderBy('id','DESC')->get();

                $sheet->loadView('content.log.export')->with('data',$data);

            });

        })->export('xlsx');
    }
    public function stock(){
        $helper['product'] = Product::get();
        $helper['warehouse'] = Warehouse::get();
        $data['product'] = [];
        $varian = [];
        
        if(isset($_GET['product'])){
            $var = LogStock::select('variant_id')->where('product_id',$_GET['product'])->get();
            foreach($var as $v){
                $varian[$v['variant_id']] = $v['variant_id'];
            }
            $data['product'] = Product::where('id',$_GET['product'])->with(['variant' => function($a) use($varian) { $a->whereIn('id', $varian); }])->first();
        }
        $data['log'] = LogStock::orderby('created_at','ASC');
        if(isset($_GET['product'])){
            $data['log'] = $data['log']->where('product_id',$_GET['product']);
        }
        if(isset($_GET['warehouse'])){
            $data['log'] = $data['log']->where('warehouse_id',$_GET['warehouse']);
        }
        $data['log'] = $data['log']->get();
        return view('content.log.stock')->with(['data' => $data,'helper' => $helper]);
    }
}
