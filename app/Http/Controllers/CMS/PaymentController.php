<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PaymentTarget;
use App\Models\Transaction;
use Illuminate\Support\Facades\Storage;


class PaymentController extends Controller
{
    public function target(){
        $data = PaymentTarget::all();
        return view('content.paymenttarget.list')->with(['data' => $data]);
    }
    public function target_form(){
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $data = PaymentTarget::where('id',$_GET['id'])->first();
            return view('content.paymenttarget.form')->with(['data' => $data]);
        }else{
            return view('content.paymenttarget.form');
        }
    }
    public function target_save(Request $request){
        $field = [
            'bank_name' => $request->input('bank_name'),
            'name' => $request->input('name'),
            'number' => $request->input('number'),
        ];

        if($request->input('id')){
            $dt = PaymentTarget::where('id',$request->id);
            $img = $dt->first()['icon'];

            $id = $request->input('id');
            $input = PaymentTarget::where('id',$id)->update($field);

            if($request->file('icon')!== null){
                Storage::delete($img);
            }
        }else{
            $input = PaymentTarget::create($field);
            $id = $input['id'];
        }
        if($request->file('icon')!== null){
            $path = $request->file('icon')->store('public/assets');
            PaymentTarget::where('id',$id)->update(['bank_icon'=> $path]);
        }
        if($input){
            $message = "Data Saved";
        }else{
            $message = "Failed to Saved";
        }
        return redirect('payment/target')->with('message',$message);
    }    
    public function target_delete($id){
        $cek = Transaction::where('payment_target',$id)->count();
        if($cek>0){
            $message = "Can't delete, data used";
        }else{
            $cek2 = PaymentTarget::count();            
            if($cek == 1){
                $message = "Can't delete, at least we need 1 data";
            }else{
                PaymentTarget::where('id',$id)->delete();
                $message = "Data Deleted";
            }
        }
        return redirect('payment/target')->with('message',$message);
    }
}
