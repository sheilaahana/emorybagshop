<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Helpers\LogHelper;
use Auth;

class PrintController extends Controller
{
    public function index(){
        $data = Transaction::where('status_trx',"PRINT")->orderby('id','desc')->get();
        return view('content.print.list')->with(['data' => $data]);
    }
    public function change($id){
        LogHelper::status($id, "GUDANG", "Generate by System, Change status after print", "ADMIN", Auth::id());
        
        return redirect('print');
    }
}
