<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\UserCategory;
use App\Models\UserPrice;
use App\Models\ProductVariant;
use App\Models\TransactionDetail;
use App\Models\Cart;
use App\Models\LogChange;
use App\Helpers\LogHelper;
use App\Models\Wishlist;
use App\Models\Warehouse;
use App\Models\WarehouseDetail;
use App\Models\LogStock;

use Illuminate\Support\Facades\Storage;
use Auth;


class ProductController extends Controller
{
    public function index(){
        $data = Product::orderBy('id','DESC');
        if(isset($_GET['status']) && !empty($_GET['status'])){
            if($_GET['status'] == 'published'){
                $cek = 1;
            }elseif($_GET['status'] == 'unpublish'){
                $cek = 0;
            }
            if(isset($cek)){
                $data = $data->where('is_publish',$cek);
            }
        }
        $data = $data->get();

        return view('content.product.list')->with(['data' => $data]);
    }
    public function detail($id){
        $data = Product::where('id',$id)->first();
        $data['on_wishlist'] = Wishlist::where('product_id',$id)->count();
        $data['on_cart'] = Cart::where('product_id',$id)->count();
        $data['log'] = LogChange::where('module','product')->where('module_id',$id)->orderby('id','DESC')->get();
        return view('content.product.detail')->with(['data' => $data]);
    }
    public function warehouse_check($id){
        $data = Product::where('id',$id)->first();
        if(!$data){
            return redirect('product')->with('message','Invalid Data Product');
        }
        
        $data['warehouse_product'] = WarehouseDetail::where('product_id',$id)->with('variant')->orderby('warehouse_id','ASC')->get();
        $data['warehouse'] = Warehouse::where('id','!=','1')->get();
        return view('content.product.warehouse')->with(['data' => $data]);
    }
    public function form(){
        if(isset($_GET['id'])){
            $data = Product::where('id',$_GET['id'])->first();
            if(!empty($data['harga'])){
                $price = [];
                foreach($data['harga'] as $h){
                    $price[$h['id']] = ['harga_jual'=>$h['harga_jual'],'harga_diskon'=>$h['harga_diskon']];
                }
                $data['harga'] = $price;
            }
        }
        $data['productcategory'] = ProductCategory::all();
        $data['usercategory'] = UserCategory::all();
        return view('content.product.form')->with(['data' => $data]);
    }
    public function save(Request $request){
        $field = [
            'product_name' => $request->input('product_name'),
            'harga_modal' => $request->input('harga_modal'),
            'harga_jual' => $request->input('harga_jual'),
            'harga_diskon' => $request->input('harga_diskon'),
            'berat' => $request->input('berat'),
            'description' => $request->input('description'),
            'specification' => $request->input('specification'),
            'type_product' => $request->input('type_product'),
            'category_id' => $request->input('category_id'),
        ];

        if($request->input('id')){
            $id = $request->input('id');
            $dt = Product::where('id',$id);
            $img = $dt->first()['image_root'];

            $input = Product::where('id',$id)->update($field);

            if($request->file('image')!== null){
                Storage::delete($img);
            }
        }else{
            $field['is_show'] = 1;

            $input = Product::create($field);
            $id = $input['id'];
        }
        if($request->file('image')!== null){
            $path = $request->file('image')->store('public/product');
            Product::where('id',$id)->update(['image'=> $path]);
        }
            foreach($request->input('price') as $idprice => $price ){
                $cek = UserPrice::where('product_id',$id)->where('category_id',$idprice)->count();
                    if($cek>0){
                        UserPrice::where('product_id',$id)->where('category_id',$idprice)
                                    ->update([
                                        'harga_jual'=>$price['harga_jual'],
                                        'harga_diskon'=>$price['harga_diskon']
                                        ]);
                    }else{
                        UserPrice::create([
                                        'harga_jual'=>$price['harga_jual'],
                                        'harga_diskon'=>$price['harga_diskon'],
                                        'product_id'=>$id,
                                        'category_id'=>$idprice,
                                        ]);
                    }
            }

        if($input){
            $message = "Data Saved";
            LogHelper::add('product', $id, 'ADMIN', Auth::id(), "change data product detail");
        }else{
            $message = "Failed to Saved";
        }
        return redirect('product/detail/'.$id)->with('message',$message);
    }
    public function delete($id){
        $cek = TransactionDetail::where('product_id',$id)->count();
        $cek2 = ProductVariant::where('product_id',$id)->count();
        if($cek>0 || $cek2>0){
            $message = "Can't delete, data used";
        }else{
            $dt = Product::where('id',$id);
            $img = $dt->first()['image_root'];

            Storage::delete($img);
            Product::where('id',$id)->delete();
            LogHelper::add('product', $id, 'ADMIN', Auth::id(), "delete data product");

            $message = "Data Deleted";
        }

        return redirect('product')->with('message',$message);
    }
    public function change_status($field,$id){
    
        $status = Product::where('id', $id)->first()[$field];
        if($status == 1){
            $status = 0;
        }elseif($status == 0){
            $status = 1;
        }
        if($field == 'is_publish'){
            $cek = ProductVariant::where('product_id',$id)->count();
            if($cek==0){
                return redirect()->back()->with('message','Membutuhkan data variant paling tidak satu data');
            }
        }
        $input = Product::where('id',$id)->update([$field => $status]);
        LogHelper::add('product', $id, 'ADMIN', Auth::id(), "change data status ".$field." to ".$status);


        return redirect()->back();
    }
    public function variant_form(){
        if(isset($_GET['id'])){
            $data = ProductVariant::where('id',$_GET['id'])->first();
        }
        $data['productcategory'] = ProductCategory::all();
        $data['usercategory'] = UserCategory::all();
        return view('content.product.variant-form')->with(['data' => $data]);
    }
    public function mass_form($id){
        $data['variant'] = ProductVariant::where('product_id',$id)->get();
        $data['productcategory'] = ProductCategory::all();
        $data['usercategory'] = UserCategory::all();
        return view('content.product.mass-form')->with(['id' => $id,'data' => $data]);
    }
    public function mass_save(Request $request){
        if(!empty($request->p)){
            foreach($request->p as $id => $value){
                $idproduct = ProductVariant::where('id',$id)->first()['product_id'];
                $qty = ProductVariant::where('id',$id)->first()['qty_stock'];

                $berubah = $value['qty_stock'] - $qty;

                ProductVariant::where('id',$id)->update([
                    'variant' => $value['variant'],
                    'qty_stock' => $value['qty_stock'],
                    'hexa' => $value['hexa'],
                ]);

                if($berubah != 0){
                    if($berubah < 0){
                        $berubah = $berubah * -1;
                        LogHelper::stock_minus('1', $id,'ADMIN', Auth::id(), $berubah,'Update Stock Variant');
                    }else{
                        LogHelper::stock_plus('1', $id,'ADMIN', Auth::id(), $berubah,'Update Warehouse Stock');
                    }
                }
                LogHelper::warehouse_stock($id, $value['qty_stock']);

                if(isset($value['image'])){
                    $last = ProductVariant::select('image')->where('id',$id)->first()['image'];
                    if(!empty($last)){
                        Storage::delete($last);
                    }
                    $path = $value['image']->store('public/product');
                    ProductVariant::where('id',$id)->update(['image'=> $path]);
                }
            }
            LogHelper::add('product', $idproduct, 'ADMIN', Auth::id(), "change data variant ");

        }

        if(!empty($request->new)){
            $much = count($request->new['variant']);
            for($a=0;$a<$much;$a++){
                if($request->new['variant']!==null ){
                    echo "a";
                    if($request->new['qty_stock'][$a]!==null && $request->new['hexa'][$a]!==null ){
                        echo"b";
                        $input = ProductVariant::create([
                            'variant' => $request->new['variant'][$a],
                            'qty_stock' => $request->new['qty_stock'][$a],
                            'hexa' => $request->new['hexa'][$a],
                            'product_id' =>$request->product_id
                        ]);
                        LogHelper::stock_plus('1', $input['id'],'ADMIN', Auth::id(), $input['qty_stock'],'Add Stock Variant from Mass Input');
                        LogHelper::warehouse_stock($input['id'], $input['qty_stock']);

                        if(isset($request->new['image'][$a])){
                            $path = $request->new['image'][$a]->store('public/product');
                            ProductVariant::where('id',$input['id'])->update(['image'=> $path]);
                        }
                        LogHelper::add('product', $request->product_id, 'ADMIN', Auth::id(), "add new data variant ".$request->new['variant'][$a]);
                    }
                }
            }
        }
        // exit();

        return redirect('product/detail/'.$request->product_id)->with('message',"Data Saved");
    }
    public function variant_save(Request $request){
        $field = [
            'variant' => $request->input('variant'),
            'qty_stock' => $request->input('qty_stock'),
            'hexa' => $request->input('hexa'),
        ];

        if($request->input('id')!==null){
            $id = $request->input('id');

            $dt = ProductVariant::where('id',$id);
            $img = $dt->first()['image'];
            $product_id = $dt->first()['product_id'];
            $qty = $dt->first()['qty_stock'];

            $berubah = $field['qty_stock'] - $qty;
            $input = ProductVariant::where('id',$id)->update($field);

            if($berubah != 0){
                if($berubah < 0){
                    $berubah = $berubah * -1;
                    LogHelper::stock_minus('1', $id,'ADMIN', Auth::id(), $berubah,'Update Stock Variant');
                }else{
                    LogHelper::stock_plus('1', $id,'ADMIN', Auth::id(), $berubah,'Update Stock Variant');
                }
            }
            LogHelper::warehouse_stock($id, $field['qty_stock']);


            if($request->file('image')!== null){
                Storage::delete($img);
            }
            LogHelper::add('product', $product_id, 'ADMIN', Auth::id(), "change data variant ".$request->variant);

        }else{
            $field['product_id'] = $request->input('product_id');
            $input = ProductVariant::create($field);
            $id = $input['id'];
            $product_id = $input['product_id'];
            LogHelper::add('product', $product_id, 'ADMIN', Auth::id(), "add new data variant ".$request->variant);
            LogHelper::stock_plus('1', $input['id'],'ADMIN', Auth::id(), $input['qty_stock'],'Add Stock Variant from Input Variant');

            LogHelper::warehouse_stock($input['id'], $input['qty_stock']);

        }

        if($request->file('image')!== null){
            $path = $request->file('image')->store('public/product');
            ProductVariant::where('id',$id)->update(['image'=> $path]);
        }
        if($input){
            $message = "Data Saved";
        }else{
            $message = "Failed to Saved";
        }

        return redirect('product/detail/'.$product_id)->with('message',$message);
    }
    public function variant_delete($id){
        $cek = TransactionDetail::where('variant_id',$id)->count();
        if($cek>0){
            $message = "Can't delete, data used on Transaction. Change stock to  0";
        }else{
            $dt = ProductVariant::where('id',$id);
            $img = $dt->first()['image_root'];
            $data = $dt->first();

            LogHelper::stock_minus('1', $data['id'], 'ADMIN', Auth::id(), $data['qty_stock'],'Delete Stock Variant '.$data['variant'].' from Input Variant');
            ProductVariant::where('id',$id)->delete();
            Cart::where('variant_id',$id)->delete();
            WarehouseDetail::where('variant_id',$id)->delete();
            Wishlist::where('variant_id',$id)->delete();

            Storage::delete($img);


            $message = "Data Deleted";
            LogHelper::add('product', $data['product_id'], 'ADMIN', Auth::id(), "delete data variant ".$data['variant']);
        }
        return redirect()->back()->with('message',$message);
    }
    public function log_stock($id){
        $data['warehouse'] = Warehouse::with(['log_stock' => function($a){ $a->orderby('variant_id','asc')->orderby('created_at','asc'); } ])->get();
        return view('content.product.stock')->with(['data' => $data,'id'=>$id]);
    }
}
