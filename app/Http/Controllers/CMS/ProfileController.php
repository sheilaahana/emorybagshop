<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Helpers\LogHelper;
use Auth;
use DB;
use Redirect;
use App\Admin;

class ProfileController extends Controller
{
    public function index(){
        $data = Admin::where('id',Auth::id())->first();
        return view('content.profile.profile')->with(['data' => $data]);
	}
	public function save_profile(Request $request){

		$validate = $request->validate([
			'email' => ['required','email',Rule::unique('apps_admin')->ignore(Auth::id())],
			'name' => 'required|max:200',
		]);

		$input = Admin::where('id',Auth::id())->update([
			'name' => $request->input('name'),
			'email' => $request->input('email'),
		]);
		if ($input) {
			$message = "Data Terupdate";
            LogHelper::add('administrator', Auth::id(), 'ADMIN', Auth::id(), $request->name." Change Their profle ");
			
		}else{
			$message = "Failed to Update";
		}
    	return Redirect::back()->with('message-profile',$message);
	}
    public function save_password(Request $request)
    {

        $request->validate([
            'new_pass' => 'required',
            'repeat_pass' => 'required',
            'current_pass' => 'required',
        ]);

        $message = 'Tidak Terupdate';
    	$newpassword = $request->input('new_pass');
    	$repeatpassword = $request->input('repeat_pass');
    	$currentpassword = $request->input('current_pass');

    	$data = Admin::where('id',Auth::id());
    	$getdata = $data->first();
    	if (!empty($getdata)) {
    		$getdata= $getdata->toArray();
        	$pdata = DB::table('apps_admin')->where('id',Auth::id())->get();
    		$pass_data = $pdata[0]->password;

		    if (password_verify($currentpassword, $pass_data)) {
		        if ($newpassword == $repeatpassword ) {
		            $Update = $data->Update([
		                'password' => bcrypt($newpassword)
		            ]);
		            $message = "Password Baru Terupdate";
            		LogHelper::add('administrator', Auth::id(), 'ADMIN', Auth::id(), $request->name." Change Their password ");
		        }else{
		            $message = "Password Baru dan Konfirmasi tidak sama";
		        }
		    }else{
		        $message = "Password Lama Salah";
		    }
        }
    	return Redirect::back()->with('message-pass',$message);
    }
}