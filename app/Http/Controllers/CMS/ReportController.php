<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Report;
use App\Helpers\Report as Rep;
use Excel;
use App\Models\Courier;
use App\Models\Transaction;
use App\Models\Warehouse;
use App\Models\WarehouseDetail;
use App\Models\Product;
use App\Models\ProductCategory;
use App;
use PDF;

class ReportController extends Controller
{
    public function index(Request $request){
        $param = [];
        if(!empty($request->all())){
            $data = Rep::laporan($request->all());
            foreach($request->all() as $a => $b){
                if($a != '_token'){
                    $param[$a] = $b;
                }
            }
        }else{
            $data = Report::all();
        }
        return view('content.report.list')->with(['data' => $data,'param' => $param]);
    }
    public function export(Request $request){
        $param = json_decode($request->param,true);
        $namafile = '';
        if(!empty($param)){
            foreach($param as $a => $b){
                if($a != '_token'){
                    if($b!==null){
                        $namafile .= "-".$a."_".$b;
                    }
                }
            }
        }else{
            $namafile .= " per ".date('d M Y');
        }  

        $excel = App::make('excel');
        Excel::create('Laporan Pemesanan'.$namafile, function($excel) use ($param)  {

            $excel->sheet('1', function($sheet) use ($param)  {

                $data = Rep::laporan($param);

                $sheet->loadView('content.report.table')->with('data',$data);

            });

        })->export('xlsx');
    }
    public function daily(Request $request){
        if(!empty($request->date)){
            $data['date'] = $request->date;
        }else{
            $data['date'] = date('Y-m-d');
        }
        $data['courier'] = Courier::get();
        $data['warehouse'] = Warehouse::get();

        $m = [];
        foreach($data['courier'] as $c){
            foreach($data['warehouse'] as $w)   {
                $a = Transaction::select('courier','shipping_status','warehouse_id')->where('courier',$c['courier'])->whereDate('created_at',$request->date)->where('shipping_status','SUDAH KIRIM')->where('warehouse_id',$w['id'])->count();
                $m[$c['courier']][$w['id']] = $a;
            }
            
        }
        $data['report'] = $m;
        return view('content.report.list-daily')->with(['data' => $data]);
    }
    public function daily_export(Request $request){
        if(!empty($request->date)){
            $data['date'] = $request->date;
        }else{
            $data['date'] = date('Y-m-d');
        }
        $data['courier'] = Courier::get();
        $data['warehouse'] = Warehouse::get();
        $data['tanggal'] = "2019-09-30";
        $data['tanggal'] = $request->date;
        $m = [];
        foreach($data['courier'] as $c){
            foreach($data['warehouse'] as $w)   {
                $a = Transaction::select('courier','shipping_status','warehouse_id')->where('courier',$c['courier'])->whereDate('created_at',$request->date)->where('shipping_status','SUDAH KIRIM')->where('warehouse_id',$w['id'])->count();
                $m[$c['courier']][$w['id']] = $a;
            }
            
        }
        $data['report'] = $m;

        // return $data;
        $namafile = $request->date ." per ".date('d M Y');

        $excel = App::make('excel');
        Excel::create('Report Warehouse '.$namafile, function($excel) use ($data)  {

            $excel->sheet('1', function($sheet) use ($data)  {

                $sheet->loadView('content.report.daily')->with(['data' => $data]);

            });

        })->export('xlsx');
    }
    public function stock(Request $request){
        $param = [];
        $param['warehouse'] = 1;
        $data['master']['warehouse'] = Warehouse::all();
        $data['master']['category'] = ProductCategory::all();
        
        $product = Product::with(['variant' => function($q) use ($param) { $q->with(['warehouse' => function($b) use ($param) { $b->where('warehouse_id',$param['warehouse']); }]); }])->orderby('id','DESC');
        if(!empty($_GET['warehouse']) && $_GET['warehouse'] != '1'){
            $stock = [];
            $product_id= [];
            $w = WarehouseDetail::where('warehouse_id',$_GET['warehouse'])->get();
            foreach($w as $ware){
                $dc[] = $ware['product_id'];
                $stock[$ware['product_id']][$ware['variant_id']] = $ware['stock'];
            }
            if(!empty($dc)){
                $dc = array_unique($dc);
                foreach($dc as $pid => $p){
                    $product_id[] = $p;
                }
            }

            $product = $product->whereIn('id',$product_id);
            $data['stock'] = $stock;
            $param['warehouse'] = $_GET['warehouse'];
        }else{
            if(!empty($_GET['category'])){
                $product = $product->where('category_id',$_GET['category']);
            }
        }
        $data['product'] = $product->get();
        

        return view('content.report.stock')->with(['data' => $data,'param' => $param]);
    }
    public function export_stock(Request $request){
        $param = [];
        $param = json_decode($request->param,true);
        $param['warehouse'] = 1;
        $namafile = date('d-M-y h:i:s');
        $product = Product::with('variant')->orderby('id','DESC');

        if(!empty($param)){
            $stock = [];
            $product_id= [];
            $w = WarehouseDetail::where('warehouse_id',$param['warehouse'])->get();
            foreach($w as $ware){
                $dc[] = $ware['product_id'];
                $stock[$ware['product_id']][$ware['variant_id']] = $ware['stock'];
            }
            if(!empty($dc)){
                $dc = array_unique($dc);
                foreach($dc as $pid => $p){
                    $product_id[] = $p;
                }
            }

            $product = $product->whereIn('id',$product_id);
            $data['stock'] = $stock;
            $param['warehouse'] = $param['warehouse'];
        }else{
            if(!empty($param['category'])){
                $product = $product->where('category_id',$param['category']);
            }
        }  
        $data['product'] = $product->get();
        // return view('content.report.export-stock')->with(['data' => $data,'param' => $param]);

        Excel::create('Laporan Sisa Stock'.$namafile, function($excel) use ($param,$data)  {

            $excel->sheet('1', function($sheet) use ($param, $data)  {


                $sheet->loadView('content.report.export-stock')->with(['data' => $data, 'param' => $param]);

            });

        })->export('xlsx');
    }
    public function handler(Request $request){
        $param = [];
        if(!empty($request->all())){
            $data = Rep::summary($request->all());
            foreach($request->all() as $a => $b){
                if($a != '_token'){
                    $param[$a] = $b;
                }
            }
        }else{
            $data = Rep::summary();
        }
        return view('content.report.handler')->with(['data' => $data,'param' => $param]);
    }
    public function handler_export(Request $request){
        $param = json_decode($request->param,true);
        $namafile = '';
        if(!empty($param)){
            foreach($param as $a => $b){
                if($a != '_token'){
                    if($b!==null){
                        $namafile .= "-".$a."_".$b;
                    }
                }
            }
        }else{
            $namafile .= " per ".date('d M Y');
        }  

        $excel = App::make('excel');
        Excel::create('Laporan Handler'.$namafile, function($excel) use ($param)  {

            $excel->sheet('1', function($sheet) use ($param)  {

                $data = Rep::summary($param);

                $sheet->loadView('content.report.handler-table')->with('data',$data);

            });

        })->export('xlsx');
    }
}

