<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\TransactionRetur;
use App\Helpers\LogHelper;
use App\Helpers\Generate;
use Auth;

class ReturController extends Controller
{
    public function list(){
        if(isset($_GET['filter'])){
            $data = TransactionRetur::where('status',$_GET['filter'])->orderby('id','desc')->get();
        }else{
            $data = TransactionRetur::orderby('id','desc')->get();
        }
        return view('content.retur.list')->with(['data' => $data]);
    }
    public function retur_form($id){
        $data = Transaction::where('id',$id)->first();
        return view('content.retur.retur-form')->with(['data' => $data]);
    }
    public function retur_detail($id){
        $data = Transaction::where('id',$id)->first();
        return view('content.retur.retur-detail')->with(['data' => $data]);
    }
    public function retur_status($status,$id){
        $status = strtoupper($status);
        if(in_array($status,['PROCESS','SUCCESS','DECLINE'])){
            TransactionRetur::where('transaction_id',$id)->update(['status' => $status]);
            LogHelper::add('transaction', $id, 'ADMIN', Auth::id(), "Change Retur Status to ".$status);
        }
        return redirect('transaction/retur/detail/'.$id);
    }
    public function retur_save(Request $request){
        $cek = TransactionRetur::where('transaction_id',$request->transaction_id)->count();
        $data = Transaction::where('id',$request->transaction_id)->first();
        if($cek > 0){
            $input = TransactionRetur::where('transaction_id',$request->transaction_id)->update([
                'reason' => $request->reason,
                'request_type' => $request->request_type,
            ]);
            LogHelper::add('transaction', $request->transaction_id, 'ADMIN', Auth::id(), "Change Retur Request Detail");
        }else{
            $input = TransactionRetur::create([
                'transaction_id' => $request->transaction_id,
                'invoice_number' => $data['invoice_number'],
                'reason' => $request->reason,
                'request_type' => $request->request_type,
                'status' => 'PROCESS',
            ]);
            LogHelper::add('transaction', $request->transaction_id, 'ADMIN', Auth::id(), "Create Retur Request to ". $request->type);
        }
        if ($request->hasFile('image_1')) {
            $path = $request->file('image_1')->store('public/retur');
            TransactionRetur::where('transaction_id',$request->transaction_id)->update(['image_1'=>$path]);
        }
        if ($request->hasFile('image_2')) {
            $path = $request->file('image_2')->store('public/retur');
            TransactionRetur::where('transaction_id',$request->transaction_id)->update(['image_2'=>$path]);
        }
        if ($request->hasFile('image_3')) {
            $path = $request->file('image_3')->store('public/retur');
            TransactionRetur::where('transaction_id',$request->transaction_id)->update(['image_3'=>$path]);
        }
        return redirect('transaction/retur/detail/'.$request->transaction_id)->with('message',"Request Retur Updated");

    }
    public function new_invoice(Request $request){
        $message = "Invalid Parameter";
        $cek = TransactionRetur::where('transaction_id',$request->transaction_id)->count();
        if($cek > 0){
            $cek2 = Transaction::where('invoice_number', $request->invoice_number)->count();
            if($cek2 > 0){
                if($request->invoice_number){
                    $d = Transaction::where('invoice_number',$request->invoice_number);
                    $cek2 = $d->count();
                    if($cek2 > 0){
                        $data = $d->first();
                        $update = TransactionRetur::where('transaction_id',$request->transaction_id)->update([
                            'new_invoice_number' => $request->invoice_number,
                            'new_transaction_id' => $data['id']
                        ]);
                        $message = "Data Updated";
                        LogHelper::add('transaction', $request->transaction_id, 'ADMIN', Auth::id(), "Set New Invoice Number to ". $request->invoice_number);

                    }else{
                        $message = "Invalid Invoice Number";
                    }
                }
            }else{
                return redirect('transaction/retur/detail/'.$request->transaction_id)->with('message','Invalid Invoice Number');
            }
        }
        return redirect('transaction/retur/detail/'.$request->transaction_id)->with('message',$message);

    }
    public function new_transaction($id){
        $cek = Transaction::where('id',$id)->count();
        if($cek > 0){
            $cek2 = TransactionRetur::where('transaction_id',$id)->first();
            if(empty($cek2['new_transaction_id'])){
            
                $data = Transaction::where('id',$id)->first();

                $input_transaksi = Transaction::create([
                    'invoice_number' => Generate::invoice(),
                    'users_id' => $data['users_id'],
                    'email' => $data['email'],
                    'nama_penerima' => $data['nama_penerima'],
                    'telpon_penerima' => $data['telpon_penerima'],
                    'alamat_penerima' => $data['alamat_penerima'],
                    'city_origin' => $data['city_origin'],
                    'city_destination' => $data['city_destination'],
                    'total' => $data['total'],
                    'payment_target' => $data['payment_target'],
                    'jenis_pesanan' => $data['jenis_pesanan'],
                    'shipping_status' => "BELUM KIRIM",
                    'shipping_service' => $data['shipping_service'],
                    'payment_status' => $data['payment_status'],
                ]);
                foreach($data['detail'] as $det){
                    $input_detail = TransactionDetail::create([
                        'transaction_id' => $input_transaksi['id'],
                        'product_id' => $det['product_id'],
                        'variant_id' => $det['variant_id'],
                        'price' => $det['price'],
                        'qty' => $det['qty'],
                        'memo' => $det['memo'],
                    ]);
                }
                LogHelper::add('transaction', $input_transaksi['id'], 'ADMIN', Auth::id(), "Generate Transaction from Retur Request ".$data['invoice_number']);

                TransactionRetur::where('transaction_id',$id)->update([
                    'new_invoice_number' => $input_transaksi['invoice_number'],
                    'new_transaction_id' => $input_transaksi['id'],
                ]);

                LogHelper::add('transaction', $id, 'ADMIN', Auth::id(), "Generate New Transaction to new Invoice ".$input_transaksi['invoice_number']);

                return redirect('transaction/retur/detail/'.$id)->with('message',"Data updated");
            }
            return redirect('transaction/retur/detail/'.$id)->with('message',"Sudah Pernah Ganti Transaksi");
        }


    }
}
