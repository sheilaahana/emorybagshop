<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Generate as Generate;
use App\Helpers\Responses as Res;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Models\Province;
use App\Models\Courier;
use Auth;

class ShippingController extends Controller
{
    public function cost(Request $request){
        $data['master']['province'] = Province::all();
        $data['master']['courier'] = Courier::all();

        if($request->all()){
            $kurir = strtolower($request->courier);
            $client = new Client(); //GuzzleHttp\Client
            $result = $client->post('https://pro.rajaongkir.com/api/cost', [
                'form_params' => [
                    'origin' => $request->city_origin?:Generate::cityorigin(),
                    'destination' => $request->city_destination,
                    'weight' => $request->input('weight')?:1,
                    'courier' => $kurir?:'jne',
                    'originType' => 'city',
                    'destinationType' => 'city'
                ],
                'headers' => [
                    'key' => '178ccaf77e12458548df4bac71c2ded6'
                ]
            ]);
            $result = json_decode($result->getBody(), true);
            
            $data['result'] = $result['rajaongkir'];
        }
        

        return view('content.shipping.cost')->with(['data' => $data]);

        // return Res::output($data);
    }
    public function resi(Request $request){
        $data['master']['courier'] = Courier::all();
        $data['error']['resi'] = "";
        $data['error']['courier'] = "";
        if($request->all()){
            $kurir = strtolower($request->courier);
            try {
                $client = new Client(); //GuzzleHttp\Client
                $result = $client->post('https://pro.rajaongkir.com/api/waybill', [
                    'form_params' => [
                        'courier' => $kurir?:'jne',
                        'waybill' => $request->resi,
                    ],
                    'headers' => [
                        'key' => '178ccaf77e12458548df4bac71c2ded6'
                    ]
                ]);
                $result = json_decode($result->getBody(), true);
                $data['result'] = $result['rajaongkir'];

            } catch (RequestException $e) {
                
                $response = $e->getResponse();
                echo (string)($response->getBody());
            } catch (\Exception $e) {
                if ($e->getResponse()->getStatusCode() == '400') {
                    $result = "Resi not Found";
                    $data['error']['resi'] = $request->resi;
                    $data['error']['courier'] = $request->courier;
                }
            }
            
        }

        return view('content.shipping.resi')->with(['data' => $data]);
    }
}
