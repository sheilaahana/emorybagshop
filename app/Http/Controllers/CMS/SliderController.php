<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Support\Facades\Storage;


class SliderController extends Controller
{
    public function index(){
        $data = Slider::all();
        return view('content.slider.list')->with(['data' => $data]);
    }
    public function form(){
        if(isset($_GET['id']) && !empty($_GET['id'])){
            $data = Slider::where('id',$_GET['id'])->first();
            return view('content.slider.form')->with(['data' => $data]);
        }else{
            return view('content.slider.form');
        }
    }
    public function save(Request $request){
        $field = [
            'name' => $request->input('name'),
            'heading' => $request->input('heading'),
            'description' => $request->input('description'),
            'cta_1_text' => $request->input('cta_1_text'),
            'cta_1_link' => $request->input('cta_1_link'),
            'cta_1_target' => $request->input('cta_1_target'),
            'cta_2_text' => $request->input('cta_2_text'),
            'cta_2_link' => $request->input('cta_2_link'),
            'cta_2_target' => $request->input('cta_2_target'),
            'image' => $request->input('image'),
        ];

        if($request->input('id')){
            $dt = Slider::where('id',$request->id);
            $img = $dt->first()['image_root'];

            $id = $request->input('id');
            $input = Slider::where('id',$id)->update($field);

            if($request->file('image')!== null){
                Storage::delete($img);
            }
        }else{
            $field['is_show'] = 1;

            $input = Slider::create($field);
            $id = $input['id'];
        }
        if($request->file('image')!== null){
            $path = $request->file('image')->store('public/assets');
            Slider::where('id',$id)->update(['image'=> $path]);
        }
        if($input){
            $message = "Data Saved";
        }else{
            $message = "Failed to Saved";
        }
        return redirect('slider')->with('message',$message);
    }    
    public function delete($id){
        $cek = Slider::count();
        if($cek<=1){
            $message = "Can't delete, At least we need 1 data slider";
        }else{
            $dt = Slider::where('id',$id);
            $img = $dt->first()['image_root'];

            Slider::where('id',$id)->delete();
            Storage::delete($img);
            $message = "Data Deleted";
        }
        return redirect('slider')->with('message',$message);
    }
    public function change_status($id){
        $status = Slider::where('id', $id)->first()['is_show'];
        if($status == 1){
            $status = 0;
        }elseif($status == 0){
            $status = 1;
        }
        $input = Slider::where('id',$id)->update(['is_show' => $status]);

        return redirect()->back();
    }
}
