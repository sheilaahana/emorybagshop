<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\TransactionStatus;
use App\Models\TransactionDetail;
use App\Helpers\LogHelper;
use App\Models\LogChange;
use App\Helpers\Generate;
use App\Helpers\WalletHelper;
use App\Helpers\ProductGenerate  as Prod;
use App\Helpers\Responses  as Res;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\Courier;
use App\Models\WarehouseDetail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\User;
use Auth;
use Validator;
use PDF;
use Excel;
use App;


class TransactionController extends Controller
{
    public function index(){
        if(empty($_GET['status'])){
            $data = Transaction::orderby('id','desc')->where('status_trx','<>','ARCHIVED');
        }else{
            $data = Transaction::orderby('id','desc');
        }
        if(!empty($_GET['filter'])){
            $data = $data->where('payment_status',$_GET['filter']);
        }
        if(!empty($_GET['date'])){
            $data = $data->whereDate('created_at',$_GET['date']);
        }
        if(!empty($_GET['status'])){
            $data = $data->where('status_trx',$_GET['status']);
        }
        $data = $data->get();
        
        return view('content.transaction.list')->with(['data' => $data]);
    }
    public function detail($id){
        $cek = Transaction::where('id',$id)->count();
        if($cek > 0){
            $data = Transaction::where('id',$id)->with(['detail' => function($a){ $a->with(['product','variant']); } ])->first();
            $data['log'] = LogChange::where('module','transaction')->where('module_id',$id)->orderby('id','DESC')->get();
            if($data['shipping_receipt'] != ''){
                try {
                    $kurir = strtolower($data['courier']);
                    $client = new Client(); //GuzzleHttp\Client
                    $result = $client->post('https://pro.rajaongkir.com/api/waybill', [
                        'form_params' => [
                            'courier' => $kurir?:'jne',
                            'waybill' => $data['shipping_receipt'],
                        ],
                        'headers' => [
                            'key' => '178ccaf77e12458548df4bac71c2ded6'
                        ]
                    ]);
                    $result = json_decode($result->getBody(), true);
                    $data['tracking'] = $result['rajaongkir'];

                } catch (RequestException $e) {
                    
                    $response = $e->getResponse();
                    echo (string)($response->getBody());
                } catch (\Exception $e) {
                    if ($e->getResponse()->getStatusCode() == '400') {
                        // return $e->getResponse()->getStatusCode();
                        $data['tracking'] = [];
                    }
                }
            }
            return view('content.transaction.detail')->with(['data' => $data]);
        }
        else{
            return redirect()->back();
        }
    }
    public function form(){
        $data['user'] = json_encode(User::all());
        $data['product'] = Product::all();
        $data['courier'] = Courier::all();
        return view('content.transaction.form')->with(['data' => $data]);
    }
    public function payment($status,$id){
        $stat = ['UNPAID','PAID','REJECTED','EXPIRED','PAYLATER'];
        if(in_array(strtoupper($status),$stat)){
            $cekbefore = Transaction::where('id',$id)->first()['payment_status'];
            $input = Transaction::where('id',$id)->update([
                    'payment_status' => $status,
                    'confirmed_date' => date('Y-m-d'),
                ]);
            if($input){
                if(in_array(strtoupper($status),['REJECTED','EXPIRED'])){
                    if(in_array(strtoupper($cekbefore),['UNPAID','PAID','PAYLATER'])){
                        $data = Transaction::where('id',$id)->first();
                        foreach($data['detail'] as $det){
                            if($data['warehouse_id'] == '1'){
                                $prodvar = ProductVariant::where('id',$det['variant_id']);
                                $qtyvar = $prodvar->first()['qty_stock'] + $det['qty'];
                                $prodvar->update(['qty_stock' => $qtyvar]);
                            }
                            $prodvar = WarehouseDetail::where('variant_id',$det['variant_id'])->where('warehouse_id',$data['warehouse_id']);
                            $qtyvar = $prodvar->first()['stock'] + $det['qty'];
                            $prodvar->update(['stock' => $qtyvar]);
                        
                            LogHelper::stock_plus($data['warehouse_id'], $det['variant_id'],'ADMIN', Auth::id(), $det['qty'],'Rejected transaction '.$data['invoice_number']);
                        }
                        LogHelper::status($id, "GAGAL", "Generate by System, Change status payment to ".$status, "ADMIN", Auth::id());
                    }
                }else{
                    if(in_array(strtoupper($cekbefore),['REJECTED','EXPIRED'])){
                        $data = Transaction::where('id',$id)->first();
                        foreach($data['detail'] as $det){
                            if($data['warehouse_id'] == '1'){
                                $prodvar = ProductVariant::where('id',$det['variant_id']);
                                $qtyvar = $prodvar->first()['qty_stock'] - $det['qty'];
                                $prodvar->update(['qty_stock' => $qtyvar]);
                            }
                            $prodvar = WarehouseDetail::where('variant_id',$det['variant_id'])->where('warehouse_id',$data['warehouse_id']);
                            $qtyvar = $prodvar->first()['stock'] - $det['qty'];
                            $prodvar->update(['stock' => $qtyvar]);
                            
                            LogHelper::stock_minus($data['warehouse_id'], $det['variant_id'],'ADMIN', Auth::id(), $det['qty'],'Valid transaction from before Rejection '.$data['invoice_number']);
                        }
                    }
                    LogHelper::status($id, "PRINT", "Generate by System, Change status payment to ".$status, "ADMIN", Auth::id());
                }
                $message = "Data Saved";
                LogHelper::add('transaction', $id, 'ADMIN', Auth::id(), "Changed transaction payment status to ".$status);
            }else{
                $message = "Failed to Saved";
            }
        }else{
            $message = "invalid status";
        }

        return redirect()->back()->with('message',$message);
    }
    public function shipping($status,$id){
        $stat = ['SUDAH','BELUM','GAGAL','TERKIRIM'];
        if(in_array(strtoupper($status),$stat)){
            if($status=='belum'){
                $change = 'BELUM KIRIM';
            }
            if($status=='sudah'){
                $change = 'SUDAH KIRIM';
            }
            if($status=='gagal'){
                $change = 'GAGAL KIRIM';
            }
            if($status=='terkirim'){
                $change = 'TERKIRIM';
            }
            $input = Transaction::where('id',$id)->update(['shipping_status' => $change]);

            if($input){
                $message = "Data Saved";
                LogHelper::add('transaction', $id, 'ADMIN', Auth::id(), "Changed transaction shipping status to ".$change);
            }else{
                $message = "Failed to Saved";
            }
        }else{
            $message = "invalid status";
        }

        return redirect()->back()->with('message',$message);
    }
    public function delete_transaction($id){
        LogHelper::status($id, 'ARCHIVED', "Archived Transaction", "ADMIN", Auth::id());
        return redirect()->back();
    }
    public function save_transaction(Request $request)
    {
        // $request = json_encode($request);
        // $request->validate([
        //     'users_id' => 'required',
        //     'nama_penerima' => 'required',
        //     'telpon_penerima' => 'required',
        //     'alamat_penerima' => 'required',
        //     'product' => 'required',
        //     'variant' => 'required',
        //     'catatan' => 'required',
        //     'qty' => 'required',
        //     'total_product' => 'required',
        // ]);
        $validator = Validator::make($request->all(), [ 
            'users_id' => 'required',
            'nama_penerima' => 'required',
            'telpon_penerima' => 'required',
            'alamat_penerima' => 'required',
            'jenis_pesanan' => 'required',
            'shipping_status' => 'required',
            'shipping_service' => 'required',
            'payment_status' => 'required',
            'product' => 'required',
            'variant' => 'required',
            // 'catatan' => 'required',
            'qty' => 'required',
            'total_product' => 'required',
        ]); 
        if ($validator->fails()) { 
           return Res::valid($validator->errors());            
        }
        // return  $request->all();
        $isi = [
            'users_id' => $request->users_id,
            'nama_penerima' => $request->nama_penerima,
            'telpon_penerima' => $request->telpon_penerima,
            'alamat_penerima' => $request->alamat_penerima,
            'jenis_pesanan' => $request->jenis_pesanan,
            'shipping_status' => $request->shipping_status,
            'courier' => $request->courier,
            'shipping_service' => $request->shipping_service,
            'ongkos_kirim' => $request->ongkos_kirim,
            'payment_status' => $request->payment_status,
            'product' => $request->product,
            'variant' => $request->variant,
            'catatan' => $request->catatan,
            'city_destination' => $request->city_destination,
            'qty' => $request->qty,
            'total_product' => $request->total_product,
            'notes' => $request->notes,
            'nama_pengirim' => isset($request->nama_pengirim) && !empty($request->nama_pengirim) ? $request->nama_pengirim : Generate::setting('nama_pengirim'),
            'kontak_pengirim' => isset($request->kontak_pengirim) && !empty($request->kontak_pengirim) ? $request->kontak_pengirim : Generate::setting('kontak_pengirim'),
        ];

        $user = User::where('id',$request->users_id)->first();

        if($isi['total_product']>=1){
            for($i=1;$i<=$isi['total_product'];$i++){
                if($isi['variant'][$i]==null || $isi['variant'][$i] == ""){
                        return Res::error(400,'invalid stock');     
                }
            }
        }

        if($isi['total_product']>=1){
            $input_transaksi = Transaction::create([
                'invoice_number' => Generate::invoice(),
                'users_id' => $user['id'],
                'email' => $user['email'],
                'nama_penerima' => $isi['nama_penerima'],
                'telpon_penerima' => $isi['telpon_penerima'],
                'alamat_penerima' => $isi['alamat_penerima'],
                'nama_pengirim' => $isi['nama_pengirim'],
                'kontak_pengirim' => $isi['kontak_pengirim'],
                'notes' => $isi['notes'],
                'city_origin' => Generate::cityorigin(),
                'city_destination' => $isi['city_destination'],
                'ongkos_kirim' => $isi['ongkos_kirim'],
                'total' => 0,
                'payment_target' => 1,
                'jenis_pesanan' => $isi['jenis_pesanan'],
                'shipping_status' => $isi['shipping_status'],
                'courier' => $isi['courier'],
                'shipping_service' => $isi['shipping_service'],
                'payment_status' => $isi['payment_status'],
                'warehouse_id' => Generate::warehouse($user['id'])
                
            ]);     
            $total_price = 0;
            $getpoint = WalletHelper::add_point($input_transaksi['id']);
            for($i=1;$i<=$isi['total_product'];$i++){
                if($isi['product'][$i]!==null && $isi['variant'][$i]!==null){
                    if(isset($isi['catatan'][$i])){
                        $catatan = $isi['product'][$i];
                    }else{
                        $catatan = "";
                    }
                    $input_detail = TransactionDetail::create([
                        'transaction_id' => $input_transaksi['id'],
                        'product_id' => $isi['product'][$i],
                        'variant_id' => $isi['variant'][$i],
                        'price' => Prod::harga($isi['product'][$i]),
                        'qty' => $isi['qty'][$i],
                        'memo' => $catatan,
                    ]);
                    $total_price = $total_price+(Prod::harga($isi['product'][$i])*$isi['qty'][$i]);

                    if(Generate::warehouse($user['id']) == '1'){
                        $prodvar = ProductVariant::where('id',$isi['variant'][$i]);
                        $qtyvar = $prodvar->first()['qty_stock'] - $isi['qty'][$i];
                        $prodvar->update(['qty_stock' => $qtyvar]);
                    }

                    $prodvar = WarehouseDetail::where('variant_id',$isi['variant'][$i])->where('warehouse_id',Generate::warehouse($user['id']));
                    $qtyvar = $prodvar->first()['stock'] - $isi['qty'][$i];
                    $prodvar->update(['stock' => $qtyvar]);
                    

                    LogHelper::stock_minus(Generate::warehouse($user['id']),  $isi['variant'][$i], 'SYSTEM', "", $isi['qty'][$i], 'Manual '.$user['id'].'Checkout '. $input_transaksi['invoice_number']);

                }
            }

            Transaction::where('id',$input_transaksi['id'])->update(['total'=>$total_price]);
            $getpoint = WalletHelper::add_point($input_transaksi['id']);
        }
        LogHelper::add('transaction', $input_transaksi['id'], 'ADMIN', Auth::id(), "Add new transaction manual invoice : ".$input_transaksi['invoice_number']);

        $data = Transaction::with('detail')->where('id',$input_transaksi['id'])->first();
        return Res::save($data);
    }
    public function nota($type,$id){
        LogHelper::add('transaction', $id, 'ADMIN', Auth::id(), "Clicked Print Generate ". $type);
        $data = Transaction::where('id',$id)->with(
                    ['detail' => function($d){ 
                        $d->with(['variant' => function($v){
                            $v->with('warehouse');
                        }
                        ,'product']); 
                    },
                    'warehouse',
                    'city'=>function($c){ 
                        $c->with('province'); 
                    }])->first();
        $data['print'] = Auth::user();

        switch ($type) {
            case 'bill':
                $pdf = PDF::loadView('pdf.bill', ['data' => $data] );
            break;
            case 'premium':
                $pdf = PDF::loadView('pdf.premium', ['data' => $data] );
            break;
            
            default:
                # code...
                break;
        }
        return $pdf->stream();
    }
    public function update(){
        if(!empty($_GET['invoice'])){
            $data = Transaction::where('invoice_number',$_GET['invoice'])->first();
        }else{
                $data = [];
        }
        return view('content.transaction.update')->with(['data' => $data]);
    }
    public function update_save(Request $request){
        $message = "failed save";
        foreach ($request->all() as $a => $b){
            if(!in_array($a,['_token','id'])){
                if($b){
                    $cek = Transaction::select($a)->where('id',$request->id)->first()[$a];
                    if($cek != $b){
                        if(!$cek){
                            $cek = "undefinded";
                        }
                        Transaction::where('id',$request->id)->update([$a => $b]);
                        LogHelper::add('transaction', $request->id, 'ADMIN', Auth::id(), "Update ".$a." from ".$cek ." to ". $b);
                        if($a == 'shipping_status'){
                            if($b == "SUDAH KIRIM"){
                                LogHelper::status($request->id, 'SELESAI', "Generate by System, change shipping status use Update Transaction", "ADMIN", Auth::id());
                            }
                        }
                    }
                    $message = "Data Saved, check details <a href='". url('transaction/detail/'.$request->id) ."> Click Here</a>";
                }
            }
        }
        $data = Transaction::where('id',$request->id)->first();
        return redirect('transaction/update?invoice='.$data['invoice_number']);
    }
    public function daily(Request $request){
        if(!empty($request->date)){
            $param['date'] = $request->date;
        }else{
            $param['date'] = date('Y-m-d');
        }
        $data = Transaction::whereDate('created_at',$param['date'])->get();

        return view('content.transaction.daily')->with(['data' => $data,'param'=>$param]);  
    }
    public function daily_export(Request $request){
        if(!empty($request->date)){
            $param['date'] = $request->date;
        }else{
            $param['date'] = date('Y-m-d');
        }
        $data = Transaction::whereDate('created_at',$param['date'])->get();
        // return $data;
        $namafile = $request->date ." per ".date('d M Y');

        $excel = App::make('excel');
        Excel::create('Daily Transaction '.$namafile, function($excel) use ($data)  {

            $excel->sheet('1', function($sheet) use ($data)  {

                $sheet->loadView('content.transaction.table-daily')->with('data',$data);

            });

        })->export('xlsx');
    }
    public function change_status(Request $request){

        LogHelper::status($request->transaction_id, $request->status, $request->description, "ADMIN", Auth::id());
        return redirect()->back();
    }
}
