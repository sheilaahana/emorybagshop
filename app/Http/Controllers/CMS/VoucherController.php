<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Voucher;
use App\Models\VoucherRedeem;
use App\Models\Product;
use App\Models\LogChange;
use Auth;
use App\Helpers\LogHelper;
use App\User;
use App\Models\ProductCategory;
use Validator;

class VoucherController extends Controller
{
    public function index(){
        $data = Voucher::all();
        return view('content.voucher.list')->with(['data' => $data]);
    }
    public function detail($id){
        $data = Voucher::where('id',$id)->first();
        $data['log'] = LogChange::where('module','voucher')->where('module_id',$id)->orderby('id','DESC')->get();
        return view('content.voucher.detail')->with(['data' => $data]);
    }
    public function form(){
        $data = [];
        $helper['product'] = Product::select('id','product_name')->get();
        $helper['category'] = ProductCategory::select('id','category_name')->get();
        $helper['user'] = User::select('id','name','email')->get();
        $helper['tag'] = ['is_promo','is_bestseller','is_hotitem','is_mostwanted'];

        if(isset($_GET['id']) && !empty($_GET['id'])){
            $data = Voucher::where('id',$_GET['id'])->first();
            $data['start_date'] = date('Y-m-d', strtotime($data['start_date']));
            $data['end_date'] = date('Y-m-d', strtotime($data['end_date']));
        }
        return view('content.voucher.form')->with(['data' => $data,'helper'=>$helper]);

    }
    public function save(Request $request){
        $validator = $request->validate([ 
            'code' => 'required|unique:voucher', 
            'discount_type' => 'required', 
            'value' => 'required', 
            'type' => 'required', 
            'start_date' => 'required', 
            'end_date' => 'required', 
            'validity_user' => 'required', 
            'validity_general' => 'required', 
            'minimal_spend' => 'required', 
            'maximal_spend' => 'required', 
        ]); 

            $request->product_include = json_encode($request->product_include);
            $request->product_exclude = json_encode($request->product_exclude);
            $request->category_include = json_encode($request->category_include);
            $request->category_exclude = json_encode($request->category_exclude);
            $request->tag_include = json_encode($request->tag_include);
            $request->tag_exclude = json_encode($request->tag_exclude);
            $request->whitelist_user = json_encode($request->whitelist_user);
            $request->blacklist_user = json_encode($request->blacklist_user);

        $field = [
            'code' => $request->code,
            'discount_type' => $request->discount_type,
            'value' => $request->value,
            'type' => $request->type,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'validity_user' => $request->validity_user,
            'validity_general' => $request->validity_general,
            'minimal_spend' => $request->minimal_spend,
            'maximal_spend' => $request->maximal_spend,
            'product_include' => $request->product_include,
            'product_exclude' => $request->product_exclude,
            'category_include' => $request->category_include,
            'category_exclude' => $request->category_exclude,
            'tag_include' => $request->tag_include,
            'tag_exclude' => $request->tag_exclude,
            'whitelist_user' => $request->whitelist_user,
            'blacklist_user' => $request->blacklist_user,
            'is_valid' => $request->is_valid,
            'is_join' => $request->is_join,
        ];

        if($request->id){
            $id = $request->id;
            $input = Voucher::where('id',$id)->update($field);
            LogHelper::add('voucher', $id, 'ADMIN', Auth::id(), "change data voucher");
        }else{
            $input = Voucher::create($field);
            $id = $input['id'];
            LogHelper::add('voucher', $id, 'ADMIN', Auth::id(), "add new data voucher");
        }
        if(isset($request->product_include) && !empty($request->product_include) ){
            $product_include = preg_replace('/\\\\\"/',"\"",$field['product_include']);
            Voucher::where('id',$id)->update(['product_include'=> $product_include]);
        }
        if(isset($request->product_exclude) && !empty($request->product_exclude) ){
            $product_exclude = preg_replace('/\\\\\"/',"\"",$field['product_exclude']);
            Voucher::where('id',$id)->update(['product_exclude'=> $product_exclude]);
        }
        if(isset($request->category_include) && !empty($request->category_include) ){
            $category_include = preg_replace('/\\\\\"/',"\"",$field['category_include']);
            Voucher::where('id',$id)->update(['category_include'=> $category_include]);
        }
        if(isset($request->category_exclude) && !empty($request->category_exclude) ){
            $category_exclude = preg_replace('/\\\\\"/',"\"",$field['category_exclude']);
            Voucher::where('id',$id)->update(['category_exclude'=> $category_exclude]);
        }
        if(isset($request->tag_include) && !empty($request->tag_include) ){
            $tag_include = preg_replace('/\\\\\"/',"\"",$field['tag_include']);
            Voucher::where('id',$id)->update(['tag_include'=> $tag_include]);
        }
        if(isset($request->tag_exclude) && !empty($request->tag_exclude) ){
            $tag_exclude = preg_replace('/\\\\\"/',"\"",$field['tag_exclude']);
            Voucher::where('id',$id)->update(['tag_exclude'=> $tag_exclude]);
        }
        if(isset($request->whitelist_user) && !empty($request->whitelist_user) ){
            $whitelist_user = preg_replace('/\\\\\"/',"\"",$field['whitelist_user']);
            Voucher::where('id',$id)->update(['whitelist_user'=> $whitelist_user]);
        }
        if(isset($request->blacklist_user) && !empty($request->blacklist_user) ){
            $blacklist_user = preg_replace('/\\\\\"/',"\"",$field['blacklist_user']);
            Voucher::where('id',$id)->update(['blacklist_user'=> $blacklist_user]);
        }
        
        if($input){
            $message = "Data Saved";
        }else{
            $message = "Failed to Saved";
        }
        return redirect('voucher/detail/'.$id)->with('message',$message);
    }    
    public function delete($id){
        $cek = VoucherRedeem::where('voucher_id',$id)->count();
        if($cek<=1){
            $message = "Can't delete, data used";
        }else{
            Voucher::where('id',$id)->delete();
            LogHelper::add('voucher', $id, 'ADMIN', Auth::id(), "change data voucher");
            $message = "Data Deleted";
        }
        return redirect('voucher')->with('message',$message);
    }
    public function change_status($data,$id){
        if($data=='active'){
            $field = 'is_valid';
        }
        if($data=='combine'){
            $field = 'is_join';
        }
        $status = Voucher::where('id', $id)->first()[$field];
        if($status == 1){
            $status = 0;
        }elseif($status == 0){
            $status = 1;
        }
        $input = Voucher::where('id',$id)->update([$field => $status]);
        LogHelper::add('voucher', $id, 'ADMIN', Auth::id(), "change data status ".$field." to ".$status);

        return redirect()->back();
    }
}
