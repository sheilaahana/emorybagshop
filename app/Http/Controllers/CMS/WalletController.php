<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wallet;
use App\Models\WalletUsers;
use App\User;
use App\Helpers\WalletHelper;
use App\Helpers\LogHelper;
use Auth;

class WalletController extends Controller
{
    public function index(){
        $data = WalletUsers::get();
        return view('content.wallet.list')->with(['data' => $data]);
    }
    public function detail($id){
        $data['wallet'] = Wallet::where('users_id',$id)->get();
        $data['users'] = User::where('id',$id)->first();
        return view('content.wallet.detail')->with(['data' => $data]);
    }
    public function form(){
        $helper['product'] = Product::select('id','product_name')->get();
        $helper['category'] = ProductCategory::select('id','category_name')->get();

        if(isset($_GET['id']) && !empty($_GET['id'])){
            $data = Wallet::where('id',$_GET['id'])->first();
            return view('content.wallet.form')->with(['data' => $data,'helper'=>$helper]);
        }else{
            return view('content.wallet.form');
        }
    }
    public function save(Request $request){
        $input = WalletHelper::add('ADMIN','WALLET',$request->all());
        
        if($input){
            $message = "Data Saved";
            LogHelper::add('customer', $request->users_id, 'ADMIN', Auth::id(), "added new wallet record ");
        }else{
            $message = "Failed to Saved";
        }
        return redirect('wallet/detail/'.$request->users_id)->with('message',$message);
    }    
}
