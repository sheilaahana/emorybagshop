<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WarehouseProduct;
use App\Models\WarehouseLog;
use App\Models\WarehouseDetail;
use App\Models\Warehouse;
use App\Models\Product;
use App\Models\LogChange;
use App\Models\ProductVariant;
use App\Helpers\LogHelper;
use Validator;
use Auth;
use App\Helpers\Responses  as Res;
use App\User;


class WarehouseController extends Controller
{
    public function index(){
        $data = Warehouse::get();
        return view('content.warehouse.list')->with(['data' => $data]);
    }
    public function detail($id){
        $data = Warehouse::where('id',$id)->first();
        $data['product'] = WarehouseProduct::where('warehouse_id',$id)->get();
        $data['log'] = LogChange::where('module','warehouse')->where('module_id',$id)->orderby('id','DESC')->get();
        return view('content.warehouse.detail')->with(['data' => $data]);
    }
    public function form(){
        $data = [];
        if(isset($_GET['id'])){            
            $data = Warehouse::where('id',$_GET['id'])->first();
        }
        return view('content.warehouse.form')->with(['data' => $data]);
    }
    public function form_product($id){
        $data = [];
        if(isset($id)){            
            $data = Warehouse::where('id',$id)->first();
        }
        $data['warehouse_id'] = $id;
        return view('content.warehouse.form-product')->with(['data' => $data]);
    }
    public function log_retur($id){
        $data = ProductVariant::with('warehouselog')->where('id',$id)->first();
        return view('content.warehouse.log')->with(['data' => $data]);
    }
    
    public function log_save(Request $request){
        $field = [
            'variant_id' => $request->variant_id,
            'qty' => $request->qty,
            'memo' => $request->memo,
            'type' => $request->type,
            'location_origin' => $request->location_origin,
            'location_destination' => $request->location_destination,
            'input_by' => Auth::id()
        ];
        $last = ProductVariant::where('id',$request->variant_id)->first()['qty_stock'];
        if($request->type=="PLUS"){
            $ganti = $last+$request->qty;
        }elseif($request->type=="MINUS"){
            $ganti = $last-$request->qty;
        }else{
            $ganti = $last;
        }
        ProductVariant::where('id',$request->variant_id)->update(['qty_stock' => $ganti]);

        $input = WarehouseLog::create($field);
        
        if($input){
            $message = "Data saved";
            LogHelper::add('log-retur', $request->variant_id, 'ADMIN', Auth::id(), "Changed log retur data to ".json_encode($field));
        }else{
            $message = "Data failed to save";
        }


        return redirect('warehouse/log/'.$request->variant_id)->with('message',$message);
    }
    public function save(Request $request){
        $field = [
            "warehouse" => $request->warehouse,
            "code" => $request->code,
            "location" => $request->location,
            "catatan" => $request->catatan,
        ];

        if($request->id){
            $cek = Warehouse::where('id',$request->id)->count();
            if($cek > 0){
                $input = Warehouse::where('id',$request->id)->update($field);
            }
            $id = $request->id;
            LogHelper::add('warehouse', $id, 'ADMIN', Auth::id(), "Changed warehouse data");

        }else{
            $input = Warehouse::create($field);
            $id = $input['id'];
            LogHelper::add('warehouse', $id, 'ADMIN', Auth::id(), "Add new warehouse data");
        }
        if($input){
            $message = "Data saved";
        }else{
            $message = "Data failed to save";
        }


        return redirect('warehouse/detail/'.$id)->with('message',$message);
    }
    public function save_detail(Request $request){
        $validator = Validator::make($request->all(), [ 
            'warehouse_id' => 'required',
            'product_id' => 'required',
            'variant_id' => 'required',
            // 'catatan' => 'required',
            'stock' => 'required',
            'nomor_rak' => 'required',
            'baris' => 'required',
            'kolom' => 'required',
        ]); 
        if ($validator->fails()) { 
           return Res::valid($validator->errors());            
        }
        $update = WarehouseDetail::where('warehouse_id',$request->warehouse_id)->where('product_id',$request->product_id)->where('variant_id',$request->variant_id);
        $cek = $update->count();
        if($cek>0){
            $last = $update->first()['stock'];
            $berubah = $request->stock - $last;

            $input = $update->update([
                'stock' => $request->stock,
                'nomor_rak' => $request->nomor_rak,
                'baris' => $request->baris,
                'kolom' => $request->kolom,
                'catatan' => $request->catatan,
            ]);

            if($berubah != 0){
                if($berubah < 0){
                    $berubah = $berubah * -1;
                    LogHelper::stock_minus($request->warehouse_id, $request->variant_id,'ADMIN', Auth::id(), $berubah,'Update Warehouse Stock');
                }else{
                    LogHelper::stock_plus($request->warehouse_id, $request->variant_id,'ADMIN', Auth::id(), $berubah,'Update Warehouse Stock');
                }
            }

            if($input){
                $message = "Data Saved";
                if($request->warehouse_id == 1){
                    LogHelper::master_stock($request->variant_id, $request->stock);
                }
                LogHelper::add('warehouse', $request->warehouse_id, 'ADMIN', Auth::id(), "Edit Detail stock variant ".$request->variant_id." to stock ".$request->stock.", nomor_rak ".$request->nomor_rak.", baris ".$request->baris.", kolom ".$request->kolom.", Notes ".$request->catatan );
            }else{
                $message = "Data failed to save";
            }
        }
        return redirect('warehouse/detail/'.$request->warehouse_id)->with('message',$message);

    }
    public function save_product(Request $request){
        $validator = Validator::make($request->all(), [ 
            'product' => 'required',
            'variant' => 'required',
            // 'catatan' => 'required',
            'stock' => 'required',
            'nomor_rak' => 'required',
            'baris' => 'required',
            'kolom' => 'required',
            'total_product' => 'required',
        ]); 
        if ($validator->fails()) { 
           return Res::valid($validator->errors());            
        }
        $isi = [
            'product' => $request->product,
            'variant' => $request->variant,
            'catatan' => $request->catatan,
            'stock' => $request->stock,
            'nomor_rak' => $request->nomor_rak,
            'baris' => $request->baris,
            'kolom' => $request->kolom,
            'total_product' => $request->total_product,
        ];
        if($isi['total_product']>=1){
            for($i=1;$i<=$isi['total_product'];$i++){
                if($isi['product'][$i]!==null && $isi['variant'][$i]!==null){
                    $cek = WarehouseDetail::where('warehouse_id',$request->id)->where('variant_id',$isi['variant'][$i])->count();
                    if($cek == 0){
                        $input_detail = WarehouseDetail::create([
                            "warehouse_id" => $request->id,
                            "product_id" => $isi['product'][$i],
                            "variant_id" => $isi['variant'][$i],
                            "stock" => $isi['stock'][$i],
                            "nomor_rak" => $isi['nomor_rak'][$i],
                            "baris" => $isi['baris'][$i],
                            "kolom" => $isi['kolom'][$i],
                            "catatan" => $isi['catatan'][$i],
                        ]);
                        LogHelper::stock_plus($request->id, $isi['variant'][$i],'ADMIN', Auth::id(), $isi['stock'][$i],'Add Warehouse Stock');
                    }else{
                        $last = WarehouseDetail::where('warehouse_id',$request->id)->where('variant_id',$isi['variant'][$i])->first()['stock'];
                        $berubah = $isi['stock'][$i] - $last;
                        
                        $input_detail = WarehouseDetail::where('warehouse_id',$request->id)->where('variant_id',$isi['variant'][$i])->update([
                            "product_id" => $isi['product'][$i],
                            "variant_id" => $isi['variant'][$i],
                            "stock" => $isi['stock'][$i],
                            "nomor_rak" => $isi['nomor_rak'][$i],
                            "baris" => $isi['baris'][$i],
                            "kolom" => $isi['kolom'][$i],
                            "catatan" => $isi['catatan'][$i],
                        ]);
                        if($berubah != 0){
                            if($berubah < 0){
                                $berubah = $berubah * -1;
                                LogHelper::stock_minus($request->id, $isi['variant'][$i],'ADMIN', Auth::id(), $berubah,'Update Warehouse Stock');
                            }else{
                                LogHelper::stock_plus($request->id, $isi['variant'][$i],'ADMIN', Auth::id(), $berubah,'Update Warehouse Stock');
                            }
                        }
                        if($request->id == 1){
                            LogHelper::master_stock($isi['variant'][$i], $isi['stock'][$i]);
                        }
                    }
                }
            }
        }
        LogHelper::add('warehouse', $request->id, 'ADMIN', Auth::id(), "Add new product stock");

        $data = Warehouse::where('id',$request->id)->first();
        return Res::save($data);
    }
    public function delete_warehouse($id){
        $cek = WarehouseDetail::where('warehouse_id',$id)->count();
        $cek2 = User::where('warehouse_id',$id)->count();
        if($cek == 0 && $cek2==0){
            $delete = Warehouse::where('id',$id)->delete();
            LogHelper::add('warehouse', $id, 'ADMIN', Auth::id(), "Delete warehouse data");
            $message = "Deleted Data";
        }else{
            $message = "Can't Delete, Data used";
        }
        return redirect('warehouse')->with('message',$message);
    }
    public function save_backup(Request $request){
        $cek = Warehouse::where('product_id',$request->product_id)->count();
        $field = [
            "product_id" => $request->product_id,
            "location" => $request->location,
            "nomor_rak" => $request->nomor_rak,
            "baris" => $request->baris,
            "kolom" => $request->kolom,
            "catatan" => $request->catatan,
        ];

        if($cek == 0 ){
            $input = Warehouse::create($field);
        }else{
            $input = Warehouse::where('product_id',$request->product_id)->update($field);
        }
        if($input){
            $message = "Data saved";
            LogHelper::add('warehouse', $request->product_id, 'ADMIN', Auth::id(), "Changed warehouse data to ".json_encode($field));
        }else{
            $message = "Data failed to save";
        }


        return redirect('warehouse')->with('message',$message);
    }
    public function transfer_stock($id){
        $data = WarehouseProduct::where('id',$id)->first();
        $data['wh'] = Warehouse::where('id', '!=', $data['warehouse_id'])->get();
        return view('content.warehouse.transfer')->with(['data' => $data]);
    }
    public function transfer_save(Request $request){
        $validator = Validator::make($request->all(), [ 
            'warehouse_origin' => 'required',
            'detail_id' => 'required',
            'warehouse_destination' => 'required',
            'stock' => 'required',
        ]); 
        if ($validator->fails()) { 
           return Res::valid($validator->errors());            
        }
        $detail = WarehouseDetail::where('id',$request->detail_id)->first();
        $origin = Warehouse::where('id',$request->warehouse_origin)->first();
        $destination = Warehouse::where('id',$request->warehouse_destination)->first();

        if($request->stock <= $detail['stock']){
            $stockorigin = $detail['stock']-$request->stock;
            if($stockorigin < 0){
                $message = " Can not Transfer Stock, Invalid Stock";
                return redirect('warehouse/detail/'.$request->warehouse_origin)->with(['message' => $message]);
            }
            $des = WarehouseDetail::where('warehouse_id',$request->warehouse_destination)->where('variant_id',$detail['variant_id']);
            $cekdes = $des->count();
            if($cekdes > 0){ // kalau udah ada barangnya di wh tsbt
                    $stockawal = $des->first()['stock'];
                    $stocktransfer = $stockawal+$request->stock;
                    $update = $des->update(['stock' => $stocktransfer ]);
            }else{
                $update = WarehouseDetail::create([
                    'warehouse_id' => $request->warehouse_destination,
                    'product_id' => $detail['product_id'],
                    'variant_id' => $detail['variant_id'],
                    'nomor_rak' => "transfer from ".$origin['code'],
                    'baris' => "transfer from ".$origin['code'],
                    'kolom' => "transfer from ".$origin['code'],
                    'catatan' => $request->catatan,
                    'stock' => $request->stock,
                ]);
            }
            if($request->warehouse_destination == '1'){
                LogHelper::master_stock($detail['variant_id'], $stocktransfer);
            }
            if($request->warehouse_origin == '1'){
                LogHelper::master_stock($detail['variant_id'], $stockorigin);
            }
            LogHelper::stock_minus($request->warehouse_origin, $detail['variant_id'],'ADMIN', Auth::id(), $request->stock,'Transfer Stock to '.$destination['warehouse']);
            LogHelper::stock_plus($request->warehouse_destination, $detail['variant_id'],'ADMIN', Auth::id(), $request->stock,'Get Stock Transfer from '.$origin['warehouse']);

            if($update){
                WarehouseDetail::where('id',$request->detail_id)->update(['stock' => $stockorigin ]);
                LogHelper::add('warehouse', $request->warehouse_origin, 'ADMIN', Auth::id(), "Transfer stok to warehouse ". $destination['warehouse']);
                LogHelper::add('warehouse', $request->warehouse_destination, 'ADMIN', Auth::id(), "Get Transfer stok from warehouse ". $origin['warehouse']);

            }

        }

        return redirect('warehouse/detail/'.$request->warehouse_origin);
    }
    
}
