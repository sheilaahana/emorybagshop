<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Helpers\Generate;
use App\Models\User;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email|unique:users', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
            'type' => 'required', 
        ]); 
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'type' => $request->type,
            'password' => Hash::make($request->password),
            'status' => 'INACTIVE',
        ]);

        $success['token'] =  $user->createToken('MyApp')->accessToken; 
        return response()->json(['success'=>$success ,'data' =>$user], $this->successStatus); 
    }
}
