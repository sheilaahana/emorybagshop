<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppsModules extends Model
{
    public $table = "apps_modules";  

    public function child()
    {
        return $this->hasMany('App\Models\AppsModules', 'parent_id', 'id');
    }
}
