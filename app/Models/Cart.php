<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $table = "cart";
    public $fillable = [
        'product_id','users_id','memo','variant_id','qty'
    ];

    public $primaryKey = 'id';

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
    public function users()
    {
        return $this->hasOne('App\User', 'id', 'users_id');
    }
    public function variant()
    {
        return $this->hasOne('App\Models\ProductVariant', 'id', 'variant_id');
    }
}
