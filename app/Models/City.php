<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $table = "apps_city";
    public $primaryKey = 'id';

    public function city()
    {
        return $this->BelongsTo('App\Models\Province', 'province_id', 'id');
    }
    public function province()
    {
        return $this->BelongsTo('App\Models\Province', 'province_id', 'id');
    }
}
