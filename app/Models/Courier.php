<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    public $table = "apps_courier";
    public $primaryKey = 'id';
}
