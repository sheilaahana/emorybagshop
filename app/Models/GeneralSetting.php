<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralSetting extends Model
{
    public $table = "apps_setting";
    public $primaryKey = 'id';
    protected $casts = [
        'jasa_pengiriman' => 'array',
    ];
}
