<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Admin;

class LogChange extends Model
{
    public $table = "log_change";
    public $fillable = [
        'module',
        'module_id',
        'description',
        'change_by',
        'change_id',
    ];
    protected $appends = [
        'admin'
    ];

    public function getAdminAttribute()
    {
        $name = "";
        $d = Admin::where('id',$this->attributes['change_id'])->first();
        if($d){
            $name = $d['name'];
        }

        return $name;
    }
}
