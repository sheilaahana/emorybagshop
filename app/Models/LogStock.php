<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Admin;

class LogStock extends Model
{
    public $table = "log_stock";
    public $fillable = [
        'warehouse_id',
        'product_id',
        'variant_id',
        'type',
        'stock',
        'description',
        'change_by',
        'change_id',
    ];
    protected $appends = [
        'admin'
    ];

    public function getAdminAttribute()
    {
        $name = "";
        if(!empty($this->attributes['change_id'])){
            $d = Admin::where('id',$this->attributes['change_id'])->first();
            if($d){
                $name = $d['name'];
            }
        }
        

        return $name;
    }
    public function warehouse()
    {
        return $this->hasOne('App\Models\Warehouse', 'id', 'warehouse_id');
    }
    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
    public function variant()
    {
        return $this->hasOne('App\Models\ProductVariant', 'id', 'variant_id');
    }
}
