<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class PaymentTarget extends Model
{
    public $table = "payment_target";
    public $fillable = [
        'bank_name','bank_icon','name','number'
    ];

    public $primaryKey = 'id';

    public function getBankIconAttribute($value)
    {
        return url(Storage::url($value));
    }
}
