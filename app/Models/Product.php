<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserPrice;
use App\Models\Wishlist;
use App\Models\ProductCategory;
use Auth;
use App\Helpers\Appsrole as Apps;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{

    public $table = "product";
    public $fillable = [
        'product_name',
        'harga_modal',
        'harga_jual',
        'harga_diskon',
        'berat',
        'description',
        'specification',
        'image',
        'count_seen',
        'is_promo',
        'is_bestseller',
        'is_hotitem',
        'is_mostwanted',
        'is_publish',
        'type_product',
        'category_id',
    ];

    public $primaryKey = 'id';

    protected $appends = [
        'category_name',
        'is_wishlist',
        'image_root'
    ];

    public function category()
    {
        return $this->hasOne('App\Models\ProductCategory', 'id', 'category_id');
    }
    public function warehouse()
    {
        return $this->hasOne('App\Models\Warehouse', 'product_id', 'id');
    }
    public function variant()
    {
        return $this->hasMany('App\Models\ProductVariant', 'product_id', 'id');
    }
    public function harga()
    {
        return $this->hasMany('App\Models\UserPrice', 'product_id', 'id');
    }
    public function getImageAttribute($value)
    {
        return url(Storage::url($value));
    }
    public function getImageRootAttribute()
    {
        if(isset($this->attributes['image'])){
            return $this->attributes['image'];
        }
    }
    public function getCategoryNameAttribute()
    {
        return ProductCategory::select('category_name')->where('id',$this->attributes['category_id'])->first()['category_name'];
    }
    public function getIsWishlistAttribute()
    {
        if (Auth::check()) {
            $cek = Wishlist::where('users_id',Auth::id())->where('product_id',$this->attributes['id'])->count();
            if($cek > 0){
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
        
    }
    public function getHargaJualAttribute($value)
    {
        if (Auth::check() && Auth::user()->category_id != null) {
            $dat = UserPrice::select('harga_jual')->where('product_id',$this->attributes['id'])->where('category_id',Auth::user()->category_id)->first();
            return $dat['harga_jual']?:$value;
        }else{
            return $value;
        }
        
    }
    public function getHargaDiskonAttribute($value)
    {
        if (Auth::check() && Auth::user()->category_id != null ) {
            $dat = UserPrice::select('harga_diskon')->where('product_id',$this->attributes['id'])->where('category_id',Auth::user()->category_id)->first();
            return $dat['harga_diskon']?:$value;
        }else{
            return $value;
        }
        
    }
}
