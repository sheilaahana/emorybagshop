<?php

namespace App\Models;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{

    public $table = "product_category";
    public $fillable = [
        'category_name','type','description','icon'
    ];

    public $primaryKey = 'id';

    public function credential()
    {
        return $this->hasOne('App\UserCredentials', 'users_id', 'id');
    }
    public function getIconAttribute($value)
    {
        return url(Storage::url($value));
    }
}
