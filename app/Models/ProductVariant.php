<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\ProductGenerate as Prod;
use Illuminate\Support\Facades\Storage;
use App\Models\Cart;
use Auth;


class ProductVariant extends Model
{
    public $table = "product_variant";
    public $fillable = [
        'variant','qty_stock','image','hexa','product_id'
    ];
    public $primaryKey = 'id';

    protected $appends = [
        'valid_stock',
        'on_cart',
        'is_wishlist',
        'image_root',
    ];
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
    public function warehouse()
    {
        return $this->hasMany('App\Models\WarehouseDetail', 'variant_id', 'id');
    }
    public function warehouselog()
    {
        return $this->hasMany('App\Models\WarehouseLog', 'variant_id', 'id');
    }
    public function getIsWishlistAttribute()
    {
        if (Auth::check()) {
            $cek = Wishlist::where('users_id',Auth::id())->where('variant_id',$this->attributes['id'])->count();
            if($cek > 0){
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
        
    }
    public function getImageAttribute($value)
    {
        return url(Storage::url($value));
    }
    public function getOnCartAttribute()
    {
        $data = Cart::where('variant_id',$this->attributes['id'])->sum('qty');
        return $data;
    }
    public function getImageRootAttribute()
    {
        if(isset($this->attributes['image'])){
            return $this->attributes['image'];
        }
    }
    public function getValidStockAttribute()
    {
        return Prod::stock($this->attributes['id']);
    }
}
