<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public $table = "apps_province";
    public $primaryKey = 'id';

    public function city()
    {
        return $this->hasMany('App\Models\City', 'province_id', 'id');
    }
}
