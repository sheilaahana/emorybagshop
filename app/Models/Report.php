<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public $table = "report";
    
    public function handler()
    {
        return $this->hasOne('App\Models\Transaction', 'id', 'id');
    }
}
