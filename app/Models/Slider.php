<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class Slider extends Model
{
    public $table = "apps_slider";
    public $primaryKey = 'id';
    public $fillable = [
        'name',
        'heading',
        'description',
        'cta_1_text',
        'cta_1_link',
        'cta_1_target',
        'cta_2_text',
        'cta_2_link',
        'cta_2_target',
        'image',
        'is_show',
    ];
    protected $appends = [
        'image_root'
    ];

    public function getImageAttribute($value)
    {
        return url(Storage::url($value));
    }
    public function getImageRootAttribute()
    {
        if(isset($this->attributes['image'])){
            return $this->attributes['image'];
        }
    }
}
