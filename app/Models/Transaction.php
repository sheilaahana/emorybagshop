<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\City;
use App\Models\TransactionRetur;
use App\Models\Province;

class Transaction extends Model
{
    public $table = "transaction";
    public $primaryKey = 'id';
    public $fillable = [
        'users_id',
        'invoice_number',
        'cashless_order',
        'email',
        'nama_penerima',
        'telpon_penerima',
        'alamat_penerima',
        'kodepos',
        'city_origin',
        'city_destination',
        'courier',
        'shipping_service',
        'ongkos_kirim',
        'total',
        'get_point',
        'payment_method',
        'payment_status',
        'payment_receipt',
        'payment_target',
        'confirmation_date',
        'shipping_status',
        'shipping_receipt',
        'shipping_time',
        'shipping_date',
        'jenis_pesanan',
        'warehouse_id',
        'customer_service',
        'quality_control',
        'gudang',
        'packing',
        'notes',
        'nama_pengirim',
        'kontak_pengirim',
        'confirmed_date',
        'change_gudang',
        'change_shipping',
        'set_shipping',
    ];
    protected $appends = [
        'city_name',
        'is_retur'
    ];
    public function getIsReturAttribute()
    {
        $data = TransactionRetur::where('transaction_id',$this->attributes['id'])->count();

        if($data > 0){
            return "true";
        }else{
            return "false";
        }
    }
    public function getCityNameAttribute()
    {
        if(isset($this->attributes['city_destination'])){
            $data = City::where('id',$this->attributes['city_destination'])->first();
            return $data['type']." ".$data['city'];
        }
    }
    public function city(){
        return $this->hasOne('App\Models\City','id','city_destination');
    }
    public function getCourierAttribute($value)
    {
        return strtoupper($value);
    }
    public function getPaymentTargetAttribute($value)
    {
        $data = PaymentTarget::where('id',$value)->first();
        return $data['bank_name']." ". $data['number'] ." a/n ".$data['name'];
    }
    public function getShippingServiceAttribute($value)
    {
        return strtoupper($value);
    }
    public function detail()
    {
        return $this->hasMany('App\Models\TransactionDetail', 'transaction_id', 'id');
    }
    public function detail_status()
    {
        return $this->hasMany('App\Models\TransactionStatus', 'transaction_id', 'id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'users_id');
    }
    public function retur()
    {
        return $this->hasOne('App\Models\TransactionRetur', 'transaction_id', 'id');
    }
    public function warehouse()
    {
        return $this->hasOne('App\Models\Warehouse', 'id', 'warehouse_id');
    }
}
