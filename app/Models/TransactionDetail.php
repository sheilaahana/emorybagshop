<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    public $table = "transaction_detail";
    public $primaryKey = 'id';
    public $fillable = [
                    'transaction_id',
                    'product_id',
                    'variant_id',
                    'price',
                    'qty',
                    'memo',
                    ];

    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction', 'transaction_id', 'id');
    }
    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
    public function variant()
    {
        return $this->hasOne('App\Models\ProductVariant', 'id', 'variant_id');
    }
}
