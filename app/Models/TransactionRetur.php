<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionRetur extends Model
{
    public $table = "transaction_retur";
    public $primaryKey = 'id';
    public $fillable = [
                    'transaction_id',
                    'invoice_number',
                    'new_transaction_id',
                    'new_invoice_number',
                    'reason',
                    'request_type',
                    'status',
                    'image_1',
                    'image_2',
                    'image_3',
                    ];

    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction', 'transaction_id', 'id');
    }
    public function newtransaction()
    {
        return $this->belongsTo('App\Models\Transaction', 'new_transaction_id', 'id');
    }
}
