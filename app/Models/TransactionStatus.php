<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Admin;

class TransactionStatus extends Model
{
    public $table = "transaction_status_log";
    public $primaryKey = 'id';
    public $fillable = [
                    'transaction_id',
                    'status',
                    'description',
                    'change_by',
                    'change_id',
                    ];
    protected $appends = [
        'admin'
    ];

    public function getAdminAttribute()
    {
        $name = "";
        if(!empty($this->attributes['change_id'])){
            $d = Admin::where('id',$this->attributes['change_id'])->first();
            if($d){
                $name = $d['name'];
            }
        }
        

        return $name;
    }
    public function transaction()
    {
        return $this->belongsTo('App\Models\Transaction', 'transaction_id', 'id');
    }
}
