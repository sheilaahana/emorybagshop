<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCategory extends Model
{
    public $table = "users_category";
    public $fillable = [
        'category','description'
    ];

    public $primaryKey = 'id';

}
