<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPrice extends Model
{
    public $table = "user_price";
    public $fillable = [
        'product_id','category_id','harga_jual','harga_diskon'
    ];

    public $primaryKey = 'id';

    public function category()
    {
        return $this->hasOne('App\Models\UserCategory', 'id', 'category_id');
    }
}
