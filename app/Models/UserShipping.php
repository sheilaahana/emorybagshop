<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\City;
use App\Models\Province;

class UserShipping extends Model
{
    public $table = "users_shipping";
    public $fillable = [
        'name','phone','address','postal_code','city','province','users_id'
    ];

    public $primaryKey = 'id';
    protected $appends = [
        'city_name',
        'province_name',
    ];

    public function getCityNameAttribute()
    {
        $data = City::where('id',$this->attributes['city'])->first();
        return $data['type']." ".$data['city'];
    }
    public function getProvinceNameAttribute()
    {
        $data = Province::where('id',$this->attributes['province'])->first();
        return $data['type']." ".$data['province'];
    }
}
