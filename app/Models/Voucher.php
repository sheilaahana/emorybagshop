<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    public $table = "voucher";
    public $fillable = [
        'code',
        'discount_type',
        'value',
        'type',
        'start_date',
        'end_date',
        'validity_user',
        'validity_general',
        'minimal_spend',
        'maximal_spend',
        'product_include',
        'product_exclude',
        'category_include',
        'category_exclude',
        'tag_include',
        'tag_exclude',
        'whitelist_user',
        'blacklist_user',
        'is_valid',
        'is_join',
    ];
    protected $casts = [
        'product_include' => 'array',
        'product_exclude' => 'array',
        'category_include' => 'array',
        'category_exclude' => 'array',
        'tag_include' => 'array',
        'tag_exclude' => 'array',
        'whitelist_user' => 'array',
        'blacklist_user' => 'array',
    ];
    public function redeem()
    {
        return $this->hasMany('App\Models\VoucherRedeem', 'voucher_id', 'id');
    }
}
