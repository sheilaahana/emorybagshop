<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherRedeem extends Model
{
    public $table = "voucher_redeem";
    public $fillable = [
        'users_id',
        'voucher_id',
        'transaction_id',
        'code',
        'discount_type',
        'type',
        'value',
    ];
    
}
