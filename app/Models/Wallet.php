<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    public $table = "wallet";
    public $fillable = [
        'users_id',
        'type',
        'amount',
        'input_by',
        'memo',
        'admin_id'
    ];
    public function users()
    {
        return $this->hasMany('App\User', 'id', 'users_id');
    }
    public function admin()
    {
        return $this->hasOne('App\Admin', 'id', 'admin_id');
    }
}
