<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletUsers extends Model
{
    public $table = "wallet_users";
}
