<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    public $table = "warehouse";
    public $fillable = ['warehouse','code','location','catatan'];
    
    public function log_stock()
    {
        return $this->hasMany('App\Models\LogStock', 'warehouse_id', 'id');
    }
}
