<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WarehouseDetail extends Model
{
    public $table = "warehouse_detail";
    public $fillable = [
        "warehouse_id",
        "product_id",
        "variant_id",
        "stock",
        "nomor_rak",
        "baris",
        "kolom",
        "catatan",
    ];

    public function warehouse()
    {
        return $this->belongsto('App\Models\Warehouse', 'warehouse_id', 'id');
    }
    public function variant()
    {
        return $this->hasOne('App\Models\ProductVariant', 'id', 'variant_id');
    }
}
