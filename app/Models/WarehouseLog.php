<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WarehouseLog extends Model
{
    public $table = "warehouse_log";
    public $fillable = [
        'variant_id',
        'qty',
        'memo',
        'type',
        'location_origin',
        'location_destination',
        'input_by',
    ];
}
