<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    public $table = "wishlist";
    public $fillable = [
        'product_id','users_id','variant_id'
    ];

    public $primaryKey = 'id';

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
    public function variant()
    {
        return $this->hasOne('App\Models\ProductVariant', 'id', 'variant_id');
    }
}
