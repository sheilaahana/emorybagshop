<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use App\Models\City;
use App\Models\Province;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','birthdate','type','status','phone','category_id','address','postal_code','city','province','gender','image','warehouse_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $appends = [
        'city_name',
        'province_name',
        'image_link',
    ];
    public function getImageLinkAttribute()
    {
        if(isset($this->attributes['image'])){
            return url(Storage::url($this->attributes['image']));
        }
    }
    public function getCityNameAttribute()
    {
        $data = City::where('id',$this->attributes['city'])->first();
        return $data['type']." ".$data['city'];
    }
    public function getProvinceNameAttribute()
    {
        $data = Province::where('id',$this->attributes['province'])->first();
        return $data['type']." ".$data['province'];
    }
    public function shipping()
    {
        return $this->hasMany('App\Models\UserShipping', 'users_id', 'id');
    }
    public function transaction()
    {
        return $this->hasMany('App\Models\Transaction', 'users_id', 'id');
    }
    public function category()
    {
        return $this->hasOne('App\Models\UserCategory', 'id', 'category_id');
    }
    public function warehouse()
    {
        return $this->hasOne('App\Models\Warehouse', 'id', 'warehouse_id');
    }
    public function city()
    {
        return $this->hasOne('App\Models\City', 'id', 'city');
    }
    public function getBirthdateAttribute($value)
    {
        return date("d/m/Y", strtotime($value));
    }
}
