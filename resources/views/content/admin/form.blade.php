@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Administrator Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('administrator')}}">Administrator</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="col-md-12">
    @if (Session::get('message'))
    <div class="alert alert-danger">
        {{Session::get('message')}}
    </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<div class="card">
    <div class="body">
        <form action="{{url('/administrator/save')}}" method="post" class="m-form" id="fnotaris" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input name="id" type="hidden" class="form-control m-input" value="{{ empty($data->id)?old('id'):$data->id}}" placeholder="id">
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Name</label>
                    <input type="text" name="name" value="{{ empty($data->name)?old('name'):$data->name}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-12">
                    <label>Email</label>
                    <input type="text" name="email" value="{{ empty($data->email)?old('email'):$data->email}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-12">
                    <label>Divisi </label>
                    <select id="single-selection" name="divisi" class="form-control" required>
                        @foreach($data['divdata'] as $pc)
                        <option value="{{$pc['id']}}" {{!empty($data['divisi'])&&$data['divisi']||old('divisi')==$pc['divisi']?'selected':''}}>{{$pc['divisi']}}</option>
                        @endforeach
                    </select>
                    
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
@endsection