@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Role administrator</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('administrator')}}">Administrator</a></li>
                <li class="breadcrumb-item active" aria-current="page">Role Admin</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-md-12">
        <form action="{{url('administrator/save_role')}}" method="post" class="m-form">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input type="hidden" name="id" class="form-control m-input" aria-describedby="emailHelp" placeholder="ID" value="{{ empty($data->id)?'':$data->id}}">
            <div class="m-portlet__body">
                <div class="row">
                    @if(isset($message))
                        <div class="alert alert-danger">
                        {{$message}}
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <table class="table">
                            <thead>
                            <tr>    
                                <th><b>Menu / Divisi</b></th>
                                @foreach($data['role'] as $r)                        
                                <th width="200">{{$r['divisi']}}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($data['module'] as $m)
                                <tr>
                                    <td >{{$m['menu']}}</td>
                                    @foreach($data['role'] as $r)                        
                                    <td>
                                        <div class="m-checkbox-inline">
                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">
                                                <input name="{{$r['id']}}[]" type="checkbox" {{in_array($m['id'],json_decode($r['role']))?'checked':''}} value="{{$m['id']}}"> Yes
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    @endforeach
                                    <?php $rolerole = [];?>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="card" style="height:500px;overflow:auto;background-color:white;padding:20px">
                            <b>Log Change</b>
                            <ul>
                                @forelse($log as $log)
                                    <li><b>{{$log['change_by']}} {{$log['admin']}}</b> <br>{{$log['description']}}<br> <i>{{date('d/m/y h:i:s',strtotime($log['created_at']))}}</i></li>
                                @empty
                                    <li>Empty Data</li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions">
                    <button type="submit" onclick="return confirm('Are you Sure?')" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection