@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Keep Product Detail</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('cart')}}">Keep Product</a></li>
                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
        </div>         
    </div>
</div>
@if(!empty($data))
<div class="col-md-12">
    @if (Session::get('message'))
    <div class="alert alert-danger">
        {{Session::get('message')}}
    </div>
    @endif
</div>
<div class="row clearfix">
    <div class="col-lg-8 col-md-8  col-sm-12">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Product Name / Variant </small>
                    <p class="mb-0"><b>{{ucfirst($data['product']['product_name'])}}</b> variant <b>{{$data['variant']}}</b></p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Category / Type</small>
                    <p class="mb-0">{{$data['product']['category']['category_name']}} / {{$data['product']['category']['type']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Berat </small>
                    <p class="mb-0">{{$data['product']['berat']}} kg</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Description</small>
                    <p class="mb-0">{!!$data['product']['description']!!}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Specification</small>
                    <p class="mb-0">{!!$data['product']['specification']!!}</p>
                </li>
               
            </ul>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Default Image</small>
                    <p><img src="{{$data['image']}}" alt="" style="width:100%;max-width:300px"></p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Harga <b>Default</b> </small>
                    <p class="mb-0">Harga Jual : {{Generate::money($data['product']['harga_jual'])}}</p>
                    <p class="mb-0">Harga Diskon : {{Generate::money($data['product']['harga_diskon'])}}</p>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-md-6 col-sm-12">  
                        <h5>List User Keep Product</h5>
                    </div>
                </div><br>
                <table class="table table-hover spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Customer Detail</th>
                            <th>Qty</th>
                            <th>Memo</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($data['cart'])>0)
                    <?php $a=1; ?>
                        @foreach($data['cart'] as $d)
                        <tr>
                            <td><span>{{$a++}}</span></td>
                            <td><b>{{ucwords($d['users']['name'])}}</b><br>
                                {!!$d['users']['email']?:'<i>Empty Email</i>'!!}<br>
                                {!!$d['users']['phone']?:'<i>Empty Phone Number</i>'!!}
                            </td>
                            <td><span>{{$d['qty']}} pcs </span></td>
                            <td><span>{{$d['memo']}} </span></td>
                            <td>
                                <a href="https://api.whatsapp.com/send?phone={{$d['users']['phone']}}" target="_blank"  class="btn btn-sm btn-success" title="Call on Whatsapp"><i class="fa fa-phone"></i></a>
                                <a href="{{url('cart/delete/'.$d['id'])}}" onclick="return confirm('Are you sure you want to delete this cart item')" class="btn btn-sm btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5"><b><i><center>Invalid Data Product</center></i></b></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@else
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <center>
                <h6>Invalid Data / Data not Found</h6>
                <hr>
                <a href="{{url('cart')}}"> Back to List Product </a>
                </center>
            </div>
        </div>
    </div>
</div>

@endif
@endsection