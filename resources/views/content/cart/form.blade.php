@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage;?>
    <div class="block-header" >
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h1>Add Keep User </h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                    <li class="breadcrumb-item"><a href="{{url('cart')}}">Keep Product</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add to User </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="body" style="overflow-x:auto">
            <form action="{{url('/cart/save')}}" method="post" class="m-form">
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <input type="hidden" name="variant_id" value="{{$data['variant']['id']}}" />
                <input type="hidden" name="product_id" value="{{$data['variant']['product_id']}}" />
                <div class="row">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-md-12">
                                <center><h5>Add To Cart User</h5></center>
                                <div class="form-group col-md-12">
                                    <label>User</label>
                                    <select name="user" class="form-control m-input" id="user" required>
                                        @foreach($data['user'] as $cc)
                                        <option value="{{$cc['id']}}">{{$cc['name']}} - {{$cc['email']}}</option>
                                        @endforeach
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group col-md-12">
                                    <label>Qty</label>
                                    <input name="qty"  type="number" min=0 max={{$data['variant']['valid_stock']}} value="0" class="form-control" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                    </div>
                    <div class="col-lg-6">
                        <table>
                            <tr><td colspan=2>Variant Detail</td></tr>
                            <tr><td colspan=2> <img src="{{$data['variant']['image']}}" alt="" style="width:100%;max-width:200px"></td>
                            <tr><td><b>Product</b></td><td>: {{$data['variant']['product']['product_name']}} </td></tr>
                            <tr><td><b>Variant</b></td><td>: {{$data['variant']['variant']}} </td></tr>
                            <tr><td><b>Qty Stocks</b></td><td>: {{$data['variant']['qty_stock']}} pcs</td></tr>
                            <tr><td><b>Valid Stocks</b></td><td>: {{$data['variant']['valid_stock']}} pcs</td></tr>
                            <tr><td>&emsp;</td></tr>
                        </table>
                    </div>
                </div>
                

        </div>
    </div>
@endsection