@extends('layouts.app')

@section('content')
<?php use App\Helpers\ProductGenerate as ProductGenerate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Cart</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Keep Product</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>No</th>
                            <th>Variant</th>
                            <th>Hexa</th>
                            <th>Image</th>
                            <th>KeepBy</th>
                            <th>KeepQty</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $a=1;?>
                        @foreach($data as $d)
                        <tr>
                            <td><span>{{$a++}}</span></td>
                            <td>
                                {{$d['product']['product_name']}}<br>
                                <b>{{$d['variant']}}</b>
                            </td>
                            <td>
                                <div style="background-color:{{$d['hexa']}};width:30px;height:30px;border-radius:100%;margin:5px 20px"></div> {{$d['hexa']}}
                            </td>
                            <td><img src="{{$d['image']}}" alt="" style="width:100%;max-width:100px"></td>
                            <td><span>{{ProductGenerate::keepby($d['id'])}} Users</span></td>
                            <td><span>{{ProductGenerate::keepqty($d['id'])}} Pcs</span></td>
                            <td>
                                <a href="{{url('cart/detail/'.$d['id'])}}" class="btn btn-sm btn-info" title="Detail"><i class="fa fa-eye"></i>  </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection