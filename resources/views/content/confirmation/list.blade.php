@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Confirmation Payment</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Confirmation Payment</li>
                </ol>
            </nav>
        </div>  
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Invoice</th>
                            <th>Customer</th>
                            <th>Payment Detail</th>
                            <th>Payment</th>
                            <th>StatusTrx</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; ?>
                        @foreach($data as $d)
                        <tr>
                            <td><span>{{$no++}}</span></td>
                            <td>{{date('d M Y h:i:s', strtotime($d['created_at']))}}</td>
                            <td><a href="{{url('transaction/detail/'.$d['id'])}}" target="_blank" class="btn btn-sm btn-default" title="Detail Transaction"><span>{{$d['invoice_number']}}</span></a></td>
                            <td><a href="{{url('customer/detail/'.$d['users_id'])}}" target="_blank">{{ucwords($d['user']['name'])}}</a></td>
                            <td>
                                <b>Payment Target : </b><br>
                                {{$d['payment_target']}}<br>
                                {{Generate::money($d['total'])}}<br>
                                @if($d['payment_receipt']!==null && !empty($d['payment_receipt']))
                                {{date('d F y h:i:s', strtotime($d['confirmation_date']))}}<br>
                                <a href="#" data-toggle="modal" data-target="#Modal-{{$d['id']}}"> View Receipt Image</a>
                                @else
                                <i>haven't confirmed yet</i>
                                @endif
                            </td>
                            <td>
                                <?php if($d['payment_status']=="UNPAID"){$badge='badge-warning';}elseif($d['payment_status']=='PAID'){$badge='badge-success';}else{$badge='badge-danger';} ?>
                                <span class="badge {{$badge}} ml-0 mr-0">{{$d['payment_status']}}</span></i>
                            </td>
                            <td><span>{{$d['status_trx']}}</span></td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#Modal-{{$d['id']}}" class="btn btn-sm btn-info" title="Detail Confirmation"><i class="fa fa-eye"></i> &emsp;Detail</a>
                            </td>
                        </tr>
                        <div class="modal fade" id="Modal-{{$d['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"> Change Warehouse Detail</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <img src="{{url(Storage::url($d['payment_receipt']))}}" alt="" style="width:100%">
                                    </div>
                                    <div class="modal-footer">
                                        <div class="btn-group" role="group">
                                            <button id="btnGroupDrop1" type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Change Status
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                            <a class="dropdown-item" href="{{url('transaction/payment/unpaid/'.$d['id'])}}">UnPaid</a>
                                            <a class="dropdown-item" href="{{url('transaction/payment/paid/'.$d['id'])}}">Paid</a>
                                            <a class="dropdown-item" href="{{url('transaction/payment/rejected/'.$d['id'])}}">Rejected</a>
                                            <a class="dropdown-item" href="{{url('transaction/payment/expired/'.$d['id'])}}">Expired</a>
                                            <a class="dropdown-item" href="{{url('transaction/payment/paylater/'.$d['id'])}}">Pay Later</a>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection