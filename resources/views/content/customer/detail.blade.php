@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate;use App\Helpers\WalletHelper as WalletHelper; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Customer Detail</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('customer')}}">Customer</a></li>
                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
        </div>            
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <a href="{{url('wallet/detail/'.$data['id'])}}" class="btn btn-sm btn-info" title="Themeforest"> View Wallet Transaction</a>
            <a href="{{url('customer/form?id='.$data['id'])}}" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-pencil"></i>&emsp; Edit Customer</a>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-4 col-md-4">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Name </small>
                    <p class="mb-0">{{ucwords($data['name'])}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Email </small>
                    <p class="mb-0">{{$data['email']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Phone </small>
                    <p class="mb-0">{{$data['phone']?:'-'}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Address </small>
                    <p class="mb-0">{{$data['address']?:'-'}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Postal Code / City / Province</small>
                    <p class="mb-0">{{$data['postal_code']?:'-'}} / {{$data['city_name']?:'-'}} / {{$data['province_name']?:'-'}}</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted"> Gender</small>
                    <p class="mb-0"> {{$data['gender']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Status </small>
                    <p class="mb-0">{{$data['status']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Type </small>
                    <p class="mb-0">{{$data['type']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Category </small>
                    <p class="mb-0">{{$data['category']['category']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Warehouse </small>
                    <p class="mb-0">{{$data['warehouse']['warehouse']}}</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="card" style="height:340px;overflow:auto;background-color:white;padding:20px">
            <b>Log Change</b>
            <ul>
                @forelse($data['log'] as $log)
                    <li><b>{{$log['change_by']}} {{$log['admin']}}</b> <br>{{$log['description']}}<br> <i>{{date('d/m/y h:i:s',strtotime($log['created_at']))}}</i></li>
                @empty
                    <li>Empty Data</li>
                @endforelse
            </ul>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Wallet</small>
                    <p class="mb-0">{{Generate::money(WalletHelper::wallet($data['id']))}}</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Point</small>
                    <p class="mb-0">{{Generate::money(WalletHelper::point($data['id']))}}</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Total Wallet</small>
                    <p class="mb-0">{{Generate::money(WalletHelper::totalwallet($data['id']))}}</p>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <h5>Data transaction</h5><br>
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Invoice</th>
                            <th>Shipping</th>
                            <th>Payment </th>
                            <th>Shipping Status</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['transaction'] as $d)
                        <tr>
                            <td><span>{{$d['id']}}</span></td>
                            <td>{{date('d M Y h:i:s', strtotime($d['created_at']))}}</td>
                            <td><span>{{$d['invoice_number']}}</span></td>
                            <td>{{$d['city_name']}}</td>
                            <td>
                                <?php if($d['payment_status']=="UNPAID"){$badge='badge-danger';}elseif($d['payment_status']=='PAID'){$badge='badge-success';}else{$badge='badge-primary';} ?>
                                <span class="badge {{$badge}} ml-0 mr-0">{{$d['payment_status']}}</span></i>
                            </td>
                            <td>{{$d['shipping_status']}}</td>
                            <td>{{Generate::money($d['total'])}}</td>
                            
                            <td>
                                <a href="{{url('transaction/detail/'.$d['id'])}}" target="_blank" class="btn btn-sm btn-default" title="Approved"><i class="fa fa-eye text-success"></i> &emsp;Detail </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection