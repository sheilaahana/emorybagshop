@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Customer Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('customer')}}">Customer</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form</li>
                </ol>
            </nav>
        </div>            
    </div>
</div>
<div class="col-md-12">
    @if (Session::get('message'))
    <div class="alert alert-danger">
        {{Session::get('message')}}
    </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<div class="card">
    <div class="body">
        <form action="{{url('/customer/save')}}" method="post" class="m-form" >
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input name="id" type="hidden" class="form-control m-input" value="{{ empty($data['data']['id'])?old('id'):$data['data']['id']}}" placeholder="id">
            <div class="row clearfix">
                <div class="form-group col-md-12">
                    <label>Warehouse </label>
                    <select id="single-selection" name="warehouse_id" class="form-control" required>
                        @foreach($data['master']['warehouse'] as $pw)
                        <option value="{{$pw['id']}}" {{!empty($data['data']['warehouse_id'])&&$data['data']['warehouse_id']==$pw['id']?'selected':''}}>{{$pw['warehouse']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label>Customer Name</label>
                    <input type="text" name="name" value="{{ empty($data['data']['name'])?old('name'):$data['data']['name']}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-4">
                    <label>Customer Email</label>
                    <input type="email" name="email" value="{{ empty($data['data']['email'])?old('email'):$data['data']['email']}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-4">
                    <label>Phone</label>
                    <input type="number" name="phone" step=".01" value="{{ empty($data['data']['phone'])?old('phone'):$data['data']['phone']}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-4">
                    <label>Customer Category / Type </label>
                    <select id="single-selection" name="category_id" class="form-control" required>
                        @foreach($data['master']['category'] as $pc)
                        <option value="{{$pc['id']}}" {{!empty($data['data']['category_id'])&&$data['data']['category_id']==$pc['id']?'selected':''}}>{{$pc['category']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label>Password</label>
                    <input type="password" name="password" step=".01" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>Type</label>
                    <br>
                    <label class="fancy-radio">
                        <input type="radio" name="type" value="RESELLER" required="" data-parsley-errors-container="#error-radio"  {{!empty($data['data']['type'])&&$data['data']['type']||old('type')=='RESELLER'?'checked':''}} >
                        <span><i></i>Reseller</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="type" value="DROPSHIP" {{!empty($data['data']['type'])&&$data['data']['type']||old('type')=='DROPSHIP'?'checked':''}} >
                        <span><i></i>Dropship</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="type" value="ECER" {{!empty($data['data']['type'])&&$data['data']['type']||old('type')=='ECER'?'checked':''}} >
                        <span><i></i>Ecer</span>
                    </label>
                    <p id="error-radio"></p>
                </div>
                <div class="form-group col-md-4">
                    <label>Status</label>
                    <br>
                    <label class="fancy-radio">
                        <input type="radio" name="status" value="INACTIVE" required="" data-parsley-errors-container="#error-radio"  {{!empty($data['data']['status'])&&$data['data']['status']||old('status')=='INACTIVE'?'checked':''}} >
                        <span><i></i>INACTIVE</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="status" value="ACTIVE" {{!empty($data['data']['status'])&&$data['data']['status']||old('status')=='ACTIVE'?'checked':''}} >
                        <span><i></i>ACTIVE</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="status" value="MODERATE" {{!empty($data['data']['status'])&&$data['data']['status']||old('status')=='MODERATE'?'checked':''}} >
                        <span><i></i>MODERATE</span>
                    </label>
                    <p id="error-radio"></p>
                </div>
                <div class="form-group col-md-4">
                    <label>Gender</label>
                    <br>
                    <label class="fancy-radio">
                        <input type="radio" name="gender" value="FEMALE" required="" data-parsley-errors-container="#error-radio"  {{!empty($data['data']['gender'])&&$data['data']['gender']||old('gender')=='FEMALE'?'checked':''}} >
                        <span><i></i>FEMALE</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="gender" value="MALE" {{!empty($data['data']['gender'])&&$data['data']['gender']||old('gender')=='MALE'?'checked':''}} >
                        <span><i></i>MALE</span>
                    </label>
                    <p id="error-radio"></p>
                </div>
                <div class="form-group col-md-12">
                    <label>Address</label>
                    <textarea class="form-control" name="address" rows="5" required="">{{ empty($data['data']['address'])?old('address'):$data['data']['address']}}</textarea>
                </div>
                <div class="form-group col-md-4">
                    <label>Province </label>
                    <select name="province" class="form-control m-input" id="province" required>
                        @foreach($data['master']['province'] as $pp)
                        <option value="{{$pp['id']}}" {{!empty($data['data']['province'])&&$data['data']['province']==$pp['id']?'selected':''}}>{{$pp['province']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label>City </label>
                    <select name="city" class="form-control m-input" id="city" required>
                        <option value=''>- Select Province First - </option>
                        <?php if (isset($data['data']['city'])) { ?>
                        <option value="{{$data['data']['city']}}" selected>{{$data['data']['city_name']}}</option>
                        <?php } ?>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group col-md-4">
                    <label>Postal Code</label>
                    <input type="text" name="postal_code" value="{{ empty($data['data']['postal_code'])?old('postal_code'):$data['data']['postal_code']}}" class="form-control" required="">
                </div>
                
            </div>
            
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
@endsection