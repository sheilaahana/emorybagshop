@extends('layouts.app')

@section('content')
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Customer</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Customer</li>
                </ol>
            </nav>
        </div> 
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <a href="{{url('customer/form')}}" class="btn btn-sm btn-info" title="">Register New Customer</a>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <form action="{{url('/customer/mass/save')}}" method="post" class="m-form" >
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                        <thead >
                            <tr>
                                <th></th>
                                <th>No</th>
                                <th>Waktu Register</th>
                                <th>Name</th>
                                <th>Jenis Pelanggan</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no=1;?>
                            @foreach($data as $d)
                            <tr>
                                <td class="width45">                                           
                                    <input type="checkbox" name="mass[]" class="form-control" value="{{$d['id']}}" style="width:15px">
                                </td>
                                <td><span>{{$no++}}</span></td>
                                <td>{{date('d M Y h:i:s', strtotime($d['created_at']))}}</td>
                                <td>
                                    <div class="font-15">
                                        <b>{{ucwords($d['name'])}}</b><br>
                                        {{$d['email']}}<br>
                                        {{$d['phone']}}
                                    </div>                                            
                                </td>
                                <td><i>{{$d['category']['category']?:"None"}}</i></td>
                                <td>
                                    <?php if($d['status']=="INACTIVE"){$badge='badge-danger';}elseif($d['status']=='ACTIVE'){$badge='badge-success';}else{$badge='badge-primary';} ?>
                                    <a href="{{url('customer/status/'.$d['id'])}}" onclick="return confirm('Are you sure you want to Change this?');" class="badge {{$badge}} ml-0 mr-0">{{$d['status']?:"None"}}</a></i>
                                </td>
                                <td>
                                    <a href="{{url('customer/detail/'.$d['id'])}}" class="btn btn-sm btn-default text-success" title="Approved"><i class="fa fa-eye text-success"></i> &emsp;Detail</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row clearfix">
                        <div class="form-group col-md-4"><br>
                            <label>Change selected to : </label>
                            <select id="single-selection" name="action" class="form-control" required>
                                <option>INACTIVE</option>
                                <option>ACTIVE</option>
                                <option>MODERATE</option>
                            </select>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="form-group col-md-4">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection