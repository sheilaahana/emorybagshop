@extends('layouts.app')

@section('content')
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Dashboard</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                </ol>
            </nav>
        </div> 
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-3">
        <div class="card">
            <div class="body">
                <div class="card-value float-right text-muted"><i class="icon-user"></i></div>
                <h3 class="mb-1">{{Auth::user()->name}}</h3>
                <div>{{Auth::user()->email}}</div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="body">
                <div class="card-value float-right text-muted"><a href="{{url('customer')}}"><i class="icon-users"></i></a></div>
                <h3 class="mb-1">{{$data['user_qty']}}</h3>
                <div>Total Customer</div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="body">
                <div class="card-value float-right text-muted"><a href="{{url('product')}}"><i class="icon-handbag"></i></a></div>
                <h3 class="mb-1">{{$data['product_qty']}}</h3>
                <div>Jumlah Product Online</div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="body">
                <div class="card-value float-right text-muted"><a href="{{url('transaction')}}"><i class="icon-basket"></i></a></div>
                <h3 class="mb-1">{{$data['transaction_qty']}}</h3>
                <div>Total Transaction</div>
            </div>
        </div>
    </div>
</div>
<div class="row"><br></div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <div id="grafik"></div>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <div class="card" >
            <div class="header">
                <h3>Transaksi Terakhir</h3>
                <ul class="header-dropdown dropdown">
                </ul>
            </div>
            <div class="body" style="height:400px;overflow: auto;">
                <table class="table table-hover table-custom spacing5 mb-0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Invoice</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $no=1; ?>
                        @foreach($data['pesanan'] as $p)
                        <tr>
                            <td class="w60">{{$no++}}</td>
                            <td>{{date('d M Y h:i:s', strtotime($p['created_at']))}}</td>
                            <td><b>{{$p['invoice_number']}}</b><br>
                                {{$p['nama_penerima']}}<br>
                                {{$p['email']}}
                            </td>
                            <td><span>{{$p['payment_status']}}</span></td>
                            <td>
                                <a href="{{url('transaction/detail/'.$p['id'])}}" target="_blank" class="btn btn-sm btn-default" title="Approved"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card" >
            <div class="header">
                <h3>Peringatan Stock</h3>
                <ul class="header-dropdown dropdown">
                </ul>
            </div>
            <div class="body" style="height:400px;overflow: auto;">
                <table class="table table-hover table-custom spacing5 mb-0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Product</th>
                            <th>Variant</th>
                            <th>Qty</th>
                            <th>Valid</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $no=1; ?>
                        @foreach($data['peringatan'] as $sp)
                        <tr>
                            <td class="w60">
                                {{$no++}}
                            </td>
                            <td><span>{{$sp['product']['product_name']}}</span></td>
                            <td><span>{{$sp['variant']}}</span></td>
                            <td>{{$sp['qty_stock']}}</td>
                            <td>{{$sp['valid_stock']}}</td>
                            <td>
                                <a href="{{url('product/variant/form?id='.$sp['id'])}}" target="_blank" class="btn btn-sm btn-default" title="Approved"><i class="fa fa-edit"></i> </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
$(document).ready(function() {
    
    var options = {
        chart: {
            height: 300,
            type: 'area',
            stacked: true,
            toolbar: {
                show: false,
            },
            events: {
                selection: function(chart, e) {
                console.log(new Date(e.xaxis.min) )
                }
            },
        },
        

        colors: ['#6435c9', '#f66d9b', '#CED4DC'],
        dataLabels: {
            enabled: false
        },

        series: [
            {
                name: 'IYA',
                data: []
            }
        ],

        fill: {
            type: 'gradient',
            gradient: {
                opacityFrom: 0.6,
                opacityTo: 0.8,
            }
        },

        legend: {
            position: 'top',
            horizontalAlign: 'right',
            show: true,
        },
        xaxis: {
            type: 'datetime',            
        },
        grid: {
            yaxis: {
                lines: {
                    show: false,
                }
            },
            padding: {
                top: 20,
                right: 0,
                bottom: 0,
                left: 0
            },
        },
        stroke: {
            show: true,
            curve: 'smooth',
            width: 1,
        },
    }

    var chart = new ApexCharts(
        document.querySelector("#grafik"),
        options
    );
    var url = 'ajax/dashboard';

    $.getJSON(url, function(response) {
        chart.updateSeries([{
            name: 'Sales',
            data: response
        }])
    });
    chart.render();

});
</script>
@endpush