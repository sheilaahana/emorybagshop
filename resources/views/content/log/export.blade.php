<?php use App\Helpers\Generate as Generate; ?>
<table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0" style="overflow-x:auto">
    <thead >
        <tr>
            <th>ID</th>
            <th>Module</th>
            <th>Module Id / Users Id</th>
            <th>Description</th>
            <th>Change By</th>
            <th>Change Id</th>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; ?>
        @foreach($data as $d)
        <tr>
            <td><span>{{$no++}}</span></td>
            <td><span>{{$d['module']}}</span></td>
            <td><span>{{$d['module_id']}}</span></td>
            <td><span>{{$d['description']}}</span></td>
            <!-- <td><span>{{substr($d['description'],0,100)}}</span></td> -->
            <td><span>{{$d['change_by']}}</span></td>
            <td><span>
            {{Generate::admin($d['change_id'])}}
            </span></td>
            <td><span>{{$d['created_at']}}</span></td>
        </tr>
        @endforeach
    </tbody>
</table>