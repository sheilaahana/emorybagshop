@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-10 col-sm-10">
            <h1>Log Change</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Log</li>
                <li class="breadcrumb-item active" aria-current="page">Changes</li>
                </ol>
            </nav>
        </div>
        <div class="col-md-2 col-sm-2">
            <div class="btn-group" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Change By
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a class="dropdown-item" href="{{url('log')}}">All Data</a>
                <a class="dropdown-item" href="{{url('log?changeby=ADMIN')}}">Admin</a>
                <a class="dropdown-item" href="{{url('log?changeby=SYSTEM')}}">System</a>
                <a class="dropdown-item" href="{{url('log?changeby=CUSTOMER')}}">Customer</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                @include('content.log.table')
            </div>
        </div>
    </div>
</div>
@endsection