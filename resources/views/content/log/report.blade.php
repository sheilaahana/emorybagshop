@extends('layouts.app')

@section('content')
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Log Report</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Log</li>
                <li class="breadcrumb-item active" aria-current="page">Report</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <form action="" method="post" class="m-form">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Module</b></span>
                        </div>
                        <select class="custom-select" name="module" id="module">
                            <option value="">Choose .. ( Default all data )</option>
                            @foreach($helper['module'] as $m)
                            <option value="{{$m}}" {{!empty($param['module'])&&$param['module']==$m?'selected':''}} > {{$m}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Change by</b></span>
                        </div>
                        <select class="custom-select" name="change_by" id="change">
                            <option value="">Choose .. ( Default all data )</option>
                            <option value="ADMIN" {{!empty($param['change_by'])&&$param['change_by']=='ADMIN'?'selected':''}} > ADMIN</option>
                            <option value="CUSTOMER" {{!empty($param['change_by'])&&$param['change_by']=='CUSTOMER'?'selected':''}} > CUSTOMER</option>
                            <option value="SYSTEM" {{!empty($param['change_by'])&&$param['change_by']=='SYSTEM'?'selected':''}}> SYSTEM</option>
                        </select>
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01"><b>Show Data</b></label>
                        </div>
                        <select class="custom-select" name="data" id="data">
                            <option value="">Choose .. ( Default 20 data )</option>
                            <option value="10" {{!empty($param['data'])&&$param['data']=='10'?'selected':''}} > 10 Data</option>
                            <option value="50" {{!empty($param['data'])&&$param['data']=='50'?'selected':''}} > 50 Data</option>
                            <option value="250" {{!empty($param['data'])&&$param['data']=='250'?'selected':''}}>250 Data</option>
                            <option value="500" {{!empty($param['data'])&&$param['data']=='500'?'selected':''}}>500 Data</option>
                            <option value="all" {{!empty($param['data'])&&$param['data']=='all'?'selected':''}}>Show All Data</option>
                        </select>
                    </div>
                    <div class="col-lg-12 col-md-12"><br>
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="{{url('log/report')}}" class="btn btn-danger">Reset</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <div class="col-lg-12 col-md-12">
                    <form action="{{url('log/export')}}" method="post" class="m-form">
                        <input type="hidden" name="param" value="{{ empty($param)?'':json_encode($param)}}" class="form-control" aria-describedby="basic-addon1">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <button type="submit" style="float:right" class="btn btn-primary">Export Data</button>
                    </form>
                    <br>
                    <br>
                </div>
                <div class="col-lg-12 col-md-12" style="overflow-x:auto">
                @include('content.log.table')    
                </div>            
                <!-- -->
            </div>
        </div>
    </div>
</div>
@endsection