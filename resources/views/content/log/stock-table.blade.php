<style>
table{
    width:100%;
}
th,td{
        padding:5px 10px;
    }
    .bg{
        background-color: #ccc;

    }
    h5{
        margin:50px 10px 10px   ;
        font-size:14px
    }
    </style>
@forelse($data['product']['variant'] as $v )
<h5>{{$data['product']['product_name'] ." - ". $v['variant']}}</h5>
<table border=1>
    <thead >
        <tr class="bg">
            <th>ID</th>
            <th>Date</th>
            <th>Change By</th>
            <th>Type</th>
            <th>Stock</th>
            <th>Description</th>
            <th>Sub Total</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; $total = 0;?>
        @foreach($data['log'] as $d)
            @if($d['variant_id'] == $v['id'])
                <?php 
                    if($d['type'] == 'minus'){
                        $total = $total - $d['stock'];
                    }else{
                        $total = $total + $d['stock'];
                    }
                ?>
                <tr>
                    <td><span>{{$no++}}</span></td>
                    <td><span>{{date('d M Y h:i:s', strtotime($d['created_at']))}}</span></td>
                    <td><span>{{$d['change_by']}} {{$d['admin']}}</span></td>
                    <td><span>{{$d['type']}}</span></td>
                    <td><span>{{$d['stock']}}</span></td>
                    <td><span>{{substr($d['description'],0,100)}}</span></td>
                    <td><span>{{$total}}</span></td>
                </tr>
            @endif
        @endforeach
        @if($total != 0)
        <tr><td colspan='6' style="text-align:right"><b>Total</b></td><td><b>{{$total}}</b></td></tr>
        @endif
        <tr><td colspan='7' style="text-align:center">End Of Log Stock</td></tr>
    </tbody>
</table>
@empty
<h6 style="text-align:center">Empty Log</h6>
@endforelse