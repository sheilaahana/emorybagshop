@extends('layouts.app')

@section('content')
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Log Report</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Log</li>
                <li class="breadcrumb-item active" aria-current="page">Report Stock</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <form action="" class="m-form">
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Product</b></span>
                        </div>
                        <select class="select2 form-control" name="product" id="module">
                            <option value="">Choose .. ( Default all data )</option>
                            @foreach($helper['product'] as $m)
                            <option value="{{$m['id']}}" {{!empty($_GET['product'])&&$_GET['product']==$m['id']?'selected':''}} > {{$m['product_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Warehouse</b></span>
                        </div>
                        <select class="custom-select" name="warehouse" id="module">
                            <option value="">Choose .. ( Default Master Batam )</option>
                            @foreach($helper['warehouse'] as $w)
                            <option value="{{$w['id']}}" {{!empty($_GET['warehouse'])&&$_GET['warehouse']==$w['id']?'selected':''}} > {{$w['warehouse']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-12 col-md-12"><br>
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="{{url('log/report')}}" class="btn btn-danger">Reset</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
                @if($data['product'])
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                    @include('content.log.stock-table')                
                <!-- -->
            </div>
        </div>
    </div>
    @endif
</div>
@endsection