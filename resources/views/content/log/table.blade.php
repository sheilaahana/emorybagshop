<style>
th,td{
        padding:5px 10px;
    }
    .bg{
        background-color: #ccc;

    }
    h5{
        margin:50px 10px 10px   ;
        font-size:14px
    }
    </style>
<table  class="table table-hover  table-custom spacing5 mb-0">
    <thead >
        <tr class="bg">
            <th>ID</th>
            <th>Module</th>
            <th>Description</th>
            <th>Change By</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; ?>
        @foreach($data as $d)
        <tr>
            <td><span>{{$no++}}</span></td>
            <td><span>{{$d['module']}}</span></td>
            <td><div style="width:600px;overflow-x:auto;border:1px solid gray;padding:15px" >{!!str_replace(',','<br>', $d['description'])!!}</div></td>
            <!-- <td><span>{{substr($d['description'],0,100)}}</span></td> -->
            <td><span>{{$d['change_by']}} {{$d['admin']}}</span></td>
        </tr>
        @endforeach
    </tbody>
</table>