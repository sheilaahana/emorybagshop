@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Payment Target Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('payment/target')}}">Payment Target</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form</li>
                </ol>
            </nav>
        </div>          
    </div>
</div>
<div class="card">
    <div class="body" style="overflow-x:auto">
        <form action="{{url('/payment/target/save')}}" method="post" class="m-form" id="fnotaris" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input name="id" type="hidden" class="form-control m-input" value="{{ empty($data->id)?old('id'):$data->id}}" placeholder="id">
            
            <div class="form-group">
                <label>Bank Name</label>
                <input type="text" name="bank_name" value="{{ empty($data->bank_name)?old('bank_name'):$data->bank_name}}" class="form-control" required="">
            </div>
            <div class="form-group">
                <label>Icon</label>
                <input type="file" name="icon"  class="form-control">
            </div>
            <div class="form-group">
                <label>Account Name</label>
                <input type="text" name="name" value="{{ empty($data->name)?old('name'):$data->name}}" class="form-control" required="">
            </div>
            <div class="form-group">
                <label>Account Number</label>
                <input type="text" name="number" value="{{ empty($data->number)?old('number'):$data->number}}" class="form-control" required="">
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
@endsection