@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Payment Target</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Payment Target</li>
                </ol>
            </nav>
        </div>            
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <a href="{{url('payment/target/form')}}" class="btn btn-sm btn-primary" title="">Add Payment Target</a>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Created</th>
                            <th>Bank Name</th>
                            <th>Icon</th>
                            <th>Account</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $d)
                        <tr>
                            <td><span>{{$d['id']}}</span></td>
                            <td>{{date('d M Y h:i:s', strtotime($d['created_at']))}}</td>
                            <td><span>{{$d['bank_name']}}</span></td>
                            <td>
                                <img src="{{$d['bank_icon']}}" alt="" style="width:100%;max-width:100px">                                
                            </td>
                            <td><span>{{$d['number']}} a/n {{$d['name']}}</span></td>                            
                            <td>
                                <a href="{{url('payment/target/form?id='.$d['id'])}}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="{{url('payment/target/delete/'.$d['id'])}}" onclick="return confirm('Are you sure you want to delete this item')" class="btn btn-sm btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection