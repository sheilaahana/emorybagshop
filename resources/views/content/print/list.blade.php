@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Print Nota Transaction</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Print</li>
                </ol>
            </nav>
        </div>  
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Confirmed Date</th>
                            <th>Invoice</th>
                            <th>Customer</th>
                            <th>Shipping</th>
                            <th>Payment</th>
                            <th>StatusTrx</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; ?>
                        @foreach($data as $d)
                        <tr>
                            <td><span>{{$no++}}</span></td>
                            <td>{{date('d M Y h:i:s', strtotime($d['created_at']))}}</td>
                            <td>{{ $d['confirmed_date'] ? date('d M Y', strtotime($d['confirmed_date'])) : "" }}</td>
                            <td><a href="{{url('transaction/detail/'.$d['id'])}}" target="_blank" class="btn btn-sm btn-default" title="Detail Transaction"><span>{{$d['invoice_number']}}</span></a></td>
                            <td><a href="{{url('customer/detail/'.$d['users_id'])}}" target="_blank">{{ucwords($d['user']['name'])}}</a></td>
                            <td><span class="badge primary ml-0 mr-0">{{$d['shipping_status']}}</span></i></td>
                            <td>
                                <?php if($d['payment_status']=="UNPAID"){$badge='badge-warning';}elseif($d['payment_status']=='PAID'){$badge='badge-success';}else{$badge='badge-danger';} ?>
                                <span class="badge {{$badge}} ml-0 mr-0">{{$d['payment_status']}}</span></i>
                            </td>
                            <td>{{$d['status_trx']}}</td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#Modal-{{$d['id']}}" class="btn btn-sm btn-info" title="Detail Confirmation"><i class="fa fa-eye"></i> &emsp;Detail</a>
                            </td>
                        </tr>
                        <div class="modal fade bd-example-modal-lg" id="Modal-{{$d['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"> Print Nota Transaction</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <ul class="list-group">
                                                    <li class="list-group-item" >
                                                    <small class="text-muted">Invoice Number : </small><br>  <a href="{{url('transaction/detail/'.$d['id'])}}" >{{$d['invoice_number']}} </a><br>
                                                    </li>
                                                    <li class="list-group-item">
                                                    <small class="text-muted">Nama Penerima : </small><br> {{$d['nama_penerima']}}<br>
                                                    </li>
                                                    <li class="list-group-item">
                                                    <small class="text-muted">Telpon Penerima : </small><br> {{$d['telpon_penerima']}}<br>
                                                    </li>
                                                    <li class="list-group-item">
                                                    <small class="text-muted">Email : </small><br> {{$d['email']}}<br>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-6">
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                    <small class="text-muted">Cashless OrderID : </small><br> {{$d['cashless_order']}}<br>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <small class="text-muted">Alamat : </small><br> {{$d['alamat_penerima']}}, {{$d['city']['type']." ".$d['city']['city']}}, {{$d['city']['province']['province']}}, {{$d['kodepos']}}<br>
                                                    </li>
                                                    <li class="list-group-item">
                                                    <small class="text-muted">Shipping : </small><br> {{$d['courier']}} {{$d['shipping_service']}}<br>
                                                    </li>
                                                    <li class="list-group-item">
                                                    <small class="text-muted">Shipping Status : </small><br> {{$d['shipping_status']}}<br>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-12">
                                            <br>
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                        <small class="text-muted">Nota ( Download / Print )</small>
                                                        <p class="mb-0"><a target="_blank" href="{{url('transaction/nota/bill/'.$d['id'])}}">Generate Nota Bill</a></p>
                                                        <p class="mb-0"><a target="_blank" href="{{url('transaction/nota/premium/'.$d['id'])}}">Generate Nota Premium</a></p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>  
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{url('print/change/'.$d['id'])}}" class="btn btn-warning">Send To Gudang</a>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection