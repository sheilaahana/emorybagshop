@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Product Detail</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('product')}}">Product</a></li>
                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
        </div>            
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <a href="{{url('product/warehouse/'.$data['id'])}}" class="btn btn-sm btn-primary" title="Themeforest"> Warehouse Check</a>
            <a href="{{url('log/stock?product='.$data['id'].'&warehouse=1')}}" class="btn btn-sm btn-info" title="Themeforest"><i class="icon-eye"></i>&emsp; Log Stock</a>
            <a href="{{url('product/form?id='.$data['id'])}}" class="btn btn-sm btn-warning" title="Themeforest"><i class="icon-pencil"></i>&emsp; Edit Product</a>

        </div>
    </div>
</div>
@if(!empty($data))
<div class="col-md-12">
    @if (Session::get('message'))
    <div class="alert alert-danger">
        {{Session::get('message')}}
    </div>
    @endif
</div>
<div class="row clearfix">
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Product Name </small>
                    <p class="mb-0">{{ucfirst($data['product_name'])}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Category / Type</small>
                    <p class="mb-0">{{$data['category']['category_name']}} / {{$data['category']['type']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Berat </small>
                    <p class="mb-0">{{$data['berat']}} kg</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Description</small>
                    <p class="mb-0">{!!$data['description']!!}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Specification</small>
                    <p class="mb-0">{!!$data['specification']!!}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Default Image</small>
                    <p><img src="{{$data['image']}}" alt="" style="width:100%;max-width:300px"></p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Informasi Product</small>
                    <p class="mb-0">On Wishlist : {{Generate::number($data['on_wishlist'])}} Items</p>
                    <p class="mb-0">On Cart : {{Generate::number($data['on_cart'])}} Items</p>

                </li>
                <li class="list-group-item">
                    <small class="text-muted">Harga <b>Default</b> </small>
                    <p class="mb-0">Harga Jual : {{Generate::money($data['harga_jual'])}}</p>
                    <p class="mb-0">Harga Diskon : {{Generate::money($data['harga_diskon'])}}</p>
                </li>
                @forelse($data['harga'] as $h)
                <li class="list-group-item">
                    <small class="text-muted">Harga <b>{{$h['category']['category']}}</b> </small>
                    <p class="mb-0">Harga Jual : {{Generate::money($h['harga_jual'])}}</p>
                    <p class="mb-0">Harga Diskon : {{Generate::money($h['harga_diskon'])}}</p>
                </li>
                @empty
                <li class="list-group-item">
                    <small class="text-muted">Harga User Category </small>
                    <p class="mb-0"><i>Undefinded</i></p>
                </li>
                @endforelse
            </ul>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Created Date </small>
                    <p class="mb-0">{{date('d F y - h:i', strtotime($data['created_at']))}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Count Seen</small>
                    <p class="mb-0">{{Generate::number($data['payment_target'])}} seen</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Is Promo</small>
                    <?php if($data['is_promo']=="0"){$badge='badge-danger';}elseif($data['is_promo']=='1'){$badge='badge-success';}else{$badge='badge-primary';} ?>
                    <p class="mb-0"><a href="{{url('product/status/is_promo/'.$data['id'])}}" onclick="return confirm('Are you sure you want to Change this?');" class="badge {{$badge}} ml-0 mr-0">{{$data['is_promo']=='1'?'True':'False'}}</a></p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Is Best Seller</small>
                    <?php if($data['is_bestseller']=="0"){$badge='badge-danger';}elseif($data['is_bestseller']=='1'){$badge='badge-success';}else{$badge='badge-primary';} ?>
                    <p class="mb-0"><a href="{{url('product/status/is_bestseller/'.$data['id'])}}" onclick="return confirm('Are you sure you want to Change this?');" class="badge {{$badge}} ml-0 mr-0">{{$data['is_bestseller']=='1'?'True':'False'}}</a></p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Is Hot Item</small>
                    <?php if($data['is_hotitem']=="0"){$badge='badge-danger';}elseif($data['is_hotitem']=='1'){$badge='badge-success';}else{$badge='badge-primary';} ?>
                    <p class="mb-0"><a href="{{url('product/status/is_hotitem/'.$data['id'])}}" onclick="return confirm('Are you sure you want to Change this?');" class="badge {{$badge}} ml-0 mr-0">{{$data['is_hotitem']=='1'?'True':'False'}}</a></p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Is Most Wanted</small>
                    <?php if($data['is_mostwanted']=="0"){$badge='badge-danger';}elseif($data['is_mostwanted']=='1'){$badge='badge-success';}else{$badge='badge-primary';} ?>
                    <p class="mb-0"><a href="{{url('product/status/is_mostwanted/'.$data['id'])}}" onclick="return confirm('Are you sure you want to Change this?');" class="badge {{$badge}} ml-0 mr-0">{{$data['is_mostwanted']=='1'?'True':'False'}}</a></p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Is Publish</small>
                    <?php if($data['is_publish']=="0"){$badge='badge-danger';}elseif($data['is_publish']=='1'){$badge='badge-success';}else{$badge='badge-primary';} ?>
                    <p class="mb-0"><a href="{{url('product/status/is_publish/'.$data['id'])}}" onclick="return confirm('Are you sure you want to Change this?');" class="badge {{$badge}} ml-0 mr-0">{{$data['is_publish']=='1'?'True':'False'}}</a></p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="card" style="height:340px;overflow:auto;background-color:white;padding:20px">
            <b>Log Change</b>
            <ul>
                @forelse($data['log'] as $log)
                    <li><b>{{$log['change_by']}} {{$log['admin']}}</b> <br>{{$log['description']}}<br> <i>{{date('d/m/y h:i:s',strtotime($log['created_at']))}}</i></li>
                @empty
                    <li>Empty Data</li>
                @endforelse
            </ul>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-md-6 col-sm-12">  
                        <h5>List Item Product Variant</h5>
                    </div>
                    <div class="col-md-5 col-sm-12 text-right hidden-xs">
                        <a href="{{url('product/mass/'.$data['id'])}}" class="btn btn-sm btn-info" title="Themeforest"><i class="icon-pencil"></i>&emsp; Mass Variant</a>
                        <a href="{{url('product/variant/form?product_id='.$data['id'])}}" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-plus"></i>&emsp; Add Variant</a>
                    </div>
                </div><br>
                <table class="table table-hover spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Variant</th>
                            <th>Qty / Valid Stock</th>
                            <th>Hexa</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($data['variant'])>0)
                        @foreach($data['variant'] as $d)
                        <tr>
                            <td><span>{{$d['id']}}</span></td>
                            <td><span>{{$d['variant']}}</span></td>
                            <td><span>{{$d['qty_stock']}} pcs / {{$d['valid_stock']}} pcs</span></td>
                            <td>
                                <div style="background-color:{{$d['hexa']}};width:30px;height:30px;border-radius:100%;margin:5px 20px"></div> {{$d['hexa']}}
                            </td>
                            <td><img src="{{$d['image']}}" alt="" style="width:100%;max-width:100px"></td>
                            <td>
                                <a href="{{url('cart/form?id='.$d['id'])}}" class="btn btn-sm btn-primary" title="Add to Cart"><i class="fa fa-plus"></i></a>
                                <a href="{{url('product/variant/form?id='.$d['id'])}}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="{{url('product/variant/delete/'.$d['id'])}}" onclick="return confirm('Are you sure you want to delete this item')" class="btn btn-sm btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5"><b><i><center>Invalid Data Product</center></i></b></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@else
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <center>
                <h6>Invalid Data / Data not Found</h6>
                <hr>
                <a href="{{url('product')}}"> Back to List Product </a>
                </center>
            </div>
        </div>
    </div>
</div>

@endif
@endsection