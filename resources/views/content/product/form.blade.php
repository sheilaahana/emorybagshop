@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Product Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('product')}}">Product</a></li>
                @if(isset($data->id))
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('product/detail/'.$data->id)}}">Detail</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Product</li>
                @else
                <li class="breadcrumb-item active" aria-current="page">Add Product</li>
                @endif
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="card">
    <div class="body">
        <form action="{{url('/product/save')}}" method="post" class="m-form" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input name="id" type="hidden" class="form-control m-input" value="{{ empty($data->id)?old('id'):$data->id}}" placeholder="id">
            <div class="row clearfix">
                <div class="form-group col-md-6">
                    <label>Product Name</label>
                    <input type="text" name="product_name" value="{{ empty($data->product_name)?old('product_name'):$data->product_name}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-6">
                    <label>Image</label>
                    <input type="file" name="image"  class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label>Product Category / Type </label>
                    <select id="single-selection" name="category_id" class="form-control" required>
                        @foreach($data['productcategory'] as $pc)
                        <option value="{{$pc['id']}}" {{!empty($data['category_id'])&&$data['category_id']==$pc['id']?'selected':''}}>{{$pc['category_name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>Type</label>
                    <br>
                    <label class="fancy-radio">
                        <input type="radio" name="type_product" value="Ready Stock" required="" data-parsley-errors-container="#error-radio"  {{!empty($data['type_product'])&&$data['type_product']||old('type_product')=='Ready Stock'?'checked':''}} >
                        <span><i></i>Ready Stock</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="type_product" value="PO" {{!empty($data['type_product'])&&$data['type_product']||old('type_product')=='PO'?'checked':''}} >
                        <span><i></i>PO</span>
                    </label>
                    <p id="error-radio"></p>
                </div>
                <div class="form-group col-md-6">
                    <label>Description</label>
                    <textarea class="form-control" name="description" rows="10" required="">{{ empty($data->description)?old('description'):$data->description}}</textarea>
                </div>
                <div class="form-group col-md-6">
                    <label>Specification</label>
                    <textarea class="form-control" name="specification" rows="10" required="">{{ empty($data->specification)?old('specification'):$data->specification}}</textarea>
                </div>
                <div class="form-group col-md-6">
                    <label>Berat</label>
                    <input type="number" name="berat" step=".01" value="{{ empty($data->berat)?old('berat'):$data->berat}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-6">
                    <label>Harga Modal</label>
                    <input type="number" name="harga_modal" value="{{ empty($data->harga_modal)?old('harga_modal'):$data->harga_modal}}" class="form-control" required="">
                </div>
                <div class="form-group  col-md-6">
                    <label>Harga Jual ( Default ) </label>
                    <input type="number" min=0 name="harga_jual" value="{{ empty($data->harga_jual)?0:$data->harga_jual}}" class="form-control" required="">
                </div>
                <div class="form-group  col-md-6">
                    <label>Harga Diskon ( Default ) * kosongkan apabila tidak ada </label>
                    <input type="number" min=0 name="harga_diskon" value="{{ empty($data->harga_diskon)?0:$data->harga_diskon}}" class="form-control" required="">
                </div>
                @foreach($data['usercategory'] as $uc)
                <div class="form-group  col-md-6">
                    <label>Harga Jual ( {{$uc['category']}} ) </label>
                    <input type="number" min=0 name="price[{{$uc['id']}}][harga_jual]" value="{{ empty($data['harga'][$uc['id']]['harga_jual'])?0:$data['harga'][$uc['id']]['harga_jual']}}" class="form-control" >
                </div>
                <div class="form-group  col-md-6">
                    <label>Harga Diskon ( {{$uc['category']}} ) * kosongkan apabila tidak ada </label>
                    <input type="number" min=0 name="price[{{$uc['id']}}][harga_diskon]" value="{{ empty($data['harga'][$uc['id']]['harga_diskon'])?0:$data['harga'][$uc['id']]['harga_diskon']}}" class="form-control" >
                </div>
                @endforeach
                
            </div>
            
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
@endsection