@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Product</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Product</li>
                </ol>
            </nav>
        </div>            
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <a href="{{url('product')}}" class="btn btn-sm btn-info" title="">All Product</a>
            <a href="?status=published" class="btn btn-sm btn-success" title="">Published Product</a>
            <a href="?status=unpublish" class="btn btn-sm btn-warning" title="">Unpublish Product</a>
            <a href="{{url('product/form')}}" class="btn btn-sm btn-primary" title="">Add Product</a>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>No</th>
                            <th>Product Name</th>
                            <th>Image</th>
                            <th>Category</th>
                            <th>Type</th>
                            <th>Publish</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $a=1;?>
                        @foreach($data as $d)
                        <tr>
                            <td><span>{{$a++}}</span></td>
                            <td>
                                <b>{{$d['product_name']}}</b><br>
                                Harga Modal : {{Generate::money($d['harga_modal'])}}<br>
                                Harga Jual : {{Generate::money($d['harga_jual'])}}<br>
                                Harga Diskon : {{Generate::money($d['harga_diskon'])}}<br>
                            </td>
                            <td>
                                <img src="{{$d['image']}}" alt="" style="width:100%;max-width:100px">                                
                            </td>
                            <td>{{$d['category']['category_name']}} </td>
                            <td>{{$d['type_product']}}</td>
                            <td>
                                <?php if($d['is_publish']=="0"){$badge='badge-danger';}elseif($d['is_publish']=='1'){$badge='badge-success';}else{$badge='badge-primary';} ?>
                                <a href="{{url('product/status/is_publish/'.$d['id'])}}" onclick="return confirm('Are you sure you want to Change this?');" class="badge {{$badge}} ml-0 mr-0">{{$d['is_publish']=='1'?'True':'False'}}</a>
                            </td>
                            <td>
                                <a href="{{url('product/detail/'.$d['id'])}}" class="btn btn-sm btn-info" title="Detail"><i class="fa fa-eye"></i>  </a>
                                <a href="{{url('product/form?id='.$d['id'])}}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="{{url('product/delete/'.$d['id'])}}" onclick="return confirm('Are you sure you want to delete this item')" class="btn btn-sm btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection