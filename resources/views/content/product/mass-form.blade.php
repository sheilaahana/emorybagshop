@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Product Variant Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('product')}}">Product</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('product/detail/'.$id)}}">Detail</a></li>
                <li class="breadcrumb-item active" aria-current="page">Variant Form</li>
                </ol>
            </nav>
        </div>            
    </div>
</div>
<div class="card">
    <div class="body" style="overflow-x:auto">
        <form action="{{url('/product/mass/save')}}" method="post" class="m-form" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input name="product_id" type="hidden" class="form-control m-input" value="{{$id}}" placeholder="id">
            <table class="table table-hover spacing5 mb-0">
                <thead >
                    <tr>
                        <th>ID</th>
                        <th>Variant</th>
                        <th></th>
                        <th>Image</th>
                        <th>Qty</th>
                        <th>Hexa</th>
                    </tr>
                </thead>
                <tbody class="add">
                @foreach($data['variant'] as $v)
                    <tr>
                        <td>{{$v['id']}}</td>
                        <td>
                            <div class="form-group ">
                                <input type="text" name="p[{{$v['id']}}][variant]" value="{{ $v['variant']}}" class="form-control" required="">
                            </div>
                        </td>
                        <td><img src="{{$v['image']}}" alt="" style="width:100%;max-width:100px"></td>
                        <td>
                            <div class="form-group ">
                                <input type="file" name="p[{{$v['id']}}][image]"  class="form-control">
                            </div>
                        </td>
                        <td>
                            <div class="form-group ">
                                <input type="number" name="p[{{$v['id']}}][qty_stock]" min="0"  value="{{$v['qty_stock']}}" class="form-control" required="">
                            </div>
                        </td>
                        <td>
                            <div class="input-group colorpicker">                                   
                                <input type="text" name="p[{{$v['id']}}][hexa]" class="form-control" value="{{ $v['hexa']}}" required>
                                <div class="input-group-append">
                                    <span class="input-group-text"><span class="input-group-addon"><i></i></span></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    <tr id="varadd">
                        <td>Add</td>
                        <td>
                            <div class="form-group ">
                                <input type="text" name="new[variant][]" value="" class="form-control">
                            </div>
                        </td>
                        <td></td>
                        <td>
                            <div class="form-group ">
                                <input type="file" name="new[image][]"  class="form-control">
                            </div>
                        </td>
                        <td>
                            <div class="form-group ">
                                <input type="number" name="new[qty_stock][]" min="0"  class="form-control">
                            </div>
                        </td>
                        <td>
                            <div class="input-group colorpicker">                                   
                                <input type="text" name="new[hexa][]" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text"><span class="input-group-addon"><i></i></span></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        <center>&emsp;<button type="submit" class="btn btn-danger">Save Data</button></center>
        </form>
        <button class="btn btn-primary" id="add">Add Data</button>
    </div>
</div>
@endsection
@push('scripts')
<script>
$(document).ready(function(){
  $("#add").click(function(){
    $("#varadd").clone().appendTo("tbody");
  });
});
</script>
@endpush