@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<style>
    td{
        text-align: center;
    }
</style>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Product Detail - Log Stock</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('product')}}">Product</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('product/detail/'.$id)}}">Detail</a></li>
                <li class="breadcrumb-item active" aria-current="page">LogStock</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
@if(!empty($data))
<div class="col-md-12">
    @if (Session::get('message'))
    <div class="alert alert-danger">
        {{Session::get('message')}}
    </div>
    @endif
</div>
<div class="row clearfix">
@foreach($data['warehouse'] as $w)
    <div class="col-lg-6 col-md-6">
        <div class="card" style="height:340px;overflow:auto;background-color:white;padding:20px">
            <b>Warehouse {{$w['warehouse']}} ( {{$w['code']}} )</b><hr>
            <table width="100%" border=1>
                <tr style="background-color:lightblue;">
                    <td>Variant</td>
                    <td>DB / CR</td>
                    <td>Desc</td>
                    <td>Change By</td>
                    <td>Time</td>
                </tr>
                @forelse($w['log_stock'] as $log)
                <tr>
                    <td>{{$log['variant']['variant']}}</td>
                    <td>
                        @if($log['type'] == "minus")
                            - 
                        @elseif($log['type'] == "plus")
                            +
                        @endif
                        {{$log['stock']}}
                    </td>
                    <td>{{$log['description']}}</td>
                    <td>{{$log['change_by']}} {{$log['admin']}}</td>
                    <td>{{date('d/m/y h:i:s',strtotime($log['created_at']))}}</td>
                </tr>
                @empty
                <tr><td colspan="5">Empty Data</td></tr>
                @endforelse
            </table>
        </div>
    </div>
@endforeach
</div>
@else
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <center>
                <h6>Invalid Data / Data not Found</h6>
                <hr>
                <a href="{{url('product')}}"> Back to List Product </a>
                </center>
            </div>
        </div>
    </div>
</div>

@endif
@endsection