@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Product Variant Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('product')}}">Product</a></li>
                @if(isset($data->id))
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('product/detail/'.$data['product_id'])}}">Detail</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Variant</li>
                @else
                <li class="breadcrumb-item active" aria-current="page">Add Variant</li>
                @endif
                </ol>
            </nav>
        </div>            
    </div>
</div>
<div class="card">
    <div class="body" style="overflow-x:auto">
        <form action="{{url('/product/variant/save')}}" method="post" class="m-form" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input name="id" type="hidden" class="form-control m-input" value="{{ empty($data->id)?old('id'):$data->id}}" placeholder="id">
            <?php if(isset($_GET['product_id'])){ $product_id=$_GET['product_id']; $image="required"; }else{ $product_id=''; $image="";} ?>
            <input name="product_id" type="hidden" class="form-control m-input" value="{{$product_id}}" placeholder="id">
            <div class="row clearfix varadd">
                <div class="form-group col-md-6">
                    <label>Variant</label>
                    <input type="text" name="variant" value="{{ empty($data->variant)?old('variant'):$data->variant}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-6">
                    <label>Image</label>
                    <input type="file" name="image"  class="form-control" >
                </div>
                <div class="form-group col-md-6">
                    <label>Qty Stock</label>
                    <input type="number" name="qty_stock" value="{{ empty($data->qty_stock)?old('qty_stock'):$data->qty_stock}}" class="form-control" required="">
                </div>
                
                <div class="col-md-6"><b>HEX CODE</b>
                    <div class="input-group colorpicker">                                   
                        <input type="text" name="hexa" class="form-control" value="{{ empty($data->hexa)?old('hexa'):$data->hexa}}" required>
                        <div class="input-group-append">
                            <span class="input-group-text"><span class="input-group-addon"><i></i></span></span>
                        </div>
                    </div>
                </div>
            </div>
            
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
@endsection
