@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; 


?>

<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Product Detail</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('product')}}">Product</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('product/detail/'.$data['id'])}}">Detail</a></li>
                <li class="breadcrumb-item active" aria-current="page">Warehouse Check</li>
                </ol>
            </nav>
        </div>            
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <a href="{{url('product/warehouse/'.$data['id'])}}" class="btn btn-sm btn-primary" title="Themeforest"> Warehouse Check</a>
            <a href="{{url('log/stock?product='.$data['id'].'&warehouse=1')}}" class="btn btn-sm btn-info" title="Themeforest"><i class="icon-eye"></i>&emsp; Log Stock</a>
            <a href="{{url('product/form?id='.$data['id'])}}" class="btn btn-sm btn-warning" title="Themeforest"><i class="icon-pencil"></i>&emsp; Edit Product</a>

        </div>
    </div>
</div>
@if(!empty($data['id']))
<div class="col-md-12">
    @if (Session::get('message'))
    <div class="alert alert-danger">
        {{Session::get('message')}}
    </div>
    @endif
</div>
<div class="row clearfix">
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Product Name </small>
                    <p class="mb-0">{{ucfirst($data['product_name'])}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Category / Type</small>
                    <p class="mb-0">{{$data['category']['category_name']}} / {{$data['category']['type']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Berat </small>
                    <p class="mb-0">{{$data['berat']}} kg</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Description</small>
                    <p class="mb-0">{!!$data['description']!!}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Specification</small>
                    <p class="mb-0">{!!$data['specification']!!}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Default Image</small>
                    <p><img src="{{$data['image']}}" alt="" style="width:100%;max-width:300px"></p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <h3>Warehouse Check</h3>
                    <table class="table table-custom">
                        @foreach($data['warehouse'] as $w)
                        <tr >
                            <td> <b>{{$w['warehouse']}} ( {{$w['code']}} )</td>
                            <td><a href="{{url('warehouse/detail/'.$w['id'])}}" class="btn btn-sm btn-info" style="zoom:75%">Detail Warehouse</a></td>
                        </tr>
                        <tr style="font-weight:600">
                            <td width="80">Hexa</td>
                            <td>Variant</td>
                            <td>Stock</td>
                        </tr>
                        @forelse($data['warehouse_product'] as $p)
                            @if($p['warehouse_id'] == $w['id'])
                        <tr >
                            <td><div style="background-color:{{$p['variant']['hexa']}};width:30px;height:30px;border-radius:100%;margin:5px 20px"></div></td>
                            <td>{{$p['variant']['variant']}}</td>
                            <td>{{$p['stock']}}</td>
                        </tr>
                        @endif
                        @empty
                        <tr>
                            <td>Empty Stock</td>
                        </tr>
                        @endforelse
                        @endforeach
                       
                    </table>
                </li>
            </ul>
        </div>
    </div>
</div>
@else
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <center>
                <h6>Invalid Data / Data not Found</h6>
                <hr>
                <a href="{{url('product')}}"> Back to List Product </a>
                </center>
            </div>
        </div>
    </div>
</div>

@endif
@endsection