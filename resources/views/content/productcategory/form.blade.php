@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Product Category Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('category/product')}}">Product Category</a></li>
                <li class="breadcrumb-item active" aria-current="page"> Form</li>
                </ol>
            </nav>
        </div>           
    </div>
</div>
<div class="card">
    <div class="body">
        <form action="{{url('/category/product/save')}}" method="post" class="m-form" id="fnotaris" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input name="id" type="hidden" class="form-control m-input" value="{{ empty($data->id)?old('id'):$data->id}}" placeholder="id">
            
            <div class="form-group">
                <label>Category</label>
                <input type="text" name="category_name" value="{{ empty($data->category_name)?old('category_name'):$data->category_name}}" class="form-control" required="">
            </div>
            <div class="form-group">
                <label>Icon</label>
                <input type="file" name="icon"  class="form-control">
            </div>
            <!-- <div class="form-group">
                <label>Radio Button</label>
                <br>
                <label class="fancy-radio">
                    <input type="radio" name="type" value="Ready Stock" required="" data-parsley-errors-container="#error-radio"  {{!empty($data['type'])&&$data['type']||old('type')=='Ready Stock'?'checked':''}} >
                    <span><i></i>Ready Stock</span>
                </label>
                <label class="fancy-radio">
                    <input type="radio" name="type" value="PO" {{!empty($data['type'])&&$data['type']||old('type')=='PO'?'checked':''}} >
                    <span><i></i>PO</span>
                </label>
                <p id="error-radio"></p>
            </div> -->
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="description" required="">{{ empty($data->description)?old('description'):$data->description}}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
@endsection