@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Administrator Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Administrator</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="col-md-12">
    @if (Session::get('message'))
    <div class="alert alert-danger">
        {{Session::get('message')}}
    </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<div class="card">
    <div class="body">
    <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    </div>
                <div class="col-md-6">
                     <h5>Setting Profile</h5><hr>
                     <div class="col-md-12">
                        @if (Session::get('message-profile'))
                        <div class="alert alert-warning">
                            {{Session::get('message-profile')}}
                        </div>
                        @endif
                    </div>
                    <form action="{{url('profile/setting/profile')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" name="name" value="{{$data['name']}}" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" name="email" value="{{$data['email']}}" class="form-control m-input" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <button class="btn btn-info" style="margin: 5px" type="submit">Save Profile</button>
                    </form>
                </div>
                <div class="col-md-6">
                    <h5>Change Password</h5><hr>
                    <div class="col-md-12">
                        @if (Session::get('message-pass'))
                        <div class="alert alert-warning">
                            {{Session::get('message-pass')}}
                        </div>
                        @endif
                    </div>
                    <form action="{{url('profile/setting/password')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <input class="form-control" name="email" type="hidden" style="margin: 5px" placeholder="Current Password" required>
                        <input class="form-control" name="current_pass" type="password" style="margin: 5px" placeholder="Current Password" required>
                        <input class="form-control" name="new_pass" type="password" style="margin: 5px" placeholder="New Password" required>
                        <input class="form-control" name="repeat_pass" type="password" style="margin: 5px" placeholder="Repeat Password" required>
                        <hr>
                        <button class="btn btn-info" style="margin: 5px" type="submit">Save Password</button>
                    </form>
                </div>
            </div>
    </div>
</div>
@endsection