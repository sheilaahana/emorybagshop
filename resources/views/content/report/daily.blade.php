<?php use App\Helpers\Generate as Generate; $total=[];$ware=[];$tot = 0; ?>
<style>
    th,td{
        padding:5px 10px;
    }
    .bg{
        background-color: #ccc;

    }
</style>
<h5>Report Transaction {{$data['date']}}</h5>
<table border="1" style="width:100%">
    <thead >
        <tr class="hide">
            <td></td>
            <td colspan="3"><h5>Report Transaction </h5></td>
        </tr>
        <tr  class="hide">
            <td></td>
            <td>Tanggal</td>
            <td>{{$data['date']}}</td>
        </tr>
        <tr class="bg">
            <th></th>
            <th>No</th>
            <th>Kurir</th>
            @foreach($data['warehouse'] as $w)
            <?php $ware[$w['id']] = 0; ?>

                <th>{{$w['warehouse']}} ( {{$w['code']}} ) </th>
            @endforeach
            <th>total</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1;?>
       
        @forelse($data['courier'] as $c)
        <?php $total[$c['courier']] = 0; ?>
        <tr>
            <td></td>
            <td>{{$no++}}</td>
            <td>{{$c['courier']}}</td>
            @foreach($data['warehouse'] as $w)
                <?php 
                    $qty = $data['report'][$c['courier']][$w['id']]; 
                    $total[$c['courier']] = $total[$c['courier']]+$qty; 
                    $ware[$w['id']] = $ware[$w['id']]+$qty; 
                    ?>
                <td>
                @if($qty!=0)
                {{$qty}}
                @else
                -
                @endif
                </td>
               
            @endforeach
            <td>
            @if($total[$c['courier']]!=0)
            {{$total[$c['courier']]}}
            @else
            -
            @endif
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="9"><center>Empty Data</center></td>
        </tr>
        @endforelse
        <tr class="bg">
            <td></td><td></td><td><b>Grand Total</b></td>
            @foreach($data['warehouse'] as $w)
            <?php $tot = $tot+$ware[$w['id']]; ?>
                <th>{{$ware[$w['id']]}} </th>
            @endforeach
            <th>{{$tot}}</th>
            </tr>
    </tbody>
</table>