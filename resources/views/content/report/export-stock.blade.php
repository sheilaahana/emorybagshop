<?php use App\Helpers\Generate as Generate;?>

@foreach($data['product'] as $d)
    <table  border=1>
        <tr >
            <th class="bg">Product Name</th>
            <th >{{$d['product_name']}} </th>
        </tr>
        <tr >
            <th class="bg">Category</th>
            <th >{{$d['category']['category_name']}}</th>
        </tr>
        <tr >
            <th class="bg">Warehouse</th>
            <th >{{Generate::wh($param['warehouse'])}}</th>
        </tr>
        <tr class="bg">
            <th>Variant</th>
            <th>Qty Stock</th>
        </tr>
        @foreach($d['variant'] as $v)
        <tr>
            <td>{{$v['variant']}} {{$v['id']}}</td>
            <?php
                if(isset($data['stock'][$d['id']][$v['id']])){
                    $stock = $data['stock'][$d['id']][$v['id']];
                } else {
                    $stock = 0;
                }
            ?>
            <td class="{{$stock<='0'?'red':''}}">
                {{$stock}}
            </td>
        </tr>
        @endforeach
        <tr><td></td><td></td></tr>
    </table>
    @endforeach
