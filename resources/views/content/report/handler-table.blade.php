<?php use App\Helpers\Generate as Generate; ?>
<style>
table{
    width:100%;
}
th,td{
    padding:5px 10px;
}
.bg{
    background-color: #ccc;

}
    </style>
<table border=1>
    <tbody>
        <?php $no=1;?>
            <tr><th colspan=3 class="bg">Customer Service</th></tr>
        @forelse($data['customer_service'] as $date => $name)
            <tr><td rowspan="{{count($name)+1}}" width="200px">{{$date}} </td>
            @foreach($name as $name => $value)
            <tr>
                <td>{{$name}}</td>
                <td>{{$value}}</td>
            </tr>
            @endforeach
        @empty
            <tr><td colspan=2>Empty Data</td></tr>
        @endforelse

        <tr><th colspan=3>&emsp;</th></tr>
        <tr><th colspan=3 class="bg">Quality Control</th></tr>
        @forelse($data['quality_control'] as $date => $name)
            <tr><td rowspan="{{count($name)+1}}" width="200px">{{$date}} </td>
            @foreach($name as $name => $value)
            <tr>
                <td>{{$name}}</td>
                <td>{{$value}}</td>
            </tr>
            @endforeach
        @empty
            <tr><td colspan=2>Empty Data</td></tr>
        @endforelse

        <tr><th colspan=3>&emsp;</th></tr>
        <tr><th colspan=3 class="bg">Gudang</th></tr>
        @forelse($data['gudang'] as $date => $name)
            <tr><td rowspan="{{count($name)+1}}" width="200px">{{$date}} </td>
            @foreach($name as $name => $value)
            <tr>
                <td>{{$name}}</td>
                <td>{{$value}}</td>
            </tr>
            @endforeach
        @empty
            <tr><td colspan=2>Empty Data</td></tr>
        @endforelse
        
        <tr><th colspan=3>&emsp;</th></tr>
        <tr><th colspan=3 class="bg">Packing</th></tr>
        @forelse($data['packing'] as $date => $name)
            <tr><td rowspan="{{count($name)+1}}" width="200px">{{$date}} </td>
            @foreach($name as $name => $value)
            <tr>
                <td>{{$name}}</td>
                <td>{{$value}}</td>
            </tr>
            @endforeach
        @empty
            <tr><td colspan=2>Empty Data</td></tr>
        @endforelse
    </tbody>
</table>