@extends('layouts.app')

@section('content')
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Handler</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Handler Summary</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <div class="col-lg-12 col-md-12">
                    <form action="{{url('report/handler/export')}}" method="post" class="m-form">
                        <input type="hidden" name="param" value="{{ empty($param)?'':json_encode($param)}}" class="form-control" aria-describedby="basic-addon1">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <button type="submit" style="float:right" class="btn btn-primary">Export Data</button>
                    </form>
                    <br>
                    <br>
                </div>
                <div class="col-md-12" style="overflow-x:auto">
                @include('content.report.handler-table')                
                </div>
                <!-- -->
            </div>
        </div>
    </div>
</div>
@endsection