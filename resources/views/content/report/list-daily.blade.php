@extends('layouts.app')

@section('content')
<style>
    .hide{
        display: none;
    }
</style>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Report</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Report</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <form action="" method="post" class="m-form">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Date Transaction</b></span>
                        </div>
                        <input type="date" name="date" value="{{ empty($data['date'])?old('date'):$data['date']}}" class="form-control" aria-describedby="basic-addon1">
                    </div>
                    <div class="col-lg-12 col-md-12"><br>
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="{{url('report/daily')}}" class="btn btn-danger">Reset</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <div class="col-lg-12 col-md-12">
                    <form action="{{url('report/daily/export')}}" method="post" class="m-form">
                        <input type="hidden" name="date" value="{{ empty($data['date'])?'':$data['date']}}" class="form-control" aria-describedby="basic-addon1">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <button type="submit" style="float:right" class="btn btn-primary">Export Data</button>
                    </form>
                    <br>
                    <br>
                </div>
                @include('content.report.daily')                
                <!-- -->
            </div>
        </div>
    </div>
</div>
@endsection