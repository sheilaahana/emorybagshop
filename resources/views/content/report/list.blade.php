@extends('layouts.app')

@section('content')
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Report</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Report</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <form action="" method="post" class="m-form">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Customer Name</b></span>
                        </div>
                        <input type="text" name="customer" value="{{ empty($param['customer'])?old('customer'):$param['customer']}}" class="form-control" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Invoice Number</b></span>
                        </div>
                        <input type="text" name="invoice" value="{{ empty($param['invoice'])?old('invoice'):$param['invoice']}}" class="form-control" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Start Sales Date</b></span>
                        </div>
                        <input type="date" name="start_date" value="{{ empty($param['start_date'])?old('start_date'):$param['start_date']}}" class="form-control" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>End Sales Date</b></span>
                        </div>
                        <input type="date" name="end_date" value="{{ empty($param['end_date'])?old('end_date'):$param['end_date']}}" class="form-control" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Confirmed Date</b></span>
                        </div>
                        <input type="date" name="confirmed_date" value="{{ empty($param['confirmed_date'])?old('confirmed_date'):$param['confirmed_date']}}" class="form-control" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Gudang</b></span>
                        </div>
                        <input type="date" name="change_gudang" value="{{ empty($param['change_gudang'])?old('change_gudang'):$param['change_gudang']}}" class="form-control" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Ekspedisi</b></span>
                        </div>
                        <input type="date" name="change_shipping" value="{{ empty($param['change_shipping'])?old('change_shipping'):$param['change_shipping']}}" class="form-control" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Finish</b></span>
                        </div>
                        <input type="date" name="set_shipping" value="{{ empty($param['set_shipping'])?old('set_shipping'):$param['set_shipping']}}" class="form-control" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Product Name</b></span>
                        </div>
                        <input type="text" name="product" value="{{ empty($param['product'])?old('product'):$param['product']}}" class="form-control" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01"><b>Show Data</b></label>
                        </div>
                        <select class="custom-select" name="data" id="inputGroupSelect01">
                            <option value="">Choose .. ( Default 20 data )</option>
                            <option value="10" {{!empty($param['data'])&&$param['data']||old('data')=='10'?'selected':''}} > 10 Data</option>
                            <option value="50" {{!empty($param['data'])&&$param['data']||old('data')=='50'?'selected':''}} > 50 Data</option>
                            <option value="250" {{!empty($param['data'])&&$param['data']||old('data')=='250'?'selected':''}}>250 Data</option>
                            <option value="500" {{!empty($param['data'])&&$param['data']||old('data')=='500'?'selected':''}}>500 Data</option>
                            <option value="all" {{!empty($param['data'])&&$param['data']||old('data')=='all'?'selected':''}}>Show All Data</option>
                        </select>
                    </div>
                    <div class="col-lg-12 col-md-12"><br>
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="{{url('report')}}" class="btn btn-danger">Reset</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <div class="col-lg-12 col-md-12">
                    <form action="{{url('report/export')}}" method="post" class="m-form">
                        <input type="hidden" name="param" value="{{ empty($param)?'':json_encode($param)}}" class="form-control" aria-describedby="basic-addon1">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <button type="submit" style="float:right" class="btn btn-primary">Export Data</button>
                    </form>
                    <br>
                    <br>
                </div>
                <div class="col-md-12" style="overflow-x:auto">
                @include('content.report.table')                
                </div>
                <!-- -->
            </div>
        </div>
    </div>
</div>
@endsection