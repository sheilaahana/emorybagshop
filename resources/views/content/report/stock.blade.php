@extends('layouts.app')

@section('content')

<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Report Stock</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page">Report</li>
                <li class="breadcrumb-item active" aria-current="page">Stock</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <form action="" method="get" class="m-form">
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01"><b>Warehouse</b></label>
                        </div>
                        <select class="custom-select" name="warehouse" id="inputGroupSelect01">
                            <option value="">Default Master</option>
                            @foreach($data['master']['warehouse'] as $w)
                            <option value="{{$w['id']}}"  {{!empty($_GET['warehouse'])&&$_GET['warehouse']==$w['id']?'selected':''}}> {{$w['warehouse']}} ( {{$w['code']}} )</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01"><b>Category</b></label>
                        </div>
                        <select class="custom-select" name="category" id="inputGroupSelect01">
                            <option value="">All Category </option>
                            @foreach($data['master']['category'] as $p)
                            <option value="{{$p['id']}}" {{!empty($_GET['category'])&&$_GET['category']==$p['id']?'selected':''}} > {{$p['category_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-12 col-md-12"><br>
                        <button type="submit" class="btn btn-primary">Search</button>
                        <a href="{{url('report')}}" class="btn btn-danger">Reset</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <div class="col-lg-12 col-md-12">
                    <form action="{{url('report/stock/export')}}" method="post" class="m-form">
                        <input type="hidden" name="param" value="{{ empty($param)?'':json_encode($param)}}" class="form-control" aria-describedby="basic-addon1">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <button type="submit" style="float:right" class="btn btn-primary">Export Data</button>
                    </form>
                    <br>
                    <br>
                </div>
                <div class="col-md-12" style="overflow-x:auto">
                @include('content.report.table-stock')                
                </div>
                <!-- -->
            </div>
        </div>
    </div>
</div>
@endsection