<?php use App\Helpers\Generate as Generate; ?>
<?php use App\Helpers\ProductGenerate as Prod; ?>
<style>
    th,td{
        padding:5px 10px;
        border: 1px solid black;
    }
    .bg{
        background-color: #ccc;

    }
    .red{
        color: red;
    }
    .orange{
        color: orange;
    }
    table{
        width : 100%
    }
</style>
<div class="row">
@if($param['warehouse'] == 1)
    @foreach($data['product'] as $d)
    <div class="col-lg-6 col-md-12" style="padding:10px">
    <table  border=1>
        <tr >
            <th class="bg">Product Name</th>
            <th colspan=6>{{$d['product_name']}} <a href="{{url('product/detail/'.$d['id'])}}" target="_blank"><br> ( Detail Product )</a></th>
        </tr>
        <tr >
            <th class="bg">Category</th>
            <th colspan=6>{{$d['category']['category_name']}}</th>
        </tr>
        <tr >
            <th class="bg">Warehouse</th>
            <th colspan=6>{{Generate::wh($param['warehouse'])}}</th>
        </tr>
        <tr class="bg">
            <th>Variant</th>
            <th>Qty Stock</th>
            <th>Valid Stock</th>
            <th>On Cart</th>
            <th>Rak</th>
            <th>Baris</th>
            <th>Kolom</th>
        </tr>
        @foreach($d['variant'] as $v)
        <tr>

            <td>{{$v['variant']}}</td>
            <td class="{{$v['qty_stock']<='5'?'orange':''}}" >{{$v['qty_stock']}}</td>
            <td class="{{$v['valid_stock']<='0'?'red':''}}">{{$v['valid_stock']}}</td>
            <td >{{$v['on_cart']}}</td>
            <td >{{ Prod::gudang('nomor_rak',$v['id'],$param['warehouse'])}}</td>
            <td >{{ Prod::gudang('baris',$v['id'],$param['warehouse'])}}</td>
            <td >{{ Prod::gudang('kolom',$v['id'],$param['warehouse'])}}</td>
        </tr>
        @endforeach
    </table>
    </div>
    <br>
    @endforeach
@else
@foreach($data['product'] as $d)
    <div class="col-lg-6 col-md-12" style="padding:10px">
    <table  border=1>
        <tr >
            <th class="bg">Product Name</th>
            <th colspan=2>{{$d['product_name']}} <a href="{{url('product/detail/'.$d['id'])}}" target="_blank"><br> ( Detail Product )</a></th>
        </tr>
        <tr >
            <th class="bg">Category</th>
            <th colspan=3>{{$d['category']['category_name']}}</th>
        </tr>
        <tr >
            <th class="bg">Warehouse</th>
            <th colspan=2>{{Generate::wh($param['warehouse'])}}</th>
        </tr>
        <tr class="bg">
            <th>Variant</th>
            <th>Qty Stock</th>
            <th>Rak</th>
            <th>Baris</th>
            <th>Kolom</th>
        </tr>
        @foreach($d['variant'] as $v)
        <tr>
            <td>{{$v['variant']}} {{$v['id']}}</td>
            <?php
                if(isset($data['stock'][$d['id']][$v['id']])){
                    $stock = $data['stock'][$d['id']][$v['id']];
                } else {
                    $stock = 0;
                }
            ?>
            <td class="{{$stock<='0'?'red':''}}">
                {{$stock}}
            </td>
            <td >{{ Prod::gudang('nomor_rak',$v['id'],$param['warehouse'])}}</td>
            <td >{{ Prod::gudang('baris',$v['id'],$param['warehouse'])}}</td>
            <td >{{ Prod::gudang('kolom',$v['id'],$param['warehouse'])}}</td>
        </tr>
        @endforeach
    </table>
    </div>
    <br>
    @endforeach
@endif
</div>
