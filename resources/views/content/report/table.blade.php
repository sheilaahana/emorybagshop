<?php use App\Helpers\Generate as Generate; ?>
<table class="table table-hover  table-custom spacing5 mb-0">
    <thead >
        <tr>
            <th>No</th>
            <th>Tanggal Pesan</th>
            <th>Confirmed</th>
            <th>Gudang</th>
            <th>Ekspedisi</th>
            <th>Finish</th>
            <th>Invoice</th>
            <th>Pembeli</th>
            <th>Product </th>
            <th>Variant </th>
            <th>Qty </th>
            <th>Notes</th>
            <th>SubTotal</th>
            <th>Type</th>
            <th>Status</th>
            <th>CS</th>
            <th>QC</th>
            <th>GD</th>
            <th>PK</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1;?>
        @forelse($data as $d)
        <tr>
            <td>{{$no++}}</td>
            <td>{{date('d M Y', strtotime($d['created_at']))}}</td>
            <td>{{$d['confirmed_date'] != '' ? date('d M Y', strtotime($d['confirmed_date'])): "-"}}</td>
            <td>{{$d['change_gudang'] != '' ? date('d M Y', strtotime($d['change_gudang'])): "-"}}</td>
            <td>{{$d['change_shipping'] != '' ? date('d M Y', strtotime($d['change_shipping'])): "-"}}</td>
            <td>{{$d['set_shipping'] != '' ? date('d M Y', strtotime($d['set_shipping'])): "-"}}</td>
            <td>{{$d['invoice_number']}}</td>
            <td>{{ucwords($d['name'])}}</td>
            <td>{{$d['product_name']}}</td>
            <td>{{$d['variant']}}</td>
            <td>{{$d['qty']}} </td>
            <td>{{$d['memo']}} </td>
            <td>{{Generate::money($d['qty']*$d['price'])}}</td>
            <td>{{$d['jenis_pesanan']}} </td>
            <td>{{$d['payment_status']}} </td>
            <td>{{$d['handler']['customer_service']}} </td>
            <td>{{$d['handler']['quality_control']}} </td>
            <td>{{$d['handler']['gudang']}} </td>
            <td>{{$d['handler']['packing']}} </td>
        </tr>
        @empty
        <tr>
            <td colspan="9"><center>Empty Data</center></td>
        </tr>
        @endforelse
    </tbody>
</table>