@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Retur Transaction</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Retur</li>
                </ol>
            </nav>
        </div>  
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <div class="btn-group" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                &emsp;Filter By &emsp;
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a href="{{url('retur')}}" class="dropdown-item" title="">All Data</a>
                    <a href="?filter=PROCESS" class="dropdown-item" title="">Process</a>
                    <a href="?filter=SUCCESS" class="dropdown-item" title="">Success</a>
                    <a href="?filter=DECLINE" class="dropdown-item" title="">Decline</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Invoice</th>
                            <th>Reason</th>
                            <th>New Invoice</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; ?>
                        @foreach($data as $d)
                        <tr>
                            <td><span>{{$no++}}</span></td>
                            <td>{{date('d M Y h:i:s', strtotime($d['created_at']))}}</td>
                            <td><a href="{{url('transaction/detail/'.$d['transaction_id'])}}" target="_blank" title="Detail Transaction"><span>{{$d['invoice_number']}}</span></a></td>
                            <td>{{$d['reason']}}</td>
                            <td><a href="{{url('transaction/detail/'.$d['new_transaction_id'])}}" target="_blank" title="Detail Transaction"><span>{{$d['new_invoice_number']}}</span></a></td>
                            <td>
                                <?php if($d['status']=="PROCESS"){$badge='badge-warning';}elseif($d['status']=='SUCCESS'){$badge='badge-success';}else{$badge='badge-danger';} ?>
                                <span class="badge {{$badge}} ml-0 mr-0">{{$d['status']}}</span></i>
                            </td>
                            
                            <td>
                                <a href="{{url('retur/detail/'.$d['transaction_id'])}}" class="btn btn-info" title="Detail Retur"><i class="fa fa-eye"></i> &emsp;Detail</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection