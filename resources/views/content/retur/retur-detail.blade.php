@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Retur Transaction Detail</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('retur')}}">Retur</a></li>
                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
        </div> 
    </div>
</div>
<div class="card">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                @if (Session::get('message'))
                <div class="alert alert-danger">
                    {{Session::get('message')}}
                </div>
                @endif
            </div>
            <div class="col-md-12" style="text-align:right">
                <a href="{{url('retur/form/'.$data['id'])}}" class="btn btn-sm btn-info">Form Retur Request</a>
                @if($data['retur'])
                <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Change Status
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="{{url('retur/status/process/'.$data['id'])}}">PROCESS</a>
                    <a class="dropdown-item" href="{{url('retur/status/success/'.$data['id'])}}">SUCCESS</a>
                    <a class="dropdown-item" href="{{url('retur/status/decline/'.$data['id'])}}">DECLINE</a>
                    </div>
                </div>
                <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Make New Transaction
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="{{url('retur/new/transaction/'.$data['id'])}}">Generate New Transaction</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#exampleModal">Set New Invoice Number </a>
                    </div>
                </div>
                @endif
                <hr style="border:0.2px solid gray">
            </div>
            @if($data['retur'])
            <div class="col-md-12">
            <h6><u>Retur Detail</u></h6>
                <table>
                    <tr><td><b>Invoice Number</b></td><td>: {{$data['retur']['invoice_number']}}</td></tr>
                    <tr><td style="width:200px"><b>Request Type</b></td><td>: {{$data['retur']['request_type']}}</td></tr>
                    <tr><td><b>Date</b></td><td>: {{date('d M Y - h:i:s' ,strtotime($data['retur']['created_at']))}}</td></tr>
                    <tr><td><b>Status</b></td><td>: {{$data['retur']['status']}}</td></tr>
                    <tr><td><b>Reason</b></td><td>: {{$data['retur']['reason']}}</td></tr>
                </table>
                <br></div>
            <div class="col-md-6">
                <h6><u>Transaction Detail</u></h6>
                <table>
                    <tr><td style="width:200px"><b>Invoice Number</b></td><td>: {{$data['invoice_number']}} <a href="{{url('transaction/detail/'.$data['id'])}}" target="_blank">( Detail )</a> </td></tr>
                    <tr><td><b>Cashless OrderID </b></td><td>: {{$data['cashless_order']}}</td></tr>
                    <tr><td><b>Nama Penerima </b></td><td>: {{$data['nama_penerima']}}</td></tr>
                    <tr><td><b>Telpon Penerima </b></td><td>: {{$data['telpon_penerima']}}</td></tr>
                    <tr><td><b>Email </b></td><td>: {{$data['email']}}</td></tr>
                    <tr><td><b>Alamat </b></td><td>: {{$data['alamat_penerima']}}, {{$data['city']['type']." ".$data['city']['city']}}, {{$data['city']['province']['province']}}, {{$data['kodepos']}}</td></tr>
                    <tr><td><b>Shipping </b></td><td>: {{$data['courier']}} {{$data['shipping_service']}}</td></tr>
                    <tr><td><b>Shipping Status </b></td><td>: {{$data['shipping_status']}}</td></tr>
                    <tr><td><b>Detail</b></td><td>
                        @if($data['detail'])
                            @forelse($data['detail'] as $det)
                                <li><b>{{$det['product']['product_name']}}</b> <i>{{$det['variant']['variant']}}</i> ( {{$det['qty']}} pcs )</li>
                            @empty
                                : <b>Empty Data</b>
                            @endforelse
                        @endif
                    </td></tr>
                </table>
            </div>
            @if($data['retur']['new_invoice_number'])
            <div class="col-md-6">
                <h6><u>New Transaction Detail</u></h6>
                <table>
                    <tr><td><b>New Invoice Number</b></td><td>: {{$data['retur']['new_invoice_number']}} <a href="{{url('transaction/detail/'.$data['retur']['new_transaction_id'])}}" target="_blank">( Detail )</a></td></tr>
                    <tr><td><b>Cashless OrderID </b></td><td>: {{$data['retur']['newtransaction']['cashless_order']}}</td></tr>
                    <tr><td><b>Nama Penerima </b></td><td>: {{$data['retur']['newtransaction']['nama_penerima']}}</td></tr>
                    <tr><td><b>Telpon Penerima </b></td><td>: {{$data['retur']['newtransaction']['telpon_penerima']}}</td></tr>
                    <tr><td><b>Email </b></td><td>: {{$data['retur']['newtransaction']['email']}}</td></tr>
                    <tr><td><b>Alamat </b></td><td>: {{$data['retur']['newtransaction']['alamat_penerima']}}, {{$data['retur']['newtransaction']['city']['type']." ".$data['retur']['newtransaction']['city']['city']}}, {{$data['retur']['newtransaction']['city']['province']['province']}}, {{$data['retur']['newtransaction']['kodepos']}}</td></tr>
                    <tr><td><b>Shipping </b></td><td>: {{$data['retur']['newtransaction']['courier']}} {{$data['retur']['newtransaction']['shipping_service']}}</td></tr>
                    <tr><td><b>Shipping Status </b></td><td>: {{$data['retur']['newtransaction']['shipping_status']}}</td></tr>
                    <tr><td><b>Detail</b></td><td>
                        @if($data['retur']['newtransaction'])
                            @forelse($data['retur']['newtransaction']['detail'] as $det)
                                <li><b>{{$det['product']['product_name']}}</b> <i>{{$det['variant']['variant']}}</i> ( {{$det['qty']}} pcs )</li>
                            @empty
                                : <b>Empty Data</b>
                            @endforelse
                        @endif
                    </td></tr>
                </table>
            </div>
            @endif
            @else
            <div class="col-md-12" style="text-align:center">
                <hr>
                <h5><i>Empty Data Request</i></h5>
                <hr>
                </div>
            @endif
        </div>

    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Retur Transaction</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{url('retur/new/invoice')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input type="hidden" name="transaction_id" value="{{$data['id']}}" />
            <div class="form-group">
                <label>New Invoice Number</label>
                <input type="text" name="invoice_number" id="" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection