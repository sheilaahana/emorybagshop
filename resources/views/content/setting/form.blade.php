@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Application Setting</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">General Setting </li>
                <li class="breadcrumb-item active" aria-current="page">App </li>
                </ol>
            </nav>
        </div>            
    </div>
</div>
<div class="col-md-12">
    @if (Session::get('message'))
    <div class="alert alert-danger">
        {{Session::get('message')}}
    </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<div class="card">
    <div class="body">
        <form action="{{url('/app/save')}}" method="post" class="m-form" novalidate>
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <ul class="list-group">
                        @foreach($data['data'] as $d)
                        <li class="list-group-item">
                            <small class="text-muted">
                                {{ucwords(str_replace('_',' ',$d['key_setting']))}} 
                                @if(!empty($d['note']))
                                ( {{$d['note']}} )
                                @endif
                            </small>
                            <p class="mb-0" style="padding: 10px">
                                @if($d['type']=='number')
                                <input type="number" name="change[{{$d['key_setting']}}]" value="{{ $d['value']}}" min="1" class="form-control" required="">
                                @elseif($d['type']=='email')
                                <input type="email" name="change[{{$d['key_setting']}}]" value="{{ $d['value']}}" class="form-control" required="">
                                @elseif($d['type']=='char')
                                <input type="text" name="change[{{$d['key_setting']}}]" value="{{ $d['value']}}" class="form-control" required="">
                                @elseif($d['type']=='text')
                                <textarea class="form-control" name="change[{{$d['key_setting']}}]" rows="5" required="">{{ $d['value']}}</textarea>
                                @elseif($d['type']=='city')
                                <div class="row clearfix">
                                    <div class="form-group col-md-6">
                                        <label>Province </label>
                                        <select name="change[province]" class="form-control m-input" id="province" required>
                                            <option value=''>- Select Province - </option>
                                            @foreach($data['province'] as $pp)
                                            <option value="{{$pp['id']}}" {{!empty($d['value'])&&Generate::province(Generate::cityorigin())==$pp['id']?'selected':''}}>{{$pp['province']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>City</label>
                                        <select name="change[city_origin]" class="form-control m-input" id="city" required>
                                            <option value=''>- Select Province First - </option>
                                            <option value="{{$d['value']}}" selected>{{Generate::city($d['value'])}}</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                @elseif($d['type']=='kurir')
                                <?php $d['value'] = json_decode($d['value']); ?>
                                    @foreach($data['courier'] as $cr)
                                    <label class="fancy-checkbox">
                                        <input type="checkbox" name="change[{{$d['key_setting']}}][]" value="{{$cr['courier']}}" required="" {{in_array($cr['courier'],$d['value'])?'checked':''}} >
                                        <span><i></i>{{$cr['courier']}}</span>
                                    </label>
                                    @endforeach
                                @elseif($d['type']=='store')
                                    <label class="fancy-checkbox">
                                        <input type="radio" name="change[{{$d['key_setting']}}]" value="open" required="" {{!empty($d['value'])&&$d['value']=='open'?'checked':''}} >
                                        <span><i></i>Open</span>
                                    </label>
                                    <label class="fancy-checkbox">
                                        <input type="radio" name="change[{{$d['key_setting']}}]" value="closed" required="" {{!empty($d['value'])&&$d['value']=='closed'?'checked':''}} >
                                        <span><i></i>Closed</span>
                                    </label>
                                @endif
                            </p>
                        </li>
                        @endforeach
                    </ul>
                <div>
                <center><button type="submit" class="btn btn-primary" style="margin:10px">Save Application Setting</button></center>

            </div>
        </form>
    </div>
</div>
@endsection
