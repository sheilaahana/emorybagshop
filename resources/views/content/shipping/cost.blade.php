@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage;?>
    <div class="block-header" >
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h1>Find Cost Shipping Form</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Find Cost Shipping </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="body">
            <form action="{{url('/shipping/cost')}}" method="post" class="m-form" id="fnotaris" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <div class="row">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group col-md-12">
                                    <label>Courier</label>
                                    <select name="courier" class="form-control m-input" id="courier" required>
                                        @foreach($data['master']['courier'] as $cc)
                                        <option value="{{$cc['courier']}}" {{!empty($data['result'])&&strtoupper($data['result']['query']['courier'])==$cc['courier']?'selected':''}}>{{$cc['courier']}}</option>
                                        @endforeach
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group col-md-12">
                                    <label>Province Origin </label>
                                    <select name="province_origin" class="form-control m-input" id="province" required>
                                        @foreach($data['master']['province'] as $pp)
                                        <option value="{{$pp['id']}}" {{!empty($data['result'])&&$data['result']['origin_details']['province_id']==$pp['id']?'selected':''}}>{{$pp['province']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label>City Origin</label>
                                    <select name="city_origin" class="form-control m-input" id="city" required>
                                        <option value=''>- Select Province First - </option>
                                        @if(isset($data['result']))
                                        <option value="{{$data['result']['origin_details']['city_id']}}" selected>{{$data['result']['origin_details']['city_name']}}</option>
                                        @endif
                                        <?php if (isset($data['data']['city'])) { ?>
                                        <option value="{{$data['data']['city']}}" selected>{{$data['data']['city_name']}}</option>
                                        <?php } ?>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group col-md-12">
                                    <label>Province Destination</label>
                                    <select name="province_destination" class="form-control m-input" id="province2" required>
                                        @foreach($data['master']['province'] as $pp)
                                        <option value="{{$pp['id']}}" {{!empty($data['result'])&&$data['result']['destination_details']['province_id']==$pp['id']?'selected':''}}>{{$pp['province']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label>City Destination</label>
                                    <select name="city_destination" class="form-control m-input" id="city2" required>
                                        <option value=''>- Select Province First - </option>
                                        @if(isset($data['result']))
                                        <option value="{{$data['result']['destination_details']['city_id']}}" selected>{{$data['result']['destination_details']['city_name']}}</option>
                                        @endif
                                        <?php if (isset($data['data']['city'])) { ?>
                                        <option value="{{$data['data']['city']}}" selected>{{$data['data']['city_name']}}</option>
                                        <?php } ?>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>  
                            <div class="col-md-12">
                                <div class="form-group col-md-12">
                                    <label>Weight / gram ( 1kg = 1000 gr)</label>
                                    <input name="weight" min="1" type="number" value="1000" class="form-control">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                    </div>
                    <div class="col-lg-6">
                        @if(isset($data['result']))
                            <table>
                                
                                <tr><td><b>Origin</b></td><td>: {{$data['result']['origin_details']['type']}} {{$data['result']['origin_details']['city_name']}},{{$data['result']['origin_details']['type']}}, {{$data['result']['origin_details']['province']}} </td></tr>
                                <tr><td><b>Destination</b></td><td>: {{$data['result']['destination_details']['type']}} {{$data['result']['destination_details']['city_name']}},{{$data['result']['destination_details']['type']}}, {{$data['result']['origin_details']['province']}} </td></tr>
                                <tr><td><b>Weight</b></td><td>: {{$data['result']['query']['weight']}} Gram</td></tr>
                                <tr><td><b>Courier</b></td><td>: {{$data['result']['query']['courier']}} </td></tr>
                                <tr><td><b>Result</b></td><td>: </td></tr>
                                <tr><td colspan=2>
                                    <ul>
                                        @foreach($data['result']['results'] as $res)
                                        <li><b>{{$res['name']}}</b></li>
                                        <ul>
                                            @forelse($res['costs'] as $cost)
                                            <li>{{$cost['description']}} ( {{$cost['service']}}  ) - {{Generate::money($cost['cost'][0]['value'])}} etd {{$cost['cost'][0]['etd']}} day </li>
                                            @empty
                                            <li>Invalid ( Tidak Ada Service )</li>
                                            @endforelse
                                        </ul>
                                        @endforeach
                                    </ul>
                                </td></tr>
                            </table>
                        @endif
                    </div>
                </div>
                

        </div>
    </div>
@endsection