@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-4 col-sm-12">
            <h1>Shipping Data</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Shipping Data</li>
                </ol>
            </nav>
        </div>  
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                    <form action="" method="get" class="m-form">
                <div class="row">
                    <div class="col-lg-1 col-md-12" ><br><br><b>&emsp;Search</b></div>
                    <div class="form-group col-md-2">
                        <label>Receiver Name</label>
                        <input type="text" name="name" class="form-control" style="background-color:white;" value="{{!empty($_GET['name'])&&$_GET['name']!=''?$_GET['name']:''}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label>Invoice Number</label>
                        <input type="text" name="invoice" class="form-control" style="background-color:white;" value="{{!empty($_GET['invoice'])&&$_GET['invoice']!=''?$_GET['invoice']:''}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label>Confirmation Date</label>
                        <input type="date" name="date" class="form-control" style="background-color:white;"  value="{{!empty($_GET['date'])&&$_GET['date']!=''?$_GET['date']:''}}">
                    </div>
                    <div class="form-group col-md-2">
                        <label>Shipping Status</label>
                        <select name="shipping" class="form-control">
                            <option value="" >All Status</option>
                            <option value="SUDAH KIRIM" {{!empty($_GET['shipping'])&&$_GET['shipping']=='SUDAH KIRIM'?'selected':''}}>SUDAH KIRIM</option>
                            <option value="BELUM KIRIM" {{!empty($_GET['shipping'])&&$_GET['shipping']=='BELUM KIRIM'?'selected':''}}>BELUM KIRIM</option>
                            <option value="TERKIRIM" {{!empty($_GET['shipping'])&&$_GET['shipping']=='TERKIRIM'?'selected':''}}>TERKIRIM</option>
                            <option value="GAGAL KIRIM" {{!empty($_GET['shipping'])&&$_GET['shipping']=='GAGAL KIRIM'?'selected':''}}>GAGAL KIRIM</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Nomor Resi</label>
                        <select name="resi" class="form-control">
                            <option value="">All Data</option>
                            <option value="EMPTY" {{!empty($_GET['resi'])&&$_GET['resi']=='EMPTY'?'selected':''}}>Resi Kosong</option>
                            <option value="ISSET" {{!empty($_GET['resi'])&&$_GET['resi']=='ISSET'?'selected':''}}>Resi Terisi</option>
                        </select>
                    </div>
                    <div class="col-lg-1 col-md-12"  ><br>
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <form action="{{url('/shipping/save')}}" method="post" class="m-form">
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <table class="table table-hover spacing5 mb-0">
                        <thead >
                            <tr>
                                <th>ID</th>
                                <th>Detail</th>
                                <th>Shipping To</th>
                                <th>Payment Detail</th>
                                <th>Status Pengiriman</th>
                                <th>No Resi</th>
                                <th>Pickup Date</th>
                                <th>Pickup Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; ?>
                            @forelse($data as $d)
                            <tr>
                                <td style="vertical-align:top"><span>{{$no++}}</span></td>
                                <td>
                                    <span>{{$d['invoice_number']}}</span><br>
                                    <b>Customer : </b><br>
                                    <a href="{{url('customer/detail/'.$d['users_id'])}}" target="_blank">{{ucwords($d['user']['name'])}}</a><br>
                                    <b>Purcase Date : </b><br>
                                    {{date('d M Y h:i:s', strtotime($d['created_at']))}}<br>
                                    <b>Confirmation Date : </b><br>
                                    @if($d['payment_receipt']!==null && !empty($d['payment_receipt']))
                                    {{date('d F y h:i:s', strtotime($d['confirmation_date']))}}<br>
                                    @else
                                    <i>haven't confirmed yet</i>
                                    @endif
                                </td>
                                <td>
                                    <p class="mb-0">
                                        <b>{{$d['nama_penerima']}}</b><br>
                                        {{$d['email']}} <br>
                                        {{$d['telpon_penerima']}}<br>
                                        {{$d['alamat_penerima']}}<br>
                                        {{$d['city_name']}}<br>
                                        {{$d['kodepos']}}
                                    </p>
                                </td>
                                <td style="vertical-align:top">
                                    <b>Payment Target : </b><br>
                                    {{$d['payment_target']}}<br>
                                    <b>Total : </b><br>
                                    {{Generate::money($d['total'])}}<br>
                                    <b>Ongkos Kirim : </b><br>
                                    {{Generate::money($d['ongkos_kirim'])}}<br>
                                    <?php if($d['payment_status']=="UNPAID"){$badge='badge-warning';}elseif($d['payment_status']=='PAID'){$badge='badge-success';}else{$badge='badge-danger';} ?>
                                    <span class="badge {{$badge}} ml-0 mr-0">{{$d['payment_status']}}</span></i>
                                </td>
                                <td style="vertical-align:top">
                                <select id="single-selection" name="update[{{$d['id']}}][status]" class="form-control">
                                    <?php $status=['BELUM KIRIM','SUDAH KIRIM','TERKIRIM','GAGAL KIRIM']; ?>
                                    @foreach($status as $pc)
                                    <option value="{{$pc}}" {{!empty($d['shipping_status'])&&$d['shipping_status']==$pc?'selected':''}}>{{$pc}}</option>
                                    @endforeach
                                </select>
                                </td>
                                <td style="vertical-align:top">
                                    <input type="text" name="update[{{$d['id']}}][resi]" value="{{ empty($d->shipping_receipt)?old('shipping_receipt'):$d->shipping_receipt}}" class="form-control" >                                
                                </td>
                                <td style="vertical-align:top">
                                    <input type="date" name="update[{{$d['id']}}][shipping_date]" value="{{ empty($d->shipping_receipt)?old('shipping_date'):$d->shipping_date}}" class="form-control" aria-describedby="basic-addon1">
                                </td>
                                <td style="vertical-align:top">
                                    <input type="time" name="update[{{$d['id']}}][shipping_time]" value="{{ empty($d->shipping_time)?old('shipping_time'):$d->shipping_time}}" class="form-control" aria-describedby="basic-addon1">
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="7"><center>No matching records found</center></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    @if(count($data) > 0)
                    <button type="submit" class="btn btn-primary">Save</button>
                    @endif
                </form>
                <div class="row clearfix">
                    <div class="col-sm-12 col-md-12">
                        <div style="float:right; margin:10px 40px"> {{ $data->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection