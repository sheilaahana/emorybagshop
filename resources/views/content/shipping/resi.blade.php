@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage;?>
    <div class="block-header" >
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h1>Find Waybill Shipping </h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Find Shipping </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="body" style="overflow-x:auto">
            <form action="{{url('/shipping/resi')}}" method="post" class="m-form">
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <div class="row">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group col-md-12">
                                    <label>Courier</label>
                                    <select name="courier" class="form-control m-input" id="courier" required>
                                        @foreach($data['master']['courier'] as $cc)
                                        <option value="{{$cc['courier']}}">{{$cc['courier']}}</option>
                                        @endforeach
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group col-md-12">
                                    <label>Resi Pengiriman</label>
                                    <input name="resi"  type="text" class="form-control" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-primary">Find</button>
                    </form>
                    </div>
                    <div class="col-lg-6">
                        @if(isset($data['result']))
                            <table>
                                <tr><td>Shipping Detail</td></tr>
                                <tr><td><b>Resi Pengiriman</b></td><td>: {{$data['result']['result']['summary']['waybill_number']}} </td></tr>
                                <tr><td><b>Courier</b></td><td>: {{$data['result']['result']['summary']['courier_name']}} </td></tr>
                                <tr><td><b>Service</b></td><td>: {{$data['result']['result']['summary']['service_code']}} </td></tr>
                                <tr><td><b>Date</b></td><td>: {{$data['result']['result']['summary']['waybill_date']}} </td></tr>
                                <tr><td><b>Shipper</b></td><td>: {{$data['result']['result']['summary']['shipper_name']}} </td></tr>
                                <tr><td><b>Origin</b></td><td>: {{$data['result']['result']['summary']['origin']}} </td></tr>
                                <tr><td><b>Destination</b></td><td>: {{$data['result']['result']['summary']['destination']}} </td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>Delivery Status</td></tr>
                                <tr><td><b>Status</b></td><td>: {{$data['result']['result']['delivery_status']['status']}} </td></tr>
                                <tr><td><b>Receiver</b></td><td>: {{$data['result']['result']['delivery_status']['pod_receiver']}} </td></tr>
                                <tr><td><b>Receive Date</b></td><td>: {{$data['result']['result']['delivery_status']['pod_date']}} - {{$data['result']['result']['delivery_status']['pod_time']}} </td></tr>
                                <tr><td><b>Manifest</b></td><td>: </td></tr>
                                <tr><td colspan=2>
                                    <ul>
                                        @foreach($data['result']['result']['manifest'] as $mani )
                                        <li>{{$mani['manifest_description']}} - <b>{{$mani['city_name']}}</b><br><i>( {{$mani['manifest_date']}} {{$mani['manifest_time']}} )</i></li>
                                        @endforeach
                                    </ul>
                                </td></tr>
                            </table>
                        @else
                            <table>
                                <tr><td>Shipping Detail</td></tr>
                                <tr><td><b>Resi Pengiriman</b></td><td>: {{$data['error']['resi']}} </td></tr>
                                <tr><td><b>Courier</b></td><td>: {{$data['error']['courier']}} </td></tr>
                                <tr><td>&emsp;</td></tr>
                                <tr><td>Delivery Status</td></tr>
                                <tr><td><b class="danger">Resi Not Found</b></td></tr>
                            </table>
                        @endif
                    </div>
                </div>
                

        </div>
    </div>
@endsection