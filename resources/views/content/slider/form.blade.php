@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Slider Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">General Setting</li>
                <li class="breadcrumb-item active" aria-current="page">Slider</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="card">
    <div class="body">
        <form action="{{url('/slider/save')}}" method="post" class="m-form" id="fnotaris" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input name="id" type="hidden" class="form-control m-input" value="{{ empty($data->id)?old('id'):$data->id}}" placeholder="id">
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Name</label>
                    <input type="text" name="name" value="{{ empty($data->name)?old('name'):$data->name}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-12">
                    <label>Heading</label>
                    <input type="text" name="heading" value="{{ empty($data->heading)?old('heading'):$data->heading}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-12">
                    <label>Description</label>
                    <textarea class="form-control" name="description" required="">{{ empty($data->description)?old('description'):$data->description}}</textarea>
                </div>
                <div class="form-group col-md-12">
                    <label>Image</label>
                    <input type="file" name="image"  class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>CTA 1 Text</label>
                    <input type="text" name="cta_1_text" value="{{ empty($data->cta_1_text)?old('cta_1_text'):$data->cta_1_text}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-4">
                    <label>CTA 1 Link</label>
                    <input type="text" name="cta_1_link" value="{{ empty($data->cta_1_link)?old('cta_1_link'):$data->cta_1_link}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-4">
                    <label>CTA 1 Target</label>
                    <br>
                    <label class="fancy-radio">
                        <input type="radio" name="cta_1_target" value="_blank" required="" data-parsley-errors-container="#error-radio"  {{!empty($data['cta_1_target'])&&$data['cta_1_target']=='_blank'?'checked':''}} >
                        <span><i></i>New Window</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="cta_1_target" value="_self" {{!empty($data['cta_1_target'])&&$data['cta_1_target']=='_self'?'checked':''}} >
                        <span><i></i>Self</span>
                    </label>
                    <p id="error-radio"></p>
                </div>
                <div class="form-group col-md-4">
                    <label>CTA 2 Text</label>
                    <input type="text" name="cta_2_text" value="{{ empty($data->cta_2_text)?old('cta_2_text'):$data->cta_2_text}}" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>CTA 2 Link</label>
                    <input type="text" name="cta_2_link" value="{{ empty($data->cta_2_link)?old('cta_2_link'):$data->cta_2_link}}" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label>CTA 2 Target</label>
                    <br>
                    <label class="fancy-radio">
                        <input type="radio" name="cta_2_target" value="_blank" data-parsley-errors-container="#error-radio"  {{!empty($data['cta_2_target'])&&$data['cta_2_target']=='_blank'?'checked':''}} >
                        <span><i></i>New Window</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="cta_2_target" value="_self" {{!empty($data['cta_2_target'])&&$data['cta_2_target']=='_self'?'checked':''}} >
                        <span><i></i>Self</span>
                    </label>
                    <p id="error-radio"></p>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
@endsection