@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Slider</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">General Setting</li>
                <li class="breadcrumb-item active" aria-current="page">Slider</li>
                </ol>
            </nav>
        </div>            
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <a href="{{url('slider/form')}}" class="btn btn-sm btn-primary" title="">Add Slide</a>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Slider</th>
                            <th>CTA</th>
                            <th>CTA 2</th>
                            <th>Image</th>
                            <th>Publish</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $d)
                        <tr>
                            <td><span>{{$d['id']}}</span></td>
                            <td>
                                <b><span>{{$d['name']}}</span></b><br>
                                {{date('d M Y h:i:s', strtotime($d['created_at']))}}<br>
                                <b>{{$d['heading']}}</b><br>
                                {{$d['description']}}
                            </td>
                            <td>
                                <u><b>Text :</b></u> <br>{{$d['cta_1_text']?:'undefinded'}}
                                <br><u><b>Link :</b></u> <br>{{$d['cta_1_link']?:'undefinded'}}
                                <br><u><b>Target :</b></u> <br>{{$d['cta_1_target']?:'undefinded'}}
                            </td>
                            <td>
                                <u><b>Text :</b></u> <br>{{$d['cta_2_text']?:'undefinded'}}
                                <br><u><b>Link :</b></u> <br>{{$d['cta_2_link']?:'undefinded'}}
                                <br><u><b>Target :</b></u> <br>{{$d['cta_2_target']?:'undefinded'}}
                            </td>
                            <td>
                                <img src="{{$d['image']}}" alt="" style="width:100%;max-width:100px">                                
                            </td>
                            <td>
                                <?php if($d['is_show']=="0"){$badge='badge-danger';}elseif($d['is_show']=='1'){$badge='badge-success';}else{$badge='badge-primary';} ?>
                                <a href="{{url('slider/publish/'.$d['id'])}}" onclick="return confirm('Are you sure you want to Change this?');" class="badge {{$badge}} ml-0 mr-0">{{$d['is_show']=='1'?'Show':'Hide'}}</a></i>
                            </td>
                            <td>
                                <a href="{{url('slider/form?id='.$d['id'])}}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="{{url('slider/delete/'.$d['id'])}}" onclick="return confirm('Are you sure you want to delete this item')" class="btn btn-sm btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection