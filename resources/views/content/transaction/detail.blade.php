@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Transaction Detail</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('transaction')}}">Transaction</a></li>
                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
        </div>   
        <div class="col-md-6 col-sm-12" style="text-align:right">
            <a href="{{url('transaction/update?invoice='.$data['invoice_number'])}}" class="btn btn-sm btn-secondary">Update Data</a>
             <div class="btn-group" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Change Payment Status
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a class="dropdown-item" href="{{url('transaction/payment/unpaid/'.$data['id'])}}">UnPaid</a>
                <a class="dropdown-item" href="{{url('transaction/payment/paid/'.$data['id'])}}">Paid</a>
                <a class="dropdown-item" href="{{url('transaction/payment/rejected/'.$data['id'])}}">Rejected</a>
                <a class="dropdown-item" href="{{url('transaction/payment/paylater/'.$data['id'])}}">Pay Later</a>
                </div>
            </div>
            <div class="btn-group" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Change Shipping Status
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a class="dropdown-item" href="{{url('transaction/shipping/sudah/'.$data['id'])}}">Sudah Kirim</a>
                <a class="dropdown-item" href="{{url('transaction/shipping/belum/'.$data['id'])}}">Belum Kirim</a>
                <a class="dropdown-item" href="{{url('transaction/shipping/gagal/'.$data['id'])}}">Gagal Kirim</a>
                <a class="dropdown-item" href="{{url('transaction/shipping/terkirim/'.$data['id'])}}">Terkirim</a>
                </div>
             </div>
            <a href="{{url('transaction/retur/detail/'.$data['id'])}}" class="btn btn-sm btn-secondary">Retur Request</a>
        </div>
    </div>
</div>
@if(!empty($data))
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Status Transaction <a href="#" data-toggle="modal" data-target="#logstatus">( Detail )</a></small>
                    <div class="row" style="padding: 10px 0px">
                        <?php $status = ['PENDING','CONFIRMATION','PRINT','GUDANG','EKSPEDISI','SELESAI','GAGAL']; ?>
                        @foreach($status as $s)
                            <?php if($data['status_trx'] == $s){ $btn = "btn-success text-white"; }else{ $btn= "btn-default"; } ?>
                            <a class="btn btn-sm {{$btn}}" data-toggle="modal" data-target="#changestatus{{$s}}" style="min-width:180px;margin:5px"> {{$s}} </a>
                            <div class="modal fade" id="changestatus{{$s}}" tabindex="-1" role="dialog" aria-labelledby="changestatus" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="changestatus">Change Status Transaction</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{url('transaction/change/status')}}" method="post">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                            <input type="hidden" name="transaction_id" value="{{$data['id']}}" />
                                            <div class="form-group">
                                                <label>Status</label>
                                                <input type="text" name="status" class="form-control" value="{{$s}}" readonly required>
                                            </div>
                                            <div class="form-group">
                                                <label>Description</label>
                                                <textarea name="description" class="form-control" rows="5" required></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </form>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Invoice Number </small>
                    <p class="mb-0">{{ucwords($data['invoice_number'])}}</p>
                </li>
                
                <li class="list-group-item">
                    <small class="text-muted">Customer </small>
                    <p class="mb-0"><b>{{ucwords($data['user']['name'])}}</b> <a href="{{url('customer/detail/'.$data['user']['id'])}}" target="_blank">( Detail )</a></p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Penerima </small>
                    <p class="mb-0">
                        <b>{{$data['nama_penerima']}}</b><br>
                        {{$data['email']}} <br>
                        {{$data['telpon_penerima']}}<br>
                        {{$data['alamat_penerima']}}<br>
                        {{$data['city_name']}}<br>
                        {{$data['kodepos']}}
                    </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Payment Target </small>
                    <p class="mb-0">{{$data['payment_target']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Confirmation Date</small>
                    <p class="mb-0">
                        @if($data['payment_receipt']!==null && !empty($data['payment_receipt']))
                        {{date('d F y h:i:s', strtotime($data['confirmation_date']))}}<br>
                        <a href="{{url(Storage::url($data['payment_receipt']))}}" target="_blank"> View Receipt Image</a>
                        @else
                        <i>haven't confirmed yet</i>
                        @endif
                    </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Confirmed Date </small>
                    <p class="mb-0">{{$data['confirmed_date'] != '' ? date('d F Y - h:i', strtotime($data['confirmed_date'])) : "-"}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Kirim Gudang </small>
                    <p class="mb-0">{{$data['change_gudang'] != '' ? date('d F Y - h:i', strtotime($data['change_gudang'])) : "-"}}</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Date </small>
                    <p class="mb-0">{{date('d F Y - h:i', strtotime($data['created_at']))}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Payment Status </small>
                    <p class="mb-0">{{$data['payment_status']}}</p>
                    
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Shipping Status </small>
                    <p class="mb-0">{{$data['shipping_status']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Shipping</small>
                    <p class="mb-0">{{$data['courier']}} / {{$data['shipping_service']}} </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Shipping Receipt</small>
                    <p class="mb-0">{{$data['shipping_receipt']?:"-"}}
                        @if($data['shipping_receipt']!='')
                        <a href="#" data-toggle="modal" data-target="#exampleModal"> See Tracking</a>
                        @endif
                    </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Shipping Date / Time</small>
                    <p class="mb-0">{{date('d F Y', strtotime($data['shipping_date']))?:"-"}} / {{$data['shipping_time']?:"-"}} </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Kirim Ekspedisi</small>
                    <p class="mb-0">{{$data['change_shipping'] != '' ? date('d F Y - h:i', strtotime($data['change_shipping'])) : "-"}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Set Shipping / Finish</small>
                    <p class="mb-0">{{$data['confirmed_date'] != '' ? date('d F Y - h:i', strtotime($data['confirmed_date'])) : "-"}}</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Status Transaksi</small>
                    <p class="mb-0">{{$data['status_trx']?:"-"}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Nota ( Download / Print )</small>
                    <p class="mb-0"><a target="_blank" href="{{url('transaction/nota/bill/'.$data['id'])}}">Generate Nota Bill</a></p>
                    <p class="mb-0"><a target="_blank" href="{{url('transaction/nota/premium/'.$data['id'])}}">Generate Nota Premium</a></p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Handler </small>
                    <p class="mb-0">
                    CS : {{$data['customer_service']}}<br>
                    GD : {{$data['gudang']}}<br>
                    QC : {{$data['quality_control']}}<br>
                    PC : {{$data['packing']}}<br>
                    </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Warehouse</small>
                    <p class="mb-0">{{$data['warehouse']['warehouse']?:"-"}} ( {{$data['warehouse']['code']?:"-"}} )</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Cashless Order ID</small>
                    <p class="mb-0">{{$data['cashless_order']?:"-"}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Notes Transaction</small>
                    <p class="mb-0">{{$data['notes']?:"-"}}</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card" style="height:400px;overflow:auto;background-color:white;padding:20px">
            <b>Log Change</b>
            <ul>
                @forelse($data['log'] as $log)
                    <li><b>{{$log['change_by']}} {{$log['admin']}}</b> <br>{{$log['description']}}<br> <i>{{date('d/m/y h:i:s',strtotime($log['created_at']))}}</i></li>
                @empty
                    <li>Empty Data</li>
                @endforelse
            </ul>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <h5>List Item Transaction</h5><br>
                <table class="table table-hover spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Product / Variant</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($data['detail'])>0)
                        <?php $total=0; ?>
                        @foreach($data['detail'] as $d)
                        <?php $subtotal = $d['price']*$d['qty']; $total = $total + $subtotal; ?>
                        <tr>
                            <td><span>{{$d['id']}}</span></td>
                            <td>
                                <b>{{$d['product']['product_name']}}</b><br>
                                {{$d['variant']['variant']}}<br>
                                <b>Memo : </b><br>
                                {{$d['memo']}}
                            </td>
                            <td>{{Generate::money($d['price'])}}</td>
                            <td><span>{{$d['qty']}}</span></td>
                            <td>{{Generate::money($subtotal)}}</td>
                        </tr>
                        @endforeach
                        <tr>
                            <?php $total = $total+$data['ongkos_kirim'];?>
                            <td colspan="3"></td>
                            <td><b>Shipping Price</b></td>
                            <td><b>{{Generate::money($data['ongkos_kirim'])}}</b></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td><b>Total Price</b></td>
                            <td><b>{{Generate::money($total)}}</b></td>
                        </tr>
                    @else
                        <tr>
                            <td colspan="5"><b><i><center>Invalid Data Transaction</center></i></b></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Shipping Tracking </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      @if(!empty($data['tracking']))
            <table>
                <tr><td>Shipping Detail</td></tr>
                <tr><td><b>Resi Pengiriman</b></td><td>: {{$data['tracking']['result']['summary']['waybill_number']}} </td></tr>
                <tr><td><b>Courier</b></td><td>: {{$data['tracking']['result']['summary']['courier_name']}} </td></tr>
                <tr><td><b>Service</b></td><td>: {{$data['tracking']['result']['summary']['service_code']}} </td></tr>
                <tr><td><b>Date</b></td><td>: {{$data['tracking']['result']['summary']['waybill_date']}} </td></tr>
                <tr><td><b>Shipper</b></td><td>: {{$data['tracking']['result']['summary']['shipper_name']}} </td></tr>
                <tr><td><b>Origin</b></td><td>: {{$data['tracking']['result']['summary']['origin']}} </td></tr>
                <tr><td><b>Destination</b></td><td>: {{$data['tracking']['result']['summary']['destination']}} </td></tr>
                <tr><td>&emsp;</td></tr>
                <tr><td>Delivery Status</td></tr>
                <tr><td><b>Status</b></td><td>: {{$data['tracking']['result']['delivery_status']['status']}} </td></tr>
                <tr><td><b>Receiver</b></td><td>: {{$data['tracking']['result']['delivery_status']['pod_receiver']}} </td></tr>
                <tr><td><b>Receive Date</b></td><td>: {{$data['tracking']['result']['delivery_status']['pod_date']}} - {{$data['tracking']['result']['delivery_status']['pod_time']}} </td></tr>
                <tr><td><b>Manifest</b></td><td>: </td></tr>
                <tr><td colspan=2>
                    <ul>
                        @foreach($data['tracking']['result']['manifest'] as $mani )
                        <li>{{$mani['manifest_description']}} - <b>{{$mani['city_name']}}</b><br><i>( {{$mani['manifest_date']}} {{$mani['manifest_time']}} )</i></li>
                        @endforeach
                    </ul>
                </td></tr>
            </table>
        @else
            <table>
                <tr><td>Shipping Detail</td></tr>
                <tr><td><b class="danger">Resi Not Found</b></td></tr>
            </table>
        @endif
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="logstatus" tabindex="-1" role="dialog" aria-labelledby="logstatus" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="logstatus">Log Status </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="max-height:500px;overflow-y:auto">
        <table class="tableisitable" style="width:100%">
            <tr style="background-color:black;color:white">
                <td>Status</td><td>Date</td><td> Change by</td>
            </tr>
        @forelse($data['detail_status'] as $ds)
            <tr style="background-color:lightgray">
                <td>{{$ds['status']}}</td><td>{{date('d M Y h:i:s', strtotime($ds['created_at']))}}</td><td>{{$ds['change_by']}} {{$ds['admin']}}</td>
            </tr>
            <tr><td colspan=3 style="padding:10px">{{$ds['description']}}</td></tr>
        @empty
            <tr><td>Empty Data</td></tr>
        @endforelse
        </table>
      </div>
    </div>
  </div>
</div>

@else
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <center>
                <h6>Invalid Data / Data not Found</h6>
                <hr>
                <a href="{{url('transaction')}}"> Back to List Transaction </a>
                </center>
            </div>
        </div>
    </div>
</div>

@endif
@endsection
