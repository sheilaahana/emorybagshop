@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage;?>
<div id="vapp">
    <div class="block-header" >
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h1>Rekap Transaction Form</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Rekap Transaction </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="body">
            <form action="{{url('/transaction/save')}}" method="post" class="m-form" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <input name="id" type="hidden" class="form-control m-input" value="{{ empty($data->id)?old('id'):$data->id}}" placeholder="id">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Customer Email </label>
                        <!-- <div class="multiselect_div">
                            <select id="customer_email" v-model="form.users_id" @change="onChangeUser($event)" class="multiselect multiselect-custom form-control" required>
                                <option v-for="u in user" :value=u.id >@{{ u.name }} - @{{ u.email }}</option>
                            </select>
                        </div> -->
                        <v-select
                            :options="{{ $data['user'] }}"
                            label="email"
                            :reduce="email => email.id" 
                            v-model=form.users_id
                            @input="onChangeUser($event)"
                            required
                        >
                        </v-select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Address</label>
                        <div class="multiselect_div">
                            <select id="address" v-model="form.address_id" @change="onChangeAddress($event)" class="multiselect multiselect-custom form-control" >
                                <option v-for="a in user_address" :value=a.id v-if="user_address !== null">@{{ a.id }} - @{{ a.name }} - @{{ a.address }} - @{{ a.phone }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Nama Penerima</label>
                        <input type="text" v-model="form.nama_penerima" class="form-control" required="">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Telpon Penerima</label>
                        <input type="text" v-model="form.telpon_penerima" class="form-control" required="">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Alamat Pengiriman</label>
                        <textarea class="form-control" v-model="form.alamat_penerima" required=""></textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Nama Pengirim ( Default : {{ Generate::setting('nama_pengirim') }} ) </label>
                        <input type="text" v-model="form.nama_pengirim" class="form-control" >
                    </div>
                    <div class="form-group col-md-6">
                        <label>Kontak Pengirim ( Default : {{ Generate::setting('kontak_pengirim') }} )</label>
                        <input type="text" v-model="form.kontak_pengirim" class="form-control" >
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="form-group col-md-6">
                        <label>Jenis Pesanan </label>
                        <div class="multiselect_div">
                            <select id="jenis_pesanan" v-model=form.jenis_pesanan class="form-control" >
                                <option value="REKAP">REKAP</option>
                                <option value="DROPSHIP">DROPSHIP</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Shipping Status </label>
                        <div class="multiselect_div">
                            <select id="shipping_status" v-model=form.shipping_status class="form-control" >
                                <option value="BELUM KIRIM">BELUM KIRIM</option>
                                <option value="SUDAH KIRIM">SUDAH KIRIM</option>
                                <option value="TERKIRIM">TERKIRIM</option>
                                <option value="GAGAL KIRIM">GAGAL KIRIM</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Courier </label>
                        <div class="multiselect_div">
                            <select id="courier" v-model=form.courier class="form-control" >
                                @foreach($data['courier'] as $cr)
                                <option value="{{$cr['courier']}}">{{$cr['courier']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Ongkos Kirim</label>
                        <div class="multiselect_div">
                            <input type="number" v-model.number="form.ongkos_kirim" min="0" class="form-control" required="">
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Payment Status </label>
                        <div class="multiselect_div">
                            <select id="payment_status" v-model=form.payment_status class="form-control" >
                                <option value="UNPAID">UNPAID</option>
                                <option value="PAID">PAID</option>
                                <option value="REJECTED">REJECTED</option>
                                <option value="EXPIRED">EXPIRED</option>
                                <option value="PAYLATER">PAYLATER</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Notes Transaction</label>
                        <textarea class="form-control" v-model="form.notes" required=""></textarea>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="form-group col-md-4">
                        <label>Tambah Cart ( Qty ) </label>
                        <div class="multiselect_div">
                            <input type="number" v-model.number="qty_p" min="1" class="form-control" required="">
                        </div>
                    </div>
                    
                    <div class="col-md-12 clearfix" id="form-product">
                        <div class="row" v-for="i in qty_p" :key="i">
                            <div class="form-group col-md-3">
                                <label>Product @{{ i }} </label>
                                <div class="multiselect_div">
                                    <!-- <select id="product_id" v-model=form.product[i] @change="onChangeProduct($event)" class=" form-control" required>
                                        <option value="">-- Select Product --</option>
                                        <option v-for="p in product" :value=p.code >@{{ p.label }}</option>
                                    </select> -->
                                    <v-select
                                        :options='product'
                                        label="label"
                                        :reduce="label => label.code" 
                                        v-model=form.product[i]
                                        @input="onChangeProduct($event)"
                                        required
                                    >
                                    </v-select>
                                    <!-- [{"code":2,"label":"EMORY Galleani 01EMO 2266-150000"},{"code":4,"label":"EMORY Merlion 01EMO 2305-150000"},{"code":44,"label":"ARRAYA Flyknit AQYAR 553-110000"}] -->
                                   
                                </div>

                            </div>
                            <div class="form-group col-md-3">
                                <label>Variant @{{form.product[i]}}</label>
                                <div class="multiselect_div">
                                    <select id="a" v-model=form.variant[i] class="form-control" required>
                                        <option value="">-- Select Variant --</option>
                                        <option v-for="v in variant[form.product[i]]" :value="(v.qty_stock < 1) ? '' : v.id" > @{{ v.variant }} ( @{{ v.qty_stock }} pcs )  </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Qty</label>
                                <input type="number" v-model=form.qty[i] min="1" class="form-control" required="">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Catatan</label>
                                <input type="text" v-model=form.catatan[i] min="1" class="form-control" required="">
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <button type="submit" @click.prevent="saveData()" class="btn btn-primary">Save</button>
            </form>

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
Vue.component('v-select', VueSelect.VueSelect);

const vueApp = new Vue({
    el: '#vapp',
    data: { 
        product: [],
        variant: {},
        user : {},
        user_address : {},
        qty_p : 1,
        form: {
            users_id : "",
            nama_penerima : "",
            telpon_penerima : "",
            alamat_penerima : "",
            nama_pengirim : "",
            kontak_pengirim : "",
            notes : "",
            address_id : "",
            city_destination : "",
            courier : "",
            shipping_service : "REGULER",
            ongkos_kirim : 0,
            product : {},
            variant : {},
            qty : {},
            catatan : {},
            coba : {}
        },
    },
    mounted() {
        axios.get('/data/product/2')
        .then(response => (this.product = response.data));
        axios.get('/data/user')
        .then(response => (this.user = response.data));
    },
    methods : {
        getvariant(id){
            // // console.log("coba");
            axios.get('/data/variant/'.id)
            .then(response => (this.variant[id] = response.data))
        },
        getaddress(id){
            // // console.log("coba");
            axios.get('/data/address/'.id)
            .then(response => (this.variant[id] = response.data))
        },
        
        onChangeProduct(event){
            id = event;
            console.log('product'+id);
            var _vm = this;
            axios.get('/data/variant/'+id+'/'+this.form.users_id)
            .then(function (response) {
                // handle success
                _vm.variant[id] = response.data; // asalnya gini , tapi katanya undefinded
                console.log(_vm.variant[id]); // pas ini d console munculnya undefinded
            });
            console.log('/data/variant/'+id+'/'+this.form.users_id);
        },
        onChangeUser(event){
            console.log(event);
            id = event;

            axios.get('/data/user/address/'+id)
            .then(response => (this.user_address = response.data));

            axios.get('/data/product/'+id)
            .then(response => (this.product = response.data));
        },
        onChangeAddress(event){
            id = event.target.value;
            axios.get('/data/address/'+id)
            .then(response => {
                this.form.nama_penerima = response.data.name;
                this.form.telpon_penerima = response.data.phone;
                this.form.alamat_penerima = response.data.address;
                this.form.city_destination = response.data.city;
            });
            
        },
        saveData(){
            axios.post('/transaction/save',{
                users_id : this.form.users_id,
                nama_penerima : this.form.nama_penerima,
                telpon_penerima : this.form.telpon_penerima,
                alamat_penerima : this.form.alamat_penerima,
                shipping_service : this.form.shipping_service,
                courier : this.form.courier,
                shipping_status : this.form.shipping_status,
                jenis_pesanan : this.form.jenis_pesanan,
                payment_status : this.form.payment_status,
                ongkos_kirim : this.form.ongkos_kirim,
                product : this.form.product,
                variant : this.form.variant,
                catatan : this.form.catatan,
                city_destination : this.form.city_destination,
                qty : this.form.qty,
                total_product : this.qty_p,
            })
            .then ( function (response){
                // window.location.href = "/transaction";
                console.log(response.data.meta);
                if(response.data.meta.code == 200){
                    window.location.href = "/transaction";
                }
            })
            .catch ( function (error){
                console.log(error);
                console.log("sheila");
                // console.log(error); 
            });
        }
    },
})

</script>
@endpush
