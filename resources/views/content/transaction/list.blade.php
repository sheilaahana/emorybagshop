@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-4 col-sm-12">
            <h1>Transaction</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Transaction</li>
                </ol>
            </nav>
            <a href="{{url('transaction/form')}}" class="btn btn-md btn-success" title="" style="margin:15px 0px">Add Rekap Transaction</a>

        </div>            
        <div class="col-md-8 col-sm-12 text-right hidden-xs">
            <form action="">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label>Date</label>
                        <input type="date" name="date" class="form-control" >
                    </div>
                    <div class="form-group col-md-4">
                        <?php $status = ['unpaid','paid','paylater','rejected']; ?>
                        <label>Payment Status</label>
                        <div class="multiselect_div">
                            <select name="filter" id="" class="form-control">
                                <option value="">All Status</option>
                                @foreach($status as $s)
                                <option value="{{$s}}">{{$s}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <?php $status = ['PENDING','CONFIRMATION','PRINT','GUDANG','EKSPEDISI','SELESAI','GAGAL','ARCHIVED']; ?>
                        <label>Transaction Status</label>
                        <div class="multiselect_div">
                            <select name="status" id="" class="form-control">
                                <option value="">All Status</option>
                                @foreach($status as $s)
                                <option value="{{$s}}">{{$s}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <button type="submit"  class="btn btn-lg btn-primary">Search</button>
            </form>
           
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Invoice</th>
                            <th>Receiver </th>
                            <th>Payment </th>
                            <th>Shipping Status</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1;?>
                        @foreach($data as $d)
                        <tr>
                            <td><span>{{$no++}}</span></td>
                            <td>
                                <b>{{$d['invoice_number']}}</b><br>
                                <i>Transaction Date :</i><br>
                                {{ date('d M Y h:i', strtotime($d['created_at'])) }} <br>
                                @if($d['confirmed_date'] != '')
                                <i>Confirmed Date :</i><br>
                                {{ date('d M Y', strtotime($d['confirmed_date'])) }} <br>
                                @endif
                                @if($d['change_gudang'] != '')
                                <i>Masuk Gudang:</i><br>
                                {{ date('d M Y', strtotime($d['change_gudang'])) }} <br>
                                @endif
                                @if($d['change_shipping'] != '')
                                <i>Masuk Ekspedisi:</i><br>
                                {{ date('d M Y', strtotime($d['change_shipping'])) }} <br>
                                @endif
                                @if($d['set_shipping'] != '')
                                <i>Isi Resi:</i><br>
                                {{ date('d M Y', strtotime($d['set_shipping'])) }} <br>
                                @endif
                            </td>
                            <td>
                                <b>{{$d['nama_penerima']}}</b><br>{{$d['telpon_penerima']}}<br>{{$d['city_name']}}<br>
                                {{$d['jenis_pesanan']}}
                            </td>
                            <td>
                                <?php if($d['payment_status']=="UNPAID"){$badge='badge-danger';}elseif($d['payment_status']=='PAID'){$badge='badge-success';}elseif($d['payment_status']=='PAYLATER'){$badge='badge-warning';}else{$badge='badge-primary';} ?>
                                <span class="badge {{$badge}} ml-0 mr-0" style="margin :5px">{{$d['payment_status']}}</span></i><br>
                                {{Generate::money($d['total'])}}
                                @if($d['payment_receipt']!==null && !empty($d['payment_receipt']))
                                <br><span>Confirmation Available</span>
                                @else
                                <br><span><i>haven't confirmed yet</i></span>
                                @endif
                            </td>
                            <td>
                            <b>{{$d['shipping_status']}}</b><br>
                            {{Generate::wh($d['warehouse_id'])}}
                            </td>
                            <td> <span class="badge badge-default ml-0 mr-0" style="margin :5px">{{$d['status_trx']}}</span></td>
                            
                            <td>
                                <a href="{{url('transaction/detail/'.$d['id'])}}" class="btn btn-sm btn-success" title="Detail"><i class="fa fa-eye"></i></a>
                                <a href="{{url('transaction/delete/'.$d['id'])}}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete this item')" title="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!-- -->
            </div>
        </div>
    </div>
</div>
@endsection