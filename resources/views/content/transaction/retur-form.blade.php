@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Retur Transaction Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('transaction')}}">Transaction</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('transaction/detail/'.$data['id'])}}">Detail</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('transaction/retur/detail/'.$data['id'])}}">Retur</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form</li>
                </ol>
            </nav>
        </div> 
    </div>
</div>
<div class="card">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <table>
                    <tr><td style="width:200px"><b>Invoice Number</b></td><td>: {{$data['invoice_number']}}</td></tr>
                    <tr><td><b>Cashless OrderID </b></td><td>: {{$data['cashless_order']}}</td></tr>
                    <tr><td><b>Nama Penerima </b></td><td>: {{$data['nama_penerima']}}</td></tr>
                    <tr><td><b>Telpon Penerima </b></td><td>: {{$data['telpon_penerima']}}</td></tr>
                    <tr><td><b>Email </b></td><td>: {{$data['email']}}</td></tr>
                    <tr><td><b>Alamat </b></td><td>: {{$data['alamat_penerima']}}, {{$data['city']['type']." ".$data['city']['city']}}, {{$data['city']['province']['province']}}, {{$data['kodepos']}}</td></tr>
                    <tr><td><b>Shipping </b></td><td>: {{$data['courier']}} {{$data['shipping_service']}}</td></tr>
                    <tr><td><b>Shipping Status </b></td><td>: {{$data['shipping_status']}}</td></tr>
                </table>
                <hr>
            </div>
        </div>
        <form action="{{url('/transaction/retur/save')}}" method="post" class="m-form" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <input name="transaction_id" type="hidden" class="form-control m-input" value="{{ empty($data->id)?old('id'):$data->id}}" placeholder="id">
                    <div class="form-group">
                        <label>Reason Retur / Refund *</label>
                        <textarea class="form-control" name="reason" rows="5" required="">{{ empty($data['retur']['reason'])?old('description'):$data['retur']['reason']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Request Type*</label>
                        <select name="request_type" class="form-control">
                            <option value="RETUR" {{isset($data['retur']['request_type'])&&$data['retur']['request_type']=='RETUR'?'selected':''}}>RETUR</option>
                            <option value="REFUND" {{isset($data['retur']['request_type'])&&$data['retur']['request_type']=='REFUND'?'selected':''}}>REFUND</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Image 1</label>
                        <input type="file" name="image_1" value="{{ empty($data->image_1)?old('image_1'):$data->image_1}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Image 2</label>
                        <input type="file" name="image_2" value="{{ empty($data->image_2)?old('image_2'):$data->image_2}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Image 3</label>
                        <input type="file" name="image_3" value="{{ empty($data->image_3)?old('image_3'):$data->image_3}}" class="form-control">
                    </div>
                </div>
            </div>
            
            
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
@endsection