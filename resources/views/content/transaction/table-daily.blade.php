<?php use App\Helpers\Generate as Generate; $no=1;  ?>
<table class="table table-hover  table-custom spacing5 mb-0">
    <thead >
        <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>Warehouse</th>
            <th>OrderID</th>
            <th>qty </th>
            <th>Notes</th>
            <th>Ekspedisi </th>
            <th>CS </th>
            <th>Gudang</th>
            <th>QC</th>
            <th>Packing</th>
        </tr>
    </thead>
    <tbody>
        @forelse($data as $d)
        <tr style="font-weight:800">
            <td>{{$no++}}</td>
            <td>{{$d['created_at']}}</td>
            <td>{{$d['warehouse']['code']}}</td>
            <td>{{$d['invoice_number']}}</td>
            <td></td>
            <td></td>
            <td>{{$d['courier']}} {{$d['shipping_service']}}</td>
            <td>{{$d['customer_service']}}</td>
            <td>{{$d['gudang']}}</td>
            <td>{{$d['quality_control']}}</td>
            <td>{{$d['packing']}}</td>
            <td></td>
        </tr>
            @foreach($d['detail'] as $de)
                @for($i = 0; $i < $de['qty']; $i++)
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$de['product']['product_name']}} - {{$de['variant']['variant']}}</td>
                <td>1</td>
                <td>{{$de['memo']}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
                @endfor
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><b>Total</b></td>
                <td>{{$de['qty']}}</td>
            </tr>
            @endforeach
        @empty
            <tr><td colspan="11"><center>Empty Data</center></td></tr>
        @endforelse
    </tbody>
</table>