@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>

<style>
    .input-group{
        margin: 15px 0px;
    }
    .input-group-text{
        width:200px;
    }
</style>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Update Transaction</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Update Transaction</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {!!Session::get('message')!!}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <form action="" method="get" class="m-form">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Find Data Transaction</h4>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Invoice Number</b></span>
                                </div>
                                <input type="text" name="invoice" value="{{ empty($param['invoice'])?old('invoice'):$param['invoice']}}" class="form-control" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">Search</button>
                            <a href="{{url('report')}}" class="btn btn-danger">Reset</a>
                        </div>
                    </div>
                </form>
                @if(!empty($data))
                <form action="{{url('transaction/update/save')}}" method="post" class="m-form">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <hr><h5>Data Transaction - {{$data['invoice_number']}}</h5>
                            <hr>
                            <table>
                                <tr>
                                    <td><b>Invoice Number&emsp;&emsp;&emsp;&emsp;</b></td><td>:  <a href="{{url('transaction/detail/'.$data['id'])}}" >{{$data['invoice_number']}} </a></td></tr>
                                    <td><b>Cashless OrderID </b></td><td>: {{$data['cashless_order']}}</td></tr>
                                    <td><b>Nama Penerima </b></td><td>: {{$data['nama_penerima']}}</td></tr>
                                    <td><b>Telpon Penerima </b></td><td>: {{$data['telpon_penerima']}}</td></tr>
                                    <td><b>Email </b></td><td>: {{$data['email']}}</td></tr>
                                    <td><b>Alamat </b></td><td>: {{$data['alamat_penerima']}}, {{$data['city']['type']." ".$data['city']['city']}}, {{$data['city']['province']['province']}}, {{$data['kodepos']}}</td></tr>
                                    <td><b>Shipping </b></td><td>: {{$data['courier']}} {{$data['shipping_service']}}</td></tr>
                                    <td><b>Shipping Status </b></td><td>: {{$data['shipping_status']}}</td></tr>
                                    <td><b>Ongkos Kirim </b></td><td>: {{Generate::money($data['ongkos_kirim'])}}</td></tr>
                            </table>
                            <hr>
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <input type="hidden" name="id" value="{{$data['id']}}" />
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Customer Service</b></span>
                                </div>
                                <input type="text" name="customer_service" value="{{ empty($data['customer_service'])?old('customer_service'):$data['customer_service']}}" class="form-control" aria-describedby="basic-addon1">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Quality Control</b></span>
                                </div>
                                <input type="text" name="quality_control" value="{{ empty($data['quality_control'])?old('quality_control'):$data['quality_control']}}" class="form-control" aria-describedby="basic-addon1">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Packing</b></span>
                                </div>
                                <input type="text" name="packing" value="{{ empty($data['packing'])?old('packing'):$data['packing']}}" class="form-control" aria-describedby="basic-addon1">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Gudang</b></span>
                                </div>
                                <input type="text" name="gudang" value="{{ empty($data['gudang'])?old('gudang'):$data['gudang']}}" class="form-control" aria-describedby="basic-addon1">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Tanggal Pickup</b></span>
                                </div>
                                <input type="date" name="shipping_date" value="{{ empty($data['shipping_date'])?old('shipping_date'):$data['shipping_date']}}" class="form-control" aria-describedby="basic-addon1">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Jam Pickup</b></span>
                                </div>
                                <input type="time" name="shipping_time" value="{{ empty($data['shipping_time'])?old('shipping_time'):$data['shipping_time']}}" class="form-control" aria-describedby="basic-addon1">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Shipping Status</b></span>
                                </div>
                                <select name="shipping_status" class="form-control">
                                    <option {{!empty($data['shipping_status'])&&$data['shipping_status']=='SUDAH KIRIM'?'selected':''}}>SUDAH KIRIM</option>
                                    <option {{!empty($data['shipping_status'])&&$data['shipping_status']=='BELUM KIRIM'?'selected':''}}>BELUM KIRIM</option>
                                    <option {{!empty($data['shipping_status'])&&$data['shipping_status']=='GAGAL KIRIM'?'selected':''}}>GAGAL KIRIM</option>
                                    <option {{!empty($data['shipping_status'])&&$data['shipping_status']=='TERKIRIM'?'selected':''}}>TERKIRIM</option>
                                </select>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Resi Pengiriman</b></span>
                                </div>
                                <input type="text" name="shipping_receipt" value="{{ empty($data['shipping_receipt'])?old('shipping_receipt'):$data['shipping_receipt']}}" class="form-control" aria-describedby="basic-addon1">
                            </div>
                            <button type="submit" style="float:right" class="btn btn-primary">Save Data</button>
                        </div>
                    </div>
                </form>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection