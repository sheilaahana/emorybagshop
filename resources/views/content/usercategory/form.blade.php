@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>User Category Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('category/user')}}">User Category </a></li>
                <li class="breadcrumb-item active" aria-current="page">Form</li>
                </ol>
            </nav>
        </div>         
    </div>
</div>
<div class="card">
    <div class="body">
        <form action="{{url('/category/user/save')}}" method="post" class="m-form" id="fnotaris">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input name="id" type="hidden" class="form-control m-input" value="{{ empty($data->id)?old('id'):$data->id}}" placeholder="id">
            <div class="form-group">
                <label>Category</label>
                <input type="text" name="category" value="{{ empty($data->category)?old('category'):$data->category}}" class="form-control" required="">
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="description" required="">{{ empty($data->description)?old('description'):$data->description}}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
@endsection