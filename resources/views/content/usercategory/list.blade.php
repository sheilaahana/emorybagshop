@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>User Category</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">User Category</li>
                </ol>
            </nav>
        </div>            
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <a href="{{url('category/user/form')}}" class="btn btn-sm btn-primary" title="">Add Category User</a>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Created</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $d)
                        <tr>
                            <td><span>{{$d['id']}}</span></td>
                            <td>{{date('d M Y h:i:s', strtotime($d['created_at']))}}</td>
                            <td><span>{{$d['category']}}</span></td>
                            <td>{{$d['description']}}</td>
                            <td>
                                <a href="{{url('category/user/form?id='.$d['id'])}}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="{{url('category/user/delete/'.$d['id'])}}" onclick="return confirm('Are you sure you want to delete this item')" class="btn btn-sm btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="card" style="height:500px;overflow:auto;background-color:white;padding:20px">
            <b>Log Change</b>
            <ul>
                @forelse($log as $log)
                    <li><b>{{$log['change_by']}} {{$log['admin']}}</b> <br>{{$log['description']}}<br> <i>{{date('d/m/y h:i:s',strtotime($log['created_at']))}}</i></li>
                @empty
                    <li>Empty Data</li>
                @endforelse
            </ul>
        </div>
    </div>
</div>
@endsection