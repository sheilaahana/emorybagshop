@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; use App\Helpers\VoucherGenerate as VoucherGenerate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Voucher Detail</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('voucher')}}">Voucher</a></li>
                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
        </div>            
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <a href="{{url('voucher/form?id='.$data['id'])}}" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-pencil"></i>&emsp; Edit Voucher</a>
        </div>
    </div>
</div>
@if(!empty($data))
<div class="row clearfix">
    <div class="col-lg-5 col-md-5">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Created Date </small>
                    <p class="mb-0">{{date('d F y - h:i', strtotime($data['created_at']))}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Voucher Code </small>
                    <p class="mb-0">{{ucwords($data['code'])}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Valid Date Use ( Start - End ) </small>
                    <p class="mb-0">{{date('d F y - h:i', strtotime($data['start_date']))}} s/d {{date('d F y - h:i', strtotime($data['end_date']))}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Discount Type / Value</small>
                    <p class="mb-0">{{ucwords($data['discount_type'])}}
                    @if($data['discount_type']=='PERCENT')
                       - {{$data['value']}} %
                    @else
                       - {{Generate::money($data['value'])}}
                    @endif
                    <br><i>{{$data['is_join']=='1'?'Can be combined ':'Cannot be combined '}} with other vouchers</i>
                    </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Type Voucher</small>
                    <p class="mb-0">{{$data['type']}} </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Terms Spend ( Min - Max )</small>
                    <p class="mb-0">{{Generate::money($data['minimal_spend'])}} s/d {{Generate::money($data['maximal_spend'])}} </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Validity Qty ( User - General )</small>
                    <p class="mb-0">{{Generate::number($data['validity_user'])}} times use / User <br>
                     {{Generate::number($data['validity_general'])}} times use / Voucher</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Active Status / Can be Combined</small>
                    <p class="mb-0">
                    <a href="{{url('voucher/change/active/'.$data['id'])}}" onclick="return confirm('Are you sure you want to Change this?');" class="badge ml-0 mr-0">{{$data['is_valid']=='1'?'Active':'Inactive'}}</a></i>
                    - <a href="{{url('voucher/change/combine/'.$data['id'])}}" onclick="return confirm('Are you sure you want to Change this?');" class="badge ml-0 mr-0">{{$data['is_join']=='1'?'Can be combined':'Cannot be combined'}}</a></i>
                    </p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Total Redeemed</small>
                    <p class="mb-0">{{VoucherGenerate::redeemed($data['id'])}} times </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Product Include </small>
                    <p class="mb-0">
                    @if(!empty($data['product_include']))
                        
                        @forelse($data['product_include'] as $pi)
                        - {{ VoucherGenerate::pname($pi)}}<br>
                        @empty
                        Can be used for all products
                        @endforelse
                    @else
                        Can be used for all products
                    @endif
                    </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Product Exclude </small>
                    <p class="mb-0">
                    @if(!empty($data['product_exclude']))
                        @forelse($data['product_exclude'] as $pe)
                        - {{ VoucherGenerate::pname($pe)}}<br>
                        @empty
                            Can be used for all products
                        @endforelse
                    @else
                        Can be used for all products
                    @endif
                    </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Category Include </small>
                    <p class="mb-0">
                    @if(!empty($data['category_include']))
                        @forelse($data['category_include'] as $ci)
                        - {{ VoucherGenerate::category($ci)}}<br>
                        @empty
                        Can be used for all categories
                        @endforelse
                    @else
                        Can be used for all categories
                    @endif
                    </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Category Exclude </small>
                    <p class="mb-0">
                    @if(!empty($data['category_exclude']))
                        @forelse($data['category_exclude'] as $ce)
                        - {{ VoucherGenerate::category($ce)}}<br>
                        @empty
                            Can be used for all categories
                        @endforelse
                    @else
                        Can be used for all categories
                    @endif
                    </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Tag Include </small>
                    <p class="mb-0">
                    @if(!empty($data['tag_include']))
                        @forelse($data['tag_include'] as $ti)
                        - {{ $ti}}<br>
                        @empty
                        Can be used for all tags
                        @endforelse
                    @else
                        Can be used for all tags
                    @endif
                    </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Tag Exclude </small>
                    <p class="mb-0">
                    @if(!empty($data['tag_exclude']))
                        @forelse($data['tag_exclude'] as $te)
                        - {{ $te}}<br>
                        @empty
                            Can be used for all tags
                        @endforelse
                    @else
                        Can be used for all tags
                    @endif
                    </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">User Whitelist </small>
                    <p class="mb-0">
                    @if(!empty($data['whitelist_user']))
                        @forelse($data['whitelist_user'] as $ti)
                        - {!!  Generate::user($ti) !!}<br>
                        @empty
                        Can be used for all users
                        @endforelse
                    @else
                        Can be used for all users
                    @endif
                    </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">User Blacklist </small>
                    <p class="mb-0">
                    @if(!empty($data['blacklist_user']))
                        @forelse($data['blacklist_user'] as $te)
                        - {!! Generate::user($te) !!}<br>
                        @empty
                            Can be used for all users
                        @endforelse
                    @else
                        Can be used for all users
                    @endif
                    </p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="card" style="height:340px;overflow:auto;background-color:white;padding:20px">
            <b>Log Change</b>
            <ul>
                @forelse($data['log'] as $log)
                    <li><b>{{$log['change_by']}} {{$log['admin']}}</b> <br>{{$log['description']}}<br> <i>{{date('d/m/y h:i:s',strtotime($log['created_at']))}}</i></li>
                @empty
                    <li>Empty Data</li>
                @endforelse
            </ul>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <h5>List Redeem Voucher</h5><br>
                <table class="table table-hover spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>User</th>
                            <th>Transaction id</th>
                            <th>Detail</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($data['redeem'] as $d)
                        <tr>
                            <td><span>{{$d['id']}}</span></td>
                            <td><span>{!!Generate::user($d['users_id'])!!}</span></td>
                            <td><span>{{$d['transaction_id']}}</span></td>
                            <td>
                                {{$d['code']}}<br>
                                {{$d['discount_type']}} / {{$d['type']}}<br>
                                {{Generate::money($d['value'])}}<br>
                            </td>
                            <td><span>{{$d['created_at']}}</span></td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5"><b><i><center>Not Redeemed yet</center></i></b></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@else
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <center>
                <h6>Invalid Data / Data not Found</h6>
                <hr>
                <a href="{{url('Voucher')}}"> Back to List Voucher </a>
                </center>
            </div>
        </div>
    </div>
</div>

@endif
@endsection