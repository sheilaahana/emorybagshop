@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Voucher Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('voucher')}}">Voucher</a></li>
                @if(isset($data->id))
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('voucher/detail/'.$data->id)}}">Detail</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Voucher</li>
                @else
                <li class="breadcrumb-item active" aria-current="page">Add Voucher</li>
                @endif
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="card">
    <div class="body">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <form action="{{url('/voucher/save')}}" method="post" class="m-form" id="fnotaris" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input name="id" type="hidden" class="form-control m-input" value="{{ empty($data->id)?old('id'):$data->id}}" placeholder="id">
            <div class="row">
                <div class="form-group col-md-4">
                    <label>Code</label>
                    <input type="text" name="code" value="{{ empty($data->code)?old('code'):$data->code}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-4">
                    <label>Validity per use</label>
                    <input type="number" min="1" name="validity_user" value="{{ empty($data->validity_user)?old('validity_user'):$data->validity_user}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-4">
                    <label>Validity per voucher</label>
                    <input type="number" min="1" name="validity_general" value="{{ empty($data->validity_general)?old('validity_general'):$data->validity_general}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-4">
                    <label>Voucher Type</label>
                    <br>
                    <label class="fancy-radio">
                        <input type="radio" name="type" value="CART" required="" data-parsley-errors-container="#error-radio"  {{!empty($data['type'])&&$data['type']=='CART'?'checked':''}} >
                        <span><i></i>CART</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="type" value="PRODUCT" {{!empty($data['type'])&&$data['type']=='PRODUCT'?'checked':''}} >
                        <span><i></i>PRODUCT</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="type" value="SHIPPING" {{!empty($data['type'])&&$data['type']=='SHIPPING'?'checked':''}} >
                        <span><i></i>SHIPPING</span>
                    </label>
                    <p id="error-radio"></p>
                </div>
                <div class="form-group col-md-4">
                    <label>Discount Type</label>
                    <br>
                    <label class="fancy-radio">
                        <input type="radio" name="discount_type" value="PERCENT" required="" data-parsley-errors-container="#error-radio"  {{!empty($data['discount_type'])&&$data['discount_type']=='PERCENT'?'checked':''}} >
                        <span><i></i>PERCENT</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="discount_type" value="AMOUNT" {{!empty($data['discount_type'])&&$data['discount_type']=='AMOUNT'?'checked':''}} >
                        <span><i></i>AMOUNT</span>
                    </label>
                    <p id="error-radio"></p>
                </div>
                <div class="form-group col-md-4">
                    <label>Value</label>
                    <input type="text" name="value" value="{{ empty($data->value)?old('value'):$data->value}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-4">
                    <label>Minimal Spend</label>
                    <input type="number" min="1" name="minimal_spend" value="{{ empty($data->minimal_spend)?old('minimal_spend'):$data->minimal_spend}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-4">
                    <label>Maximal Spend</label>
                    <input type="number" min="1" name="maximal_spend" value="{{ empty($data->maximal_spend)?old('maximal_spend'):$data->maximal_spend}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-2">
                    <label>Active Status</label>
                    <br>
                    <label class="fancy-radio">
                        <input type="radio" name="is_valid" value="1" required="" data-parsley-errors-container="#error-radio"  {{isset($data['is_valid'])&&$data['is_valid']=='1'?'checked':''}} >
                        <span><i></i>Active</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="is_valid" value="0" {{isset($data['is_valid'])&&$data['is_valid']=='0'?'checked':''}} >
                        <span><i></i>InActive</span>
                    </label>
                    <p id="error-radio"></p>
                </div>
                <div class="form-group col-md-2">
                    <label>Combine Status</label>
                    <br>
                    <label class="fancy-radio">
                        <input type="radio" name="is_join" value="1" data-parsley-errors-container="#error-radio"  {{isset($data['is_join'])&&$data['is_join']=='1'?'checked':''}} >
                        <span><i></i>Can</span>
                    </label>
                    <label class="fancy-radio">
                        <input type="radio" name="is_join" value="0" {{isset($data['is_join'])&&$data['is_join']=='0'?'checked':''}} >
                        <span><i></i>Cannot</span>
                    </label>
                    <p id="error-radio"></p>
                </div>
                <div class="form-group col-md-6">
                    <label>Start Date</label>
                    <input type="date" name="start_date" value="{{ empty($data->start_date)?old('start_date'):$data->start_date}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-6">
                    <label>End Date</label>
                    <input type="date" name="end_date" value="{{ empty($data->end_date)?old('end_date'):$data->end_date}}" class="form-control" required="">
                </div>
                <div class="col-lg-6 col-md-12">
                    <label>Product Include</label>
                    <div class="multiselect_div">
                        <select id="product_id" name="product_include[]" class="multiselect multiselect-custom form-control" multiple="multiple">
                            @foreach($helper['product'] as $p)
                            <option value="{{$p['id']}}" {{isset($data['product_include'])&&in_array($p['id'],$data['product_include'])?'selected':''}}>{{$p['product_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label>Product Exclude</label>
                    <div class="multiselect_div">
                        <select id="product_exclude" name="product_exclude[]" class="multiselect multiselect-custom form-control" multiple="multiple">
                            @foreach($helper['product'] as $p)
                            <option value="{{$p['id']}}" {{isset($data['product_exclude'])&&in_array($p['id'],$data['product_exclude'])?'selected':''}}>{{$p['product_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label>Category Include</label>
                    <div class="multiselect_div">
                        <select id="category_include" name="category_include[]" class="multiselect multiselect-custom form-control" multiple="multiple">
                            @foreach($helper['category'] as $c)
                            <option value="{{$c['id']}}" {{isset($data['category_include'])&&in_array($c['id'],$data['category_include'])?'selected':''}}>{{$c['category_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label>Category Exclude</label>
                    <div class="multiselect_div">
                        <select id="category_exclude" name="category_exclude[]" class="multiselect multiselect-custom form-control" multiple="multiple">
                            @foreach($helper['category'] as $c)
                            <option value="{{$c['id']}}" {{isset($data['category_exclude'])&&in_array($c['id'],$data['category_exclude'])?'selected':''}}>{{$c['category_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label>Tags Include</label>
                    <div class="multiselect_div">
                        <select id="tag_include" name="tag_include[]" class="multiselect multiselect-custom form-control" multiple="multiple">
                            @foreach($helper['tag'] as $t)
                            <option value="{{$t}}" {{isset($data['tag_include'])&&in_array($t,$data['tag_include'])?'selected':''}}>{{ucwords(str_replace('_',' ',$t))}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label>Tags Exclude</label>
                    <div class="multiselect_div">
                        <select id="tag_exclude" name="tag_exclude[]" class="multiselect multiselect-custom form-control" multiple="multiple">
                            @foreach($helper['tag'] as $t)
                            <option value="{{$t}}" {{isset($data['tag_exclude'])&&in_array($t,$data['tag_exclude'])?'selected':''}}>{{ucwords(str_replace('_',' ',$t))}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label>Whitelist User</label>
                    <div class="multiselect_div">
                        <select id="whitelist_user" name="whitelist_user[]" class="multiselect multiselect-custom form-control" multiple="multiple">
                            @foreach($helper['user'] as $u)
                            <option value="{{$u['id']}}" {{isset($data['whitelist_user'])&&in_array($u['id'],$data['whitelist_user'])?'selected':''}}>{{strtolower($u['email'])}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label>Blacklist User</label>
                    <div class="multiselect_div">
                        <select id="blacklist_user" name="blacklist_user[]" class="multiselect multiselect-custom form-control" multiple="multiple">
                            @foreach($helper['user'] as $u)
                            <option value="{{$u['id']}}" {{isset($data['blacklist_user'])&&in_array($u['id'],$data['blacklist_user'])?'selected':''}}>{{strtolower($u['email'])}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>

@endsection
@push('scripts')
<script>
$(function() {

   // Multiselect
    $('.multiselect').multiselect({
        enableFiltering: true,
        maxHeight: 300,
    });
});
</script>
@endpush
