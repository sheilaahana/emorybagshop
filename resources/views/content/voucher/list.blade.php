@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; use App\Helpers\VoucherGenerate as VoucherGenerate; ?>

<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Voucher</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Voucher</li>
                </ol>
            </nav>
        </div>            
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <a href="{{url('voucher/form')}}" class="btn btn-sm btn-primary" title="">Add Voucher</a>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Code</th>
                            <th>Type</th>
                            <th>Date</th>
                            <th>Validity</th>
                            <th>Redeemed</th>
                            <th>Active</th>
                            <th>Combine</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $d)
                        <tr>
                            <td><span>{{$d['id']}}</span></td>
                            <td>
                                <b><span>{{$d['code']}}</span></b>
                            </td>
                            <td>
                                <b><span>{{$d['type']}}</span></b><br>
                                <span>{{$d['discount_type']}}<br>( {{ Generate::number($d['value'])}} ) </span>
                            </td>
                            <td>
                                <b>Start :</b> <br>{{date('d F Y', strtotime($d['start_date']))}}
                                <br><b>End :</b> <br>{{date('d F Y', strtotime($d['end_date']))}}
                            </td>
                            <td>
                                <b> User :</b> {{$d['validity_user']?:'undefinded'}}
                                <br><b> General :</b>{{$d['validity_general']?:'undefinded'}}
                            </td>
                            <td><span>{{VoucherGenerate::redeemed($d['id'])}} x </span></td>
                            <td>
                                <?php if($d['is_valid']=="0"){$badge='badge-danger';}elseif($d['is_valid']=='1'){$badge='badge-success';}else{$badge='badge-primary';} ?>
                                <a href="{{url('voucher/change/active/'.$d['id'])}}" onclick="return confirm('Are you sure you want to Change this?');" class="badge {{$badge}} ml-0 mr-0">{{$d['is_valid']=='1'?'Active':'Inactive'}}</a></i>
                            </td>
                            <td>
                                <?php if($d['is_join']=="0"){$badge='badge-danger';}elseif($d['is_join']=='1'){$badge='badge-success';}else{$badge='badge-primary';} ?>
                                <a href="{{url('voucher/change/combine/'.$d['id'])}}" onclick="return confirm('Are you sure you want to Change this?');" class="badge {{$badge}} ml-0 mr-0">{{$d['is_join']=='1'?'Can be combined':'Cannot be combined'}}</a></i>
                            </td>
                            <td>
                                <a href="{{url('voucher/detail/'.$d['id'])}}" class="btn btn-sm btn-info" title="Edit"><i class="fa fa-eye"></i></a>
                                <a href="{{url('voucher/form?id='.$d['id'])}}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="{{url('voucher/delete/'.$d['id'])}}" onclick="return confirm('Are you sure you want to delete this item')" class="btn btn-sm btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection