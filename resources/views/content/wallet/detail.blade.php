@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Wallet Detail</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('wallet')}}"> Wallet </a></li>
                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
@if(!empty($data))
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Name </small>
                    <p class="mb-0"><a href="{{url('customer/detail/'.$data['users']['id'])}}" target="_blank">{{ucwords($data['users']['name'])}}</a></p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Email </small>
                    <p class="mb-0">{{$data['users']['email']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Phone </small>
                    <p class="mb-0">{{$data['users']['phone']?:'-'}}</p>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <h5>History Wallet</h5><br>
                <table class="table table-hover spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Input By</th>
                            <th>Memo</th>
                            <th>Type</th>
                            <th>Amount</th>
                            <th>SubTotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1;$subtotal=0;?>
                        @forelse($data['wallet'] as $d)
                        <?php 
                            if($d['type'] == 'DB'){
                                $subtotal = $subtotal + $d['amount']; 
                            }elseif($d['type'] == 'CR'){
                                $subtotal = $subtotal - $d['amount']; 
                            }
                        ?>
                        <tr>
                            <td><span>{{$no++}}</span></td>
                            <td><span>{{date('d M Y h:i:s', strtotime($d['created_at']))}}</span></td>
                            <td><span>{{$d['input_by']}} {{$d['admin']['name']}}</span></td>
                            <td><span>{{$d['memo']}}</span></td>
                            <td>
                                <?php if($d['type']=="DB"){$badge='badge-success';}elseif($d['type']=='CR'){$badge='badge-danger';}else{$badge='badge-warning';} ?>
                                <span class="badge {{$badge}} ml-0 mr-0">{{$d['type']}}</a>
                            </td>
                            <td><span>{{Generate::money($d['amount'])}}</span></td>
                            <td><b>{{Generate::money($subtotal)}}</b></td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="7"><b><i><center>Not Redeemed yet</center></i></b></td>
                        </tr>
                        @endforelse
                        <tr>
                            <td colspan="4"></td><td><b><i>Total </i></b></td><td><b><i>{{Generate::money($subtotal)}}</i></b></td>
                        </tr>
                        <tr>
                            <td colspan="7"><b><i><hr>Add New Record</i></b></td>
                        </tr>
                        <form action="{{url('/wallet/save')}}" method="post" class="m-form" id="fnotaris" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <input name="users_id" type="hidden" class="form-control m-input" value="{{ empty($data['users']['id'])?old('id'):$data['users']['id']}}" placeholder="id">
                        <tr>
                            <td><span>Add</span></td>
                            <td><b>{{date('d M Y h:i:s')}}</b></td>
                            <td><span>ADMIN</span></td>
                            <td>
                                <input type="text" name="memo" class="form-control" placeholder="memo" required>
                            </td>
                            <td>
                                <select id="type" name="type" class="form-control" required>
                                    <option value="CR">CREDIT</option>
                                    <option value="DB">DEBIT</option>
                                </select>
                            </td>
                            <td><input type="number" name="amount" class="form-control" placeholder="Amount : 100000" required></td>
                            <td> <button type="submit" class="btn btn-primary">Submit</button></td>
                        </tr>
                        </form>
                    </tbody>
                </table>
            </div> 
        </div>
    </div>
</div>
@endif
@endsection