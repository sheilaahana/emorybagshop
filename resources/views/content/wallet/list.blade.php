@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; use App\Helpers\VoucherGenerate as VoucherGenerate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Wallet</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Wallet</li>
                </ol>
            </nav>
        </div>            
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Customer</th>
                            <th>Total Wallet</th>
                            <th>Total Transaction</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $d)
                        <tr>
                            <td><span>{{$d['id']}}</span></td>
                            <td><span><b>{{$d['name']}}</b><br>{{$d['email']}}</span></td>
                            <td><span>{{Generate::money($d['total_wallet'])}}</span></td>
                            <td><span>{{Generate::number($d['qty_transaction'])}}</span></td>
                            <td>
                                <a href="{{url('wallet/detail/'.$d['id'])}}" class="btn btn-sm btn-info" title="Edit"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection