@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Warehouse Detail</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('warehouse')}}">Warehouse</a></li>
                <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
        </div>            
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <a href="{{url('warehouse/form?id='.$data['id'])}}" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-pencil"></i>&emsp; Edit Warehouse</a>
        </div>
    </div>
</div>
@if(!empty($data))
<div class="row clearfix">
    <div class="col-lg-9 col-md-9">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Warehouse ID </small>
                    <p class="mb-0">ID - {{$data['id']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Warehouse </small>
                    <p class="mb-0">{{ucwords($data['warehouse'])}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Code </small>
                    <p class="mb-0">{{ucwords($data['code'])}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Location</small>
                    <p class="mb-0">{{$data['location']}} </p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Date </small>
                    <p class="mb-0">{{date('d F y - h:i', strtotime($data['created_at']))}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Catatan </small>
                    <p class="mb-0">{{$data['catatan']}}</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-3 col-md-3">
        <div class="card" style="height:340px;overflow:auto;background-color:white;padding:20px">
            <b>Log Change</b>
            <ul>
                @forelse($data['log'] as $log)
                    <li><b>{{$log['change_by']}} {{$log['admin']}}</b> <br>{{$log['description']}}<br> <i>{{date('d/m/y h:i:s',strtotime($log['created_at']))}}</i></li>
                @empty
                    <li>Empty Data</li>
                @endforelse
            </ul>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <div class="row">
                    <div class="col-md-8">
                        <h5>List Item Warehouse</h5><br>
                    </div>
                    <div class="col-md-4 col-sm-12 text-right hidden-xs">
                        <a href="{{url('warehouse/form/product/'.$data['id'])}}" class="btn btn-sm btn-info" title="Themeforest"><i class="icon-plus"></i>&emsp; Add Product Stock</a>
                    </div>
                </div>
               
                <table class="table js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>Product</th>
                            <th>Nomor Rak</th>
                            <th>Baris/Kolom</th>
                            <th>Stock</th>
                            <th>Note</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($data['product'])>0)
                        <?php $total=0; ?>
                        @foreach($data['product'] as $d)
                        <?php $subtotal = $d['price']*$d['qty']; $total = $total + $subtotal; ?>
                        <tr>
                            <td><b>{{$d['product_name']}}</b> - {{$d['variant']}}</td>
                            <td>{{$d['nomor_rak']}}</td>
                            <td> {{$d['baris']}} / {{$d['kolom']}}</td>
                            <td>{{$d['stock']}}</td>
                            <td>{{$d['catatan']}}</td>
                            <td>
                                <a href="{{url('/warehouse/transfer/'.$d['id'])}}" class="btn btn-sm btn-success" title="Transfer Stock"><i class="fa fa-refresh"></i> </a>
                                <a href="#" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal-{{$d['variant_id']}}" title="Edit"><i class="fa fa-pencil"></i> </a>

                                <div class="modal fade" id="Modal-{{$d['variant_id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <form action="{{url('/warehouse/save/detail')}}" method="post" class="m-form" >
                                            <div class="modal-header">
                                                <h5 class="modal-title"> Change Warehouse Detail</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <table width="100%"> 
                                                    <tr><td>Warehouse</td><td>: {{$data['warehouse']}}</td></tr>
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                                    <input type="hidden" class="form-control" name="warehouse_id" value="{{$data['id']}}">
                                                    <input type="hidden" class="form-control" name="product_id" value="{{$d['product_id']}}">
                                                    <input type="hidden" class="form-control" name="variant_id" value="{{$d['variant_id']}}">
                                                    <tr><td>Product Name</td><td>: {{$d['product_name']}}</td></tr>
                                                    <tr><td>Variant</td><td>: {{$d['variant']}}</td></tr>
                                                    <tr><td>Nomor Rak</td><td><input type="text" class="form-control" name="nomor_rak" value="{{$d['nomor_rak']}}"></td></tr>
                                                    <tr><td>Baris</td><td><input type="text" class="form-control" name="baris" value="{{$d['baris']}}"></td></tr>
                                                    <tr><td>Kolom</td><td><input type="text" class="form-control" name="kolom" value="{{$d['kolom']}}"></td></tr>
                                                    <tr><td>Stock</td><td><input type="number" class="form-control" name="stock" min="0" value="{{$d['stock']}}"></td></tr>
                                                    <tr><td>Note</td><td><textarea name="catatan" id="" rows="5" class="form-control">{{$d['catatan']}}</textarea></td></tr>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5"><b><i><center>Invalid Data Warehouse</center></i></b></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@else
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <center>
                <h6>Invalid Data / Data not Found</h6>
                <hr>
                <a href="{{url('Warehouse')}}"> Back to List Warehouse </a>
                </center>
            </div>
        </div>
    </div>
</div>

@endif
@endsection