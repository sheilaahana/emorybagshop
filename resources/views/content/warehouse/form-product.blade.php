@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage;?>
<div id="vapp">
    <div class="block-header" >
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h1>Add Product Stock</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="{{url('warehouse')}}">Warehouse</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="{{url('warehouse/detail/'.$data['warehouse_id'])}}">Detail</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Form Stock </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="body">
            <form action="{{url('/warehouse/save/product')}}" method="post" class="m-form" id="fnotaris" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <div class="row clearfix">
                    <div class="form-group col-md-4">
                        <label>Tambah Product ( Qty ) </label>
                        <div class="multiselect_div">
                            <input type="number" v-model.number="qty_p" min="1" class="form-control" required="">
                        </div>
                    </div>
                    
                    <div class="col-md-12 clearfix" id="form-product">
                        <div class="row" v-for="i in qty_p" :key="i" style="border: 1px solid lightgray;padding: 10px;margin:10px;">
                            <div class="col-md-12">
                                <h5><u>Product @{{ i }}</u></h5>
                            </div>
                            <div class="form-group col-md-5">
                                <label>Product Name</label>
                                <v-select
                                    :options='product'
                                    label="product_name"
                                    :reduce="product_name => product_name.id" 
                                    v-model=form.product[i]
                                    @input="onChangeProduct($event)"
                                    required
                                >
                                </v-select>
                                <!-- <div class="multiselect_div">
                                    <select id="product_id" v-model=form.product[i] @change="onChangeProduct($event)" class="multiselect multiselect-custom select2 form-control" >
                                        <option value="">-- Select Product --</option>
                                        <option v-for="p in product" :value=p.id >@{{ p.product_name }}</option>
                                    </select>
                                </div> -->
                            </div>
                            <div class="form-group col-md-5">
                                <label>Variant</label>
                                <div class="multiselect_div">
                                    <select id="a" v-model=form.variant[i] class="multiselect multiselect-custom form-control" >
                                        <option value="">-- Select Variant --</option>
                                        <option v-for="v in variant[form.product[i]]" :value=v.id> @{{ v.variant }}  </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Qty</label>
                                <input type="number" v-model=form.stock[i] min="1" class="form-control" required="">
                            </div>
                            <div class="form-group col-md-5">
                                <label>Catatan</label>
                                <input type="text" v-model=form.catatan[i] min="1" class="form-control" required="">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Nomor Rak</label>
                                <input type="text" v-model=form.nomor_rak[i] class="form-control" required="">
                            </div>
                            <div class="form-group col-md-2">
                                <label>Baris</label>
                                <input type="text" v-model=form.baris[i]  class="form-control" required="">
                            </div>
                            <div class="form-group col-md-2">
                                <label>Kolom</label>
                                <input type="text" v-model=form.kolom[i]  class="form-control" required="">
                            </div>
                            
                        </div>
                    </div>
                </div>
                <hr>
                <button type="submit" @click.prevent="saveData()" class="btn btn-primary">Save</button>
            </form>

        </div>
    </div>
</div>
@endsection
@push('scripts')

<script>
Vue.component('v-select', VueSelect.VueSelect);

$(document).ready(function() {
    $('.select2').select2();
});
const vueApp = new Vue({
    el: '#vapp',
    data: { 
        product: [],
        variant: {},
        qty_p : 1,
        form: {
            id : {{$data['warehouse_id']}},
            product : {},
            variant : {},
            stock : {},
            catatan : {},
            nomor_rak : {},
            baris : {},
            kolom : {},
        },
    },
    mounted() {
        axios.get('/data/product')
        .then(response => (this.product = response.data));
        axios.get('/data/user')
        .then(response => (this.user = response.data));
    },
    methods : {
        getvariant(id){
            // // console.log("coba");
            axios.get('/data/variant/'+id)
            .then(response => (this.variant[id] = response.data))
        },
        onChangeProduct(event){
            id = event;
            console.log('product'+id);
            var _vm = this;
            axios.get('/data/variant/'+id)
            .then(function (response) {
                // handle success
                _vm.variant[id] = response.data; 
                console.log(_vm.variant[id]); 
            });
            console.log('/data/variant/'+id+'/'+this.form.users_id);
        },
        saveData(){
            axios.post('/warehouse/save/product',{
                id : this.form.id,
                product : this.form.product,
                variant : this.form.variant,
                catatan : this.form.catatan,
                nomor_rak : this.form.nomor_rak,
                kolom : this.form.kolom,
                baris : this.form.baris,
                stock : this.form.stock,
                total_product : this.qty_p,
            })
            .then ( function (response){
                // window.location.href = "/warehouse";
                console.log(response.data.meta);
                if(response.data.meta.code == 200){
                    window.location.href = "/warehouse/detail/{{$data['warehouse_id']}}";
                }
            })
            .catch ( function (error){
                console.log("error");
                // console.log(error); 
            });
        }
    },
});


</script>
@endpush
