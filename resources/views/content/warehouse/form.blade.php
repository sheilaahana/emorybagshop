@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Warehouse Form</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('warehouse')}}">Warehouse</a></li>
                @if(isset($data->id))
                <li class="breadcrumb-item" aria-current="page"><a href="{{url('warehouse/detail/'.$data->id)}}">Detail</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Warehouse</li>
                @else
                <li class="breadcrumb-item active" aria-current="page">Add Warehouse</li>
                @endif
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="card">
    <div class="body">
        <form action="{{url('/warehouse/save')}}" method="post" class="m-form" id="fnotaris" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" />
            <input name="id" type="hidden" class="form-control m-input" value="{{ empty($data->id)?old('id'):$data->id}}" placeholder="id">
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Warehouse Name</label>
                    <input type="text" name="warehouse" value="{{ empty($data->warehouse)?old('warehouse'):$data->warehouse}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-12">
                    <label>Code</label>
                    <input type="text" name="code" value="{{ empty($data->code)?old('code'):$data->code}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-12">
                    <label>Location</label>
                    <input type="text" name="location" value="{{ empty($data->location)?old('location'):$data->location}}" class="form-control" required="">
                </div>
                <div class="form-group col-md-12">
                    <label>Catatan</label>
                    <textarea class="form-control" name="catatan" required="">{{ empty($data->catatan)?old('catatan'):$data->catatan}}</textarea>
                </div>
                
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script>
$(function() {

   // Multiselect
    $('.multiselect').multiselect({
        enableFiltering: true,
        maxHeight: 300,
    });
});
</script>
@endpush
