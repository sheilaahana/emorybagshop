@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Warehouse</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page">Warehouse</li>
                </ol>
            </nav>
        </div>            
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <a href="{{url('warehouse/form')}}" class="btn btn-sm btn-info" title="Themeforest"><i class="icon-plus"></i>&emsp; Add Warehouse</a>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        @if (Session::get('message'))
        <div class="alert alert-danger">
            {{Session::get('message')}}
        </div>
        @endif
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Warehouse</th>
                            <th>Code</th>
                            <th>Location</th>
                            <th>Catatan </th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1;?>
                        @foreach($data as $d)
                        <tr>
                            <td><span>{{$no++}}</span></td>
                            <td>{{$d['warehouse']?:"-"}}</td>
                            <td>{{$d['code']?:"-"}}</td>
                            <td><span>{{$d['location']?:"-"}}</span></td>
                            <td>{{$d['catatan']?:"Undefinded"}}</td>
                            <td>
                                <a href="{{url('warehouse/detail/'.$d['id'])}}" class="btn btn-sm btn-info" title="Detail"><i class="fa fa-eye"></i> </a>
                                <a href="{{url('warehouse/form?id='.$d['id'])}}" class="btn btn-sm btn-warning" title="Edit"><i class="fa fa-pencil"></i> </a>
                                <a href="{{url('warehouse/delete/'.$d['id'])}}" class="btn btn-sm btn-danger" title="Delete" onclick="return confirm('Are you sure you want to delete this cart item')"><i class="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!-- -->
            </div>
        </div>
    </div>
</div>
@endsection