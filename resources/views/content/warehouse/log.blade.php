@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Warehouse Detail - Log Retur</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('warehouse')}}">Warehouse</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('warehouse/detail/'.$data['warehouse_id'])}}">Detail</a></li>
                <li class="breadcrumb-item active" aria-current="page">Warehouse</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
@if(!empty($data))
<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Product Name </small>
                    <p class="mb-0">{{ucfirst($data['product']['product_name'])}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Variant </small>
                    <p class="mb-0">{{ucfirst($data['variant'])}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Qty Stock </small>
                    <p class="mb-0">{{$data['qty_stock']}} pcs</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Valid Stock </small>
                    <p class="mb-0">{{$data['valid_stock']}} pcs</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="card">
            <ul class="list-group">
                
                <li class="list-group-item">
                    <small class="text-muted">Default Image</small>
                    <p><img src="{{$data['image']}}" alt="" style="width:100%;max-width:300px"></p>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body" style="overflow-x:auto">
                <div class="row">
                    <div class="col-md-10 col-sm-12">  
                        <h5>Warehouse Log Retur</h5>
                    </div>
                    <div class="col-md-2 col-sm-12 text-right hidden-xs">
                        <!-- <a href="{{url('warehouse/log/form?variant_id='.$data['id'])}}" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-plus"></i>&emsp; Add Log</a> -->
                    </div>
                </div><br>
                <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Location Origin</th>
                            <th>Location Destination</th>
                            <th>Type</th>
                            <th>Qty</th>
                            <th>Memo</th>
                            <th>By</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1;?>
                        @forelse($data['warehouselog'] as $d)
                        <tr>
                            <td><span>{{$no++}}</span></td>
                            <td><span>{{date('d M Y h:i:s', strtotime($d['updated_at']))}}</span></td>
                            <td><span>{{$d['location_origin']}}</span></td>
                            <td><span>{{$d['location_destination']}}</span></td>
                            <td><span>{{$d['type']}}</span></td>
                            <td><span>{{$d['qty']}} pcs</span></td>
                            <td><span>{{$d['memo']}}</span></td>
                            <td><span>{{Generate::admin($d['input_by'])}}</span></td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="7"><b><i><center>Empty Data</center></i></b></td>
                        </tr>
                        @endforelse
                        <tr>
                            <td colspan="7"><b><i><hr>Add New Record</i></b></td>
                        </tr>
                        <form action="{{url('/warehouse/log/save')}}" method="post" class="m-form" id="fnotaris" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <input name="variant_id" type="hidden" class="form-control m-input" value="{{ empty($data['id'])?old('id'):$data['id']}}" placeholder="id">
                            <tr>
                                <td><span>Add</span></td>
                                <td><b>{{date('d M Y h:i:s')}}</b></td>
                                <td>
                                    <input type="text" name="location_origin" class="form-control" placeholder="location origin" required>
                                </td>
                                <td>
                                    <input type="text" name="location_destination" class="form-control" placeholder="location destination" required>
                                </td>
                                <td>
                                    <select id="type" name="type" class="form-control" required>
                                        <option value="MINUS">MINUS</option>
                                        <option value="PLUS">PLUS</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="number" name="qty" class="form-control" placeholder="qty" required>
                                </td>
                                <td>
                                    <input type="text" name="memo" class="form-control" placeholder="memo" required>
                                </td>
                                <td><b>{{Auth::user()['name']}}</b></td>
                            </tr>
                            <tr>
                                <td colspan="7"><center><button type="submit" class="btn btn-primary">Submit</button></center></td>
                            </tr>
                        </form>
                    </tbody>
                </table>
            </div> 
        </div>
    </div>
</div>
@endif
@endsection