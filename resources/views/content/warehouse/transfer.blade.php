@extends('layouts.app')

@section('content')
<?php use App\Helpers\Generate as Generate; 
use Illuminate\Support\Facades\Storage; ?>
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h1>Warehouse Stock Transfer</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('')}}">Emory</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('warehouse')}}">Warehouse</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="{{url('warehouse/detail/'.$data['warehouse_id'])}}">Detail</a></li>
                <li class="breadcrumb-item active" aria-current="page">Transfer Stock</li>
                </ol>
            </nav>
        </div> 
    </div>
</div>
@if(!empty($data))
<div class="row clearfix">
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted">Warehouse ID </small>
                    <p class="mb-0">ID - {{$data['warehouse_id']}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Warehouse </small>
                    <p class="mb-0">{{ucwords($data['warehouse'])}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Product Name </small>
                    <p class="mb-0">{{ucwords($data['product_name'])}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Variant </small>
                    <p class="mb-0">{{ucwords($data['variant'])}}</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Stock </small>
                    <p class="mb-0">{{ucwords($data['stock'])}} pcs</p>
                </li>
                <li class="list-group-item">
                    <small class="text-muted">Image Variant </small>
                    <p class="mb-0">{{ucwords($data['image_variant'])}}</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="card">
            <form action="{{url('warehouse/save/transfer')}}" method="post" autocomplete="off">
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <input type="hidden" name="warehouse_origin" id="" class="form-control" value="{{$data['warehouse_id']}}" required>
                <input type="hidden" name="detail_id" id="" class="form-control" value="{{$data['id']}}" required>
                <ul class="list-group">
                    <li class="list-group-item">
                        <small class="text-muted">Warehouse Destination </small>
                        <select name="warehouse_destination" class="form-control" id="" required>
                            @foreach($data['wh'] as $wh)
                            <option value="{{$wh['id']}}">{{$wh['warehouse']}}</option>
                            @endforeach
                        </select>
                    </li>
                    <li class="list-group-item">
                        <small class="text-muted">Stock Transfer </small>
                        <input type="number" name="stock" id="" min="0" max="{{$data['stock']}}" class="form-control" required>
                    </li>
                    <li class="list-group-item">
                        <small class="text-muted">Notes </small>
                        <input type="text" name="catatan" id="" class="form-control" required>
                    </li>
                    <li class="list-group-item">
                        <button type="submit" class="btn btn-primary">Transfer Stock</button>
                        </li>
                </ul>
            </form>
        </div>
    </div>
</div>
@else
<div class="row clearfix">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="body">
                <center>
                <h6>Invalid Data / Data not Found</h6>
                <hr>
                <a href="{{url('Warehouse')}}"> Back to List Warehouse </a>
                </center>
            </div>
        </div>
    </div>
</div>

@endif
@endsection