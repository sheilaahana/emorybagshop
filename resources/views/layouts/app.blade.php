<!doctype html>
<html lang="en">

<head>
<title>EmoryBagShop | Dashboard</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Emory Bag Shop.">
<meta name="keywords" content="Emory, BagShop, Bag, Shop">
<meta name="author" content="Sheila @ MamaneStudio">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{url('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{url('assets/vendor/animate-css/vivify.min.css')}}">

<link rel="stylesheet" href="{{url('assets/vendor/c3/c3.min.css')}}"/>
<link rel="stylesheet" href="{{url('assets/vendor/jvectormap/jquery-jvectormap-2.0.3.css')}}">

<link rel="stylesheet" href="{{url('assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{url('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{url('assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{url('assets/vendor/sweetalert/sweetalert.css')}}"/>
<link rel="stylesheet" href="{{url('assets/vendor/summernote/dist/summernote.css')}}"/>
<link rel="stylesheet" href="{{url('assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css')}}">
<link rel="stylesheet" href="{{url('assets/vendor/multi-select/css/multi-select.css')}}">
<link rel="stylesheet" href="{{url('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}">


<link rel="stylesheet" href="{{url('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
<link rel="stylesheet" href="{{url('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
<link rel="stylesheet" href="{{url('assets/vendor/nouislider/nouislider.min.css')}}">

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vuex"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
<link rel="stylesheet" href="https://unpkg.com/vue-select@latest/dist/vue-select.css">

<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script src="https://unpkg.com/vue@latest"></script>



<!-- MAIN CSS -->
<link rel="stylesheet" href="{{url('assets/css/site.min.css')}}">
<link rel="stylesheet" href="{{url('css/style.css')}}">

</head>
<body class="theme-cyan font-montserrat light_version">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<div id="wrapper">

    <nav class="navbar top-navbar">
        <div class="container-fluid">
            <div class="navbar-left">
                <div class="navbar-btn">
                    <a href=""><img src="{{url('assets/images/icon.svg')}}" alt="Oculux Logo" class="img-fluid logo"></a>
                    <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
                </div>
            </div>
            
            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                        <li><a class="icon-menu" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon-power"></i></a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                </div>
            </div>
        </div>
        <div class="progress-container"><div class="progress-bar" id="myBar"></div></div>
    </nav>  

    <div id="left-sidebar" class="sidebar">
        <div class="navbar-brand">
            <a href=""><img src="{{url('assets/images/icon.svg')}}" alt="Oculux Logo" class="img-fluid logo"><span>EmoryBagShop</span></a>
            <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
        </div>
        @include('layouts.sidebar')
    </div>

    <div id="main-content">
        <div class="container-fluid">
            @yield('content')            
        </div>
    </div>
    
</div>

<!-- Javascript -->
<script src="{{url('assets/bundles/libscripts.bundle.js')}}"></script>
<script src="{{url('assets/bundles/mainscripts.bundle.js')}}"></script>
<script src="{{url('assets/bundles/vendorscripts.bundle.js')}}"></script>

<script src="{{url('assets/bundles/c3.bundle.js')}}"></script>
<script src="{{url('assets/bundles/flotscripts.bundle.js')}}"></script>
<script src="{{url('assets/bundles/jvectormap.bundle.js')}}"></script>
<script src="{{url('assets/vendor/jvectormap/jquery-jvectormap-us-aea-en.js')}}"></script>

<script src="{{url('assets/bundles/apexcharts.bundle.js')}}"></script>

<script src="{{url('assets/js/hrdashboard.js')}}"></script>

<script src="{{url('assets/bundles/datatablescripts.bundle.js')}}"></script>
<script src="{{url('assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{url('assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
<script src="{{url('assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{url('assets/vendor/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
<script src="{{url('assets/vendor/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
<script src="{{url('assets/vendor/sweetalert/sweetalert.min.js')}}"></script><!-- SweetAlert Plugin Js -->
<script src="{{url('assets/js/pages/tables/jquery-datatable.js')}}"></script>
<script src="{{url('assets/vendor/summernote/dist/summernote.js')}}"></script>
<script src="{{url('assets/js/pages/forms/advanced-form-elements.js')}}"></script>
<script src="{{url('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script><!-- Bootstrap Colorpicker Js --> 
<script src="{{url('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{url('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script><!-- Bootstrap Tags Input Plugin Js --> 
<script src="{{url('assets/vendor/multi-select/js/jquery.multi-select.js')}}"></script><!-- Multi Select Plugin Js -->
<script src="{{url('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>


<script src="{{url('assets/vendor/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script><!-- Input Mask Plugin Js --> 
<script src="{{url('assets/vendor/jquery.maskedinput/jquery.maskedinput.min.js')}}"></script>
<script src="{{url('assets/vendor/nouislider/nouislider.js')}}"></script><!-- noUISlider Plugin Js -->

<script src="{{url('assets/js/pages/charts/apex.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#province").change(function (){
            var url = "{{url('ajax/city')}}/"+$(this).val();
            $('#city').load(url);
            return false;
        });
        $("#province2").change(function (){
            var url = "{{url('ajax/city')}}/"+$(this).val();
            $('#city2').load(url);
            return false;
        });
        $("#city").change(function (){
            var url = "{{url('ajax/district')}}/"+$(this).val();
            $('#district').load(url);
            return false;
        });
        $(".productdata").change(function (){
            var url = "{{url('ajax/product')}}/"+$(this).val();
            $('#city').load(url);
            return false;
        });
        $('.js-example-basic-single').select2();
        $('.select2').select2();
    });
    Vue.component('v-select', VueSelect.VueSelect);
</script>

@stack('scripts')
</body>
</body>
</html>
