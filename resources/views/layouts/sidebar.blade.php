<?php use App\Helpers\Appsrole as Appsrole; ?>
    <div class="sidebar-scroll">
        <div class="user-account">
            <div class="user_div">
                <img src="{{url('assets/images/icon.svg')}}" class="user-photo" alt="User Profile Picture">
            </div>
            <div class="dropdown">
                <span>Welcome,</span>
                <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>{{ Auth::user()->name }}</strong></a>
                <ul class="dropdown-menu dropdown-menu-right account vivify flipInY">
                    <li><a href="{{url('profile')}}"><i class="icon-settings"></i>Settings</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon-power"></i>Logout</a></li>
                </ul>
            </div>                
        </div>  
        <nav id="left-sidebar-nav" class="sidebar-nav">
            <ul id="main-menu" class="metismenu">
                <li class="header">Main</li>
                <?php
                $divisi = App\Admin::select('divisi')->where("id",Auth::id())->first()['divisi'];
                $idmenu = json_decode(Appsrole::menu($divisi));
                $menu = App\Models\AppsModules::where('type','!=','child')->whereIn('id',$idmenu)->with('child')->orderby('urutan','ASC')->get();
                // $menu = App\Models\AppsModules::where('type','!=','child')->with('child')->orderby('urutan','ASC')->get();
                ?>
                @foreach ($menu as $menu)
                    @if($menu['type']=='solo')
                        <li><a href="{{url($menu['url'])}}"><i class="{{$menu['icon']}}"></i><span>{{$menu['menu']}}</span></a></li>                    
                    @elseif($menu['type']=='parent')
                        <li>
                            <a href="#uiIcons" class="has-arrow"><i class="{{$menu['icon']}}"></i><span>{{$menu['menu']}}</span></a>
                            <ul>
                            @foreach($menu['child'] as $child)  
                                <li><a href="{{url($child['url'])}}">{{$child['menu']}}</a></li>
                            @endforeach
                            </ul>
                        </li>
                    @endif
                @endforeach
            </ul>
        </nav>     
    </div>