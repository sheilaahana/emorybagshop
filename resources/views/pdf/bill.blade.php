<!DOCTYPE html>
<?php use App\Helpers\Generate; ?>
<html>
<head>
	<title>Bill {{$data['invoice_number']}}</title>
    <style>
         @page { size: 17cm 7.5cm landscape;margin:25px;font-size:15px; }
         .page-break{
            display: block;
            page-break-after: always;
            position: relative;
        }
        p{
            margin: 3px;
        }
    </style>
</head>
<body>
<?php $a = 0; ?>
    @foreach($data['detail'] as $d)
        @for($i = 0; $i < $d['qty']; $i++)
        <p>
            <b>{{$data['warehouse']['code']}} - {{date('d/m/Y',strtotime($data['created_at']))}}</b>
        </p>
        <p>
            <b>NO ORDER :</b> 
            {{$data['invoice_number']}}<br>
            <b>WH :</b> 
            {{$data['warehouse']['warehouse']}}
        </p>
        <p>
            <b>
                CETAK : {{$data['print']['name']}} - {{$data['print']['id']}} <br>
                GUDANG :<br>
                QC : <br>
                PACKING:
            </b>
            <br>
            <b>EKSPEDISI :</b>
            {{$data['courier']." ".$data['shipping_service']}} - {{$data['ongkos_kirim']?Generate::money($data['ongkos_kirim']):'null'}}<br>
            <br>
            @if($data['cashless_order'])
                <b>CASHLESSID : </b><br>{{$data['cashless_order']}}<br>
            @endif
        </p>
        <p>
            <b>ORDERAN :</b><br>
            - {{$d['product']['product_name']}} {{$d['variant']['variant']}}, 
            @forelse($d['variant']['warehouse'] as $w)
                @if($w['warehouse_id'] == $data['warehouse_id'])
                ( Rak {{$w['nomor_rak']}} : Baris {{$w['baris']}} / Kolom {{$w['kolom']}} )
                @else
                <br>( Undefinded )
                @endif
            @empty
                <br>( Undefinded )
            @endforelse
        </p>
        <br>
       <hr>
       <br>
        <b>NO ORDER :</b> {{$data['invoice_number']}}<br>
        <b>To : </b>{{strtoupper($data['nama_penerima'])}}<br>
        <b>Telpon :</b>
        {{$data['telpon_penerima']}}<br>
        <b>Alamat :</b><br>
        {{$data['alamat_penerima']}}, {{$data['city']['type']." ".$data['city']['city']}}, {{$data['city']['province']['province']}}, {{$data['kodepos']}}<br>
        <b>From : </b>{{strtoupper($data['nama_pengirim']?:"undefinded")}}<br>
        <b>Telpon :</b>
        {{$data['kontak_pengirim']?:"undefinded"}}<br>
        {{$data['courier']." ".$data['shipping_service']}} - {{$data['ongkos_kirim']?Generate::money($data['ongkos_kirim']):'null'}}<br>
        @if($data['cashless_order'])
        <b>CASHLESSID : </b><br>{{$data['cashless_order']}}
        @endif
            <!-- NEW PAGE -->
            <div class="page-break"></div>
            <!-- NEW PAGE -->
        @endfor
    @endforeach
</body>
</html>