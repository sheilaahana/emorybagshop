<!DOCTYPE html>
<html>
<head>
	<title>Premium {{$data['invoice_number']}}</title>
    <style>
         @page { size: 20cm 14cm landscape;margin:25px;font-size:14px }
         .page-break{
            display: block;
            page-break-after: always;
            position: relative;
        }
        p{
            margin: 3px;
        }
        ul{
            margin: 0px;
        }
        li{
            margin-left: -25px;
        }
    </style>
</head>
<body>
    <table width="100%">
        <tr><td>Kepada</td><td>:</td><td>{{strtoupper($data['nama_penerima'])}}</td></tr>
        <tr><td>No HP</td><td>:</td><td>{{strtoupper($data['telpon_penerima'])}}</td></tr>
        <tr><td>Alamat</td><td>:</td><td>{{ucwords($data['alamat_penerima'])}}, {{$data['city']['type']." ".$data['city']['city']}}, {{$data['city']['province']['province']}}, {{$data['kodepos']}}</td></tr>
        @if($data['nama_pengirim'])
        <tr><td>Pengirim</td><td>:</td><td>{{$data['nama_pengirim']}}</td></tr>
        <tr><td>Kontak</td><td>:</td><td>{{$data['kontak_pengirim']}}</td></tr>
        @else
        <tr><td>Pengirim</td><td>:</td><td>EMORY STYLE</td></tr>
        @endif
        @if($data['cashless_order'])
        <tr><td>CashlessID</td><td>:</td><td>{{$data['cashless_order']}}</td></tr>
        @endif
        <tr><td><b>No Invoice</b></td><td>:</td><td><b>{{$data['invoice_number']}}</b></td></tr>
        <tr><td>Expedisi</td><td>:</td><td>{{$data['courier']." ".$data['shipping_service']}}</td></tr>
        <tr><td>Orderan</td><td>:</td><td>
            <ul>
            @foreach($data['detail'] as $d)
                <li>{{$d['product']['product_name']}} / {{$d['variant']['variant']}} / {{$d['qty']}} pcs</li>
            @endforeach
            </ul>
        </td></tr>
    </table>  
    <br>
    <p>
        Semoga EMORY ini dapat mencerahkan hati dan hati Kesayangan yaaa.<br>
        Jika sempat mohon beri testi atau tag foto kesayangan di Instagram kami @emorystyle.<br>
        <br>
        Note: Langsung periksa isi paket, mohon video saat membuka paket. 
        jika ada masalah kirimkan video saat membuka paket dan foto permasalahan dan langsung chat ke Whatsapp +62811-778-3800 dalam 1x24 jam setelah penerimaan.
        Gomawoyoo..
    </p><br>
    <hr>
    <center><b>EMORYSTYLE</b></center><br>

    <table width="100%">
        <tr><td width="80">No Invoice</td><td width="5px">:</td><td>{{$data['invoice_number']}}</td></tr>
        <tr><td>Date Order</td><td>:</td><td>{{date('d/m/Y h:i',strtotime($data['created_at']))}}</td></tr>
        <tr><td>Expedisi</td><td>:</td><td>{{$data['courier']." ".$data['shipping_service']}} ( {{$data['ongkos_kirim']?Generate::money($data['ongkos_kirim']):'null'}} ) </td></tr>
        <tr><td>Warehouse</td><td width="5px">:</td><td>{{$data['warehouse']['warehouse']}}</td></tr>
        @if($data['cashless_order'])
        <tr><td>CashlessID</td><td>:</td><td>{{$data['cashless_order']}}</td></tr>
        @endif
        <tr><td>Orderan</td><td>:</td><td>
            <ul>
            @foreach($data['detail'] as $d)
                <li>{{$d['product']['product_name']}} / {{$d['variant']['variant']}}</li>
                @forelse($d['variant']['warehouse'] as $w)
                    @if($w['warehouse_id'] == $data['warehouse_id'])
                    ( Rak {{$w['nomor_rak']}} : Baris {{$w['baris']}} / Kolom {{$w['kolom']}} )
                    @else
                    <br>( Undefinded )
                    @endif
                @empty
                    <br>( Undefinded )
                @endforelse
            @endforeach
            </ul>
        </td></tr>
    </table>
    <br>
    <table width="100%" >
        <tr><td width="80">CETAK</td><td width="5">:</td><td>{{$data['print']['name']}} - {{$data['print']['id']}}</td></tr>
        <tr><td>GUDANG</td><td>:</td></tr>
        <tr><td>QC</td><td>:</td></tr>
        <tr><td>PACKING</td><td>:</td></tr>
    </table>
    <i>* Mohon diisi keterangan </i>
        
            <!-- NEW PAGE -->
            <div class="page-break"></div>
            <!-- NEW PAGE -->
</body>
</html>