<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'API\UserController@register');

// Master Product
Route::get('payment/target', 'API\MasterController@payment_target');
Route::get('product/category', 'API\MasterController@product_category');
Route::get('slider', 'API\MasterController@slider');
Route::get('courier', 'API\MasterController@courier');
Route::get('general', 'API\MasterController@general_setting');

Route::get('shipping/courier', 'API\ShippingController@courier');
Route::get('shipping/province', 'API\ShippingController@province');
Route::get('shipping/city', 'API\ShippingController@city');
Route::post('shipping/tracking', 'API\ShippingController@tracking');

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('user', 'API\UserController@profile');
    Route::post('profile/edit', 'API\UserController@edit_profile');
    Route::post('profile/upload', 'API\UserController@upload_image');

    Route::group(['prefix' => 'product'], function(){
        Route::get('/', 'API\ProductController@index');
        Route::get('/detail/{id}', 'API\ProductController@detail');
    });
    Route::group(['prefix' => 'wishlist'], function(){
        Route::get('/', 'API\ActionController@wishlist');
        Route::post('/add', 'API\ActionController@add_wishlist');
        Route::get('/delete/{id}', 'API\ActionController@delete_wishlist');
        Route::get('/deletebyvariant/{id}', 'API\ActionController@delete_wishlist_variant');
        Route::get('/deletebyproduct/{id}', 'API\ActionController@delete_wishlist_product');
    });
    Route::group(['prefix' => 'cart'], function(){
        Route::get('/', 'API\ActionController@cart');
        Route::post('/add', 'API\ActionController@add_cart');
        Route::post('/mass', 'API\ActionController@mass_cart');
        Route::get('/delete/{id}', 'API\ActionController@delete_cart');
    });
    Route::group(['prefix' => 'address'], function(){
        Route::get('/', 'API\AddressController@address');
        Route::post('/add', 'API\AddressController@add_address');
        Route::get('/delete/{id}', 'API\AddressController@delete_address');
    });
    Route::group(['prefix' => 'transaction'], function(){
        Route::get('/', 'API\TransactionController@list');
        Route::get('/full', 'API\TransactionController@list_detail');
        Route::get('/detail/{id}', 'API\TransactionController@detail');
        Route::post('/terima', 'API\TransactionController@terima');
    });
    Route::group(['prefix' => 'wallet'], function(){
        Route::get('/list', 'API\WalletController@list');
        Route::get('/full', 'API\WalletController@full');
        Route::get('/amount', 'API\WalletController@amount');
    });
    
    Route::post('/billing', 'API\TransactionController@billing');
    Route::post('/voucher/check', 'API\VoucherController@voucher_check');
    Route::post('/checkout', 'API\TransactionController@checkout');
    Route::post('/upload/bukti', 'API\TransactionController@upload_bukti');
    Route::post('/mass/bukti', 'API\TransactionController@upload_bukti_mass');

    Route::post('shipping/cost', 'API\ShippingController@cost');
});