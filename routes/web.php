<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/export/log', 'CMS\DashboardController@export');
Route::get('/export/warehouse', 'CMS\DashboardController@export_wh');

Route::group(['middleware' => 'auth:web'], function(){
    Route::get('/', 'CMS\DashboardController@index')->name('home');

    Route::group(['prefix'=>'ajax'], function(){
        Route::get('/city/{id}', 'CMS\AppController@ajax_city');
        Route::get('/dashboard', 'CMS\DashboardController@ajax_data');
    });

    Route::group(['prefix'=>'profile'], function(){
        Route::get('/', 'CMS\ProfileController@index');
        Route::post('/setting/password', 'CMS\ProfileController@save_password');
        Route::post('/setting/profile', 'CMS\ProfileController@save_profile');
    });
    Route::group(['prefix'=>'log'], function(){
        Route::get('/', 'CMS\LogController@index');
        Route::post('/export', 'CMS\LogController@export');
        Route::get('/report', 'CMS\LogController@report');
        Route::get('/stock', 'CMS\LogController@stock');
        Route::post('/report', 'CMS\LogController@report');
    });
    Route::group(['prefix'=>'data'], function(){
        Route::get('/product', 'CMS\DataController@product');
        Route::get('/product/{wh}', 'CMS\DataController@product_user');
        Route::get('/variant/{id}', 'CMS\DataController@variant_product');
        Route::get('/variant/{id}/{user}', 'CMS\DataController@variant_user');
        Route::get('/stock/{id}/{user}', 'CMS\DataController@stock_user');
        Route::get('/user', 'CMS\DataController@user');
        Route::get('/user/address/{id}', 'CMS\DataController@user_address');
        Route::get('/address/{id}', 'CMS\DataController@address');
    });

    Route::get('/dashboard', 'CMS\DashboardController@index');
    Route::group(['prefix' => 'customer'], function(){
        Route::get('/', 'CMS\CustomerController@index');
        Route::get('/form', 'CMS\CustomerController@form');
        Route::post('/save', 'CMS\CustomerController@save');
        Route::post('/mass/save', 'CMS\CustomerController@mass_save');
        Route::get('/detail/{id}', 'CMS\CustomerController@detail');
        Route::get('/status/{id}', 'CMS\CustomerController@change_status');
    });
    Route::group(['prefix'=>'administrator'], function(){
        Route::get('/', 'CMS\AdministratorController@index');
        Route::get('/form', 'CMS\AdministratorController@form');
        Route::get('/role', 'CMS\AdministratorController@role_form');
        Route::get('/reset/{id}', 'CMS\AdministratorController@reset');
        Route::get('/delete/{id}', 'CMS\AdministratorController@delete');
        Route::post('/save', 'CMS\AdministratorController@save');
        Route::post('/save_role', 'CMS\AdministratorController@save_role');
    });
    Route::group(['prefix' => 'transaction'], function(){
        Route::get('/', 'CMS\TransactionController@index');
        Route::get('/form', 'CMS\TransactionController@form');
        Route::get('/detail/{id}', 'CMS\TransactionController@detail');
        Route::get('/delete/{id}', 'CMS\TransactionController@delete_transaction');
        Route::get('/payment/{status}/{id}', 'CMS\TransactionController@payment');
        Route::get('/shipping/{status}/{id}', 'CMS\TransactionController@shipping');
        Route::get('/nota/{type}/{id}', 'CMS\TransactionController@nota');
        Route::post('/save', 'CMS\TransactionController@save_transaction');

        Route::get('/update', 'CMS\TransactionController@update');
        Route::post('/update', 'CMS\TransactionController@update');
        Route::post('/update/save', 'CMS\TransactionController@update_save');

        Route::get('/daily', 'CMS\TransactionController@daily');
        Route::post('/daily', 'CMS\TransactionController@daily');
        Route::post('/daily/export', 'CMS\TransactionController@daily_export');

        Route::get('/retur/detail/{id}', 'CMS\ReturController@retur_detail');

        Route::post('/change/status', 'CMS\TransactionController@change_status');

    }); 
    Route::group(['prefix' => 'retur'], function(){
        Route::get('/', 'CMS\ReturController@list');
        Route::get('/status/{status}/{id}', 'CMS\ReturController@retur_status');
        Route::get('/detail/{id}', 'CMS\ReturController@retur_detail');
        Route::get('/form/{id}', 'CMS\ReturController@retur_form');
        Route::post('/save', 'CMS\ReturController@retur_save');
        Route::post('/new/invoice', 'CMS\ReturController@new_invoice');
        Route::get('/new/transaction/{id}', 'CMS\ReturController@new_transaction');
    }); 
    Route::group(['prefix' => 'warehouse'], function(){
        Route::get('/', 'CMS\WarehouseController@index');
        Route::get('/detail/{id}', 'CMS\WarehouseController@detail');
        Route::get('/form', 'CMS\WarehouseController@form');
        Route::get('/delete/{id}', 'CMS\WarehouseController@delete_warehouse');
        Route::post('/save', 'CMS\WarehouseController@save');
        Route::post('/save/detail', 'CMS\WarehouseController@save_detail');
        Route::get('/form/product/{id}', 'CMS\WarehouseController@form_product');
        Route::post('/save/product', 'CMS\WarehouseController@save_product');

        Route::get('/transfer/{id}', 'CMS\WarehouseController@transfer_stock');
        Route::post('/save/transfer', 'CMS\WarehouseController@transfer_save');

    }); 
    Route::group(['prefix' => 'voucher'], function(){
        Route::get('/', 'CMS\VoucherController@index');
        Route::get('/detail/{id}', 'CMS\VoucherController@detail');
        Route::get('/form', 'CMS\VoucherController@form');
        Route::post('/save', 'CMS\VoucherController@save');
        Route::get('/change/{data}/{id}', 'CMS\VoucherController@change_status');
        Route::get('/delete/{id}', 'CMS\VoucherController@delete');
    }); 
    Route::group(['prefix' => 'wallet'], function(){
        Route::get('/', 'CMS\WalletController@index');
        Route::get('/detail/{id}', 'CMS\WalletController@detail');
        Route::get('/form', 'CMS\WalletController@form');
        Route::post('/save', 'CMS\WalletController@save');
        Route::get('/{data}/{id}', 'CMS\WalletController@change_status');
    }); 
    Route::group(['prefix' => 'app'], function(){
        Route::get('/setting', 'CMS\AppController@index');
        Route::post('/save', 'CMS\AppController@save');
    }); 
    Route::group(['prefix' => 'confirmation'], function(){
        Route::get('/', 'CMS\ConfirmationController@index');
    }); 
    Route::group(['prefix' => 'print'], function(){
        Route::get('/', 'CMS\PrintController@index');
        Route::get('/change/{id}', 'CMS\PrintController@change');
    }); 
    Route::group(['prefix' => 'gudang'], function(){
        Route::get('/', 'CMS\GudangController@index');
        Route::post('/change/{id}', 'CMS\GudangController@change');
    }); 
    Route::group(['prefix' => 'ekspedisi'], function(){
        Route::get('/', 'CMS\EkspedisiController@index');
        Route::post('/change/{id}', 'CMS\EkspedisiController@change');
    }); 
    Route::group(['prefix' => 'shipping'], function(){
        Route::get('/', 'CMS\ConfirmationController@shipping');
        Route::post('/save', 'CMS\ConfirmationController@shipping_save');
        Route::get('/cost', 'CMS\ShippingController@cost');
        Route::post('/cost', 'CMS\ShippingController@cost');
        Route::get('/resi', 'CMS\ShippingController@resi');
        Route::post('/resi', 'CMS\ShippingController@resi');
    }); 
    Route::group(['prefix' => 'report'], function(){
        Route::get('/', 'CMS\ReportController@index');
        Route::post('/', 'CMS\ReportController@index');
        Route::post('/export', 'CMS\ReportController@export');
        Route::get('/daily', 'CMS\ReportController@daily');
        Route::post('/daily', 'CMS\ReportController@daily');
        Route::post('/daily/export', 'CMS\ReportController@daily_export');
        Route::get('/stock', 'CMS\ReportController@stock');
        Route::post('/stock/export', 'CMS\ReportController@export_stock');
        Route::get('/handler', 'CMS\ReportController@handler');
        Route::post('/handler', 'CMS\ReportController@handler');
        Route::post('handler/export', 'CMS\ReportController@handler_export');
    }); 
    Route::group(['prefix' => 'product'], function(){
        Route::get('/', 'CMS\ProductController@index');
        Route::get('/form', 'CMS\ProductController@form');
        Route::post('/save', 'CMS\ProductController@save');
        Route::get('/detail/{id}', 'CMS\ProductController@detail');
        Route::get('/status/{field}/{id}', 'CMS\ProductController@change_status');
        Route::get('/delete/{id}', 'CMS\ProductController@delete');
        Route::get('/mass/{id}', 'CMS\ProductController@mass_form');
        Route::get('/warehouse/{id}', 'CMS\ProductController@warehouse_check');
        Route::post('/mass/save', 'CMS\ProductController@mass_save');
        Route::group(['prefix' => 'variant'], function(){
            Route::get('/form', 'CMS\ProductController@variant_form');
            Route::post('/save', 'CMS\ProductController@variant_save');
            Route::get('/delete/{id}', 'CMS\ProductController@variant_delete');
        });
        Route::get('/stock/{id}', 'CMS\ProductController@log_stock');
    });
    Route::group(['prefix'=>'cart'], function(){
        Route::get('/', 'CMS\CartController@index');
        Route::get('/form', 'CMS\CartController@form');
        Route::post('/save', 'CMS\CartController@save');
        Route::get('/detail/{id}', 'CMS\CartController@detail');
        Route::get('/delete/{id}', 'CMS\CartController@delete');
    });

    // Data Master
    Route::group(['prefix' => 'category'], function(){
        Route::group(['prefix' => 'user'], function(){
            Route::get('/', 'CMS\CategoryController@user');
            Route::get('/form', 'CMS\CategoryController@user_form');
            Route::post('/save', 'CMS\CategoryController@user_save');
            Route::get('/delete/{id}', 'CMS\CategoryController@user_delete');
        });
        Route::group(['prefix' => 'product'], function(){
            Route::get('/', 'CMS\CategoryController@product');
            Route::get('/form', 'CMS\CategoryController@product_form');
            Route::post('/save', 'CMS\CategoryController@product_save');
            Route::get('/delete/{id}', 'CMS\CategoryController@product_delete');
        });
    });
    Route::group(['prefix' => 'payment/target'], function(){
        Route::get('/', 'CMS\PaymentController@target');
        Route::get('/form', 'CMS\PaymentController@target_form');
        Route::post('/save', 'CMS\PaymentController@target_save');
        Route::get('/delete/{id}', 'CMS\PaymentController@target_delete');
    });
    Route::group(['prefix' => 'slider'], function(){
        Route::get('/', 'CMS\SliderController@index');
        Route::get('/form', 'CMS\SliderController@form');
        Route::post('/save', 'CMS\SliderController@save');
        Route::get('/delete/{id}', 'CMS\SliderController@delete');
        Route::get('/publish/{id}', 'CMS\SliderController@change_status');
    });
});